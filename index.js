import { AppRegistry, LogBox } from 'react-native';

import App from './App';
import { name as appName } from './app.json';

// To ignore warning arising during development.
// YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader', 'Setting a timer']);
LogBox.ignoreLogs(['Require cycle:', 'Remote debugger']);

AppRegistry.registerComponent(appName, () => App);
