import { useCallback, useMemo } from 'react';

import { useDispatch } from 'react-redux';
import { startGpsRecording, stopGpsRecording, subscribeToGps, unsubscribeFromGps } from '../actions';

import { useShallowEqualSelector } from './useShallowEqualSelector';

const isContainingNullValues = (object) => Object.values(object).some((item) => item === null);

export const useGpsSubscription = ({ observer = null, shouldReceivePositionUpdates = true }) => {
  const dispatch = useDispatch();

  const gpsPosition = useShallowEqualSelector((state) => state.gps.position, { enabled: shouldReceivePositionUpdates });
  const gpsError = useShallowEqualSelector((state) => state.gps.error);
  const isRecording = useShallowEqualSelector((state) => state.gps.isRecording);
  const observers = useShallowEqualSelector((state) => state.gps.observers);

  const isObserving = useMemo(() => observers.includes(observer), [observers, observer]);
  const hasOtherObservers = useMemo(() => observers.filter((item) => item !== observer).length, [observers, observer]);

  const position = useMemo(() => {
    if (!isObserving || !shouldReceivePositionUpdates || isContainingNullValues(gpsPosition)) return null;
    return gpsPosition;
  }, [isObserving, gpsPosition, shouldReceivePositionUpdates]);

  const error = useMemo(() => {
    if (isContainingNullValues(gpsError)) return null;
    return gpsError;
  }, [gpsError]);

  const subscribe = useCallback(() => {
    if (!observer) return;

    if (!isRecording) {
      dispatch(startGpsRecording());
    }

    if (!isObserving) {
      dispatch(subscribeToGps(observer));
    }
  }, [isObserving, isRecording, observer, dispatch]);

  const unsubscribe = useCallback(() => {
    if (isObserving) {
      dispatch(unsubscribeFromGps(observer));
    }

    if (isRecording && !hasOtherObservers) {
      dispatch(stopGpsRecording());
    }
  }, [isObserving, isRecording, hasOtherObservers, observer, dispatch]);

  return {
    isObserving,
    position,
    error,
    subscribe,
    unsubscribe,
  };
};
