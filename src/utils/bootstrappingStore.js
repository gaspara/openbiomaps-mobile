import { deleteCurrentTrackLog, saveCurrentGlobalTrackLog, setLanguage, stopTrackLogRecording } from '../actions';
import ClientLogger, { SelectableClientLoggers } from '../api/ClientLogger';
import { store } from '../config/setupStore';

const initLanguageSettings = ({ language }) => {
  if (language.id) {
    store.dispatch(setLanguage());
  }
};

const saveForceClosedTrackLog = ({ recordingInProgress, currentTrackLog }) => {
  if (recordingInProgress) {
    store.dispatch(stopTrackLogRecording());
  }

  if (currentTrackLog) {
    if (currentTrackLog.trackLogArray.length) {
      store.dispatch(saveCurrentGlobalTrackLog());
    } else {
      store.dispatch(deleteCurrentTrackLog());
    }
  }
};

export const bootstrappingStore = () => {
  const { settings, trackLog } = store.getState();

  initLanguageSettings(settings);
  saveForceClosedTrackLog(trackLog);

  // set the log mode of the ClientLogger. Only works on Development
  ClientLogger.setLogMode(SelectableClientLoggers.NONE);
};
