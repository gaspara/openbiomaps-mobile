import { useCallback, useEffect, useRef } from 'react';

import BackgroundTimer from 'react-native-background-timer';
import { useDispatch } from 'react-redux';

import { useShallowEqualSelector } from './useShallowEqualSelector';

export const useTrackLogPositionUpdate = (updateAction, validPosition, shouldUpdatePosition = true) => {
  const dispatch = useDispatch();
  const trackLogUpdateInterval = useShallowEqualSelector((state) => state.settings.trackLogUpdateInterval);

  const interval = useRef();
  const position = useRef(validPosition);

  const hasValidPostion = !!validPosition;

  const updatePosition = useCallback(() => {
    if (interval.current) return;

    dispatch(updateAction(position.current));

    interval.current = BackgroundTimer.setInterval(() => {
      dispatch(updateAction(position.current));
    }, trackLogUpdateInterval);
  }, [updateAction, trackLogUpdateInterval, dispatch]);

  useEffect(() => {
    position.current = validPosition;
  }, [validPosition]);

  useEffect(() => {    
    if (hasValidPostion && shouldUpdatePosition && trackLogUpdateInterval) {
      updatePosition();
    }

    return () => {
      if (interval.current) {
        BackgroundTimer.clearInterval(interval.current);
        interval.current = undefined;
      }
    };
  }, [hasValidPostion, shouldUpdatePosition, updatePosition, trackLogUpdateInterval]);
};
