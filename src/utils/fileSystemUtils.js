import { FileSystem } from 'react-native-unimodules';
import { FILE_STORAGE_DIRECTORY } from '../Constants';

export const deleteFile = async (uri) => {
  try {
    await FileSystem.deleteAsync(uri, { idempotent: true });
    return true;
  } catch (error) {
    console.log('delete error', error);
  }
  return false;
};

export const getFileInfo = async (uri) => {
  try {
    const info = await FileSystem.getInfoAsync(uri);
    return info;
  } catch (error) {
    console.log('error getting info from file', error);
  }
  return null;
};

export const getAttachedFileDirContent = async () => {
  try {
    const dirContent = await FileSystem.readDirectoryAsync(FILE_STORAGE_DIRECTORY);
    return dirContent.map((fileNames) => `${FILE_STORAGE_DIRECTORY}${fileNames}`);
  } catch (error) {
    console.log('error reading dir content', error);
  }
  return [];
};

export const moveToAttachedFiles = async (fileName, uri) => {
  try {
    await FileSystem.makeDirectoryAsync(FILE_STORAGE_DIRECTORY, { intermediates: true });
    // ensuring filename uniqueness
    const newFileName = `${new Date().getTime()}_${fileName}`;
    const newUri = `${FILE_STORAGE_DIRECTORY}${newFileName}`;
    try {
      await FileSystem.moveAsync({ from: uri, to: newUri });
    } catch (error) {
      await FileSystem.copyAsync({ from: uri, to: newUri });
    }
    return { ok: true, data: { newFileName, newUri } };
  } catch (error) {
    console.log('error copying to attached files');
  }
  return { ok: false };
};
