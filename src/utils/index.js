import React from 'react';
import moment from 'moment';
import Toast from 'react-native-simple-toast';
import createGpx from 'gps-to-gpx';
import Image from '../components/HTMLImage';
import I18n from '../i18n';
import { SESSION_TRACKLOG_META_INFO } from '../Constants';
import { parseValueToString, isValidCoordinateArray } from './gpsUtils';

export const queryString = (params) =>
  Object.keys(params)
    .map(
      (key) => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`,
    )
    .join('&');

export const decodeQueryString = (string) =>
  string?.split('&').reduce((obj, line) => {
    const parts = line.split('=');
    if (parts[0] && parts[1]) {
      // eslint-disable-next-line no-param-reassign
      obj[decodeURIComponent(parts[0])] = decodeURIComponent(parts[1]);
    }
    return obj;
  }, {});

export const getCenterOfPlot = (type, data) => {
  let middleIndex;
  let sumLatitude;
  let sumLongitude;
  switch (type) {
    case 'point':
      return data;
    case 'line':
      middleIndex = Math.floor(data.length / 2) - 1;
      return data[middleIndex];
    case 'area':
      sumLatitude = data.reduce((acc, curr) => acc + Number(curr.latitude), 0);
      sumLongitude = data.reduce(
        (acc, curr) => acc + Number(curr.longitude),
        0,
      );
      return {
        latitude: sumLatitude / data.length,
        longitude: sumLongitude / data.length,
      };
    default:
      return null;
  }
};

export const getAverageByKey = (coordArray, key) => {
  const sum = coordArray.reduce((acc, curr) => acc + curr[key], 0);
  return sum / coordArray.length;
};

export const getCoordDeltaFromCoordArray = (coordArray, key) => {
  let min = coordArray[0][key];
  let max = coordArray[0][key];
  coordArray.forEach((coord) => {
    min = Math.min(min, coord[key]);
    max = Math.max(max, coord[key]);
  });
  return Math.abs(max - min);
};

export const calculateInitialRegion = (geometryValue) => {
  if (!Array.isArray(geometryValue)) {
    return {
      latitude: geometryValue.latitude,
      longitude: geometryValue.longitude,
      latitudeDelta: 0.0082,
      longitudeDelta: 0.0051,
    };
  }
  return {
    latitude: getAverageByKey(geometryValue, 'latitude'),
    longitude: getAverageByKey(geometryValue, 'longitude'),
    latitudeDelta: Math.max(
      0.0082,
      getCoordDeltaFromCoordArray(geometryValue, 'latitude'),
    ),
    longitudeDelta: Math.max(
      0.0051,
      getCoordDeltaFromCoordArray(geometryValue, 'longitude'),
    ),
  };
};

export const calculateDistance = (coord1, coord2) => {
  const { latitude: lat1, longitude: lon1 } = coord1;
  const { latitude: lat2, longitude: lon2 } = coord2;

  const R = 6371e3;
  const φ1 = (lat1 * Math.PI) / 180;
  const φ2 = (lat2 * Math.PI) / 180;
  const Δφ = ((lat2 - lat1) * Math.PI) / 180;
  const Δλ = ((lon2 - lon1) * Math.PI) / 180;

  const a =
    Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
    Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

  return R * c;
};

export const parseDataToWkt = (data, type) => {
  if (data.latitude && data.longitude) {
    return `POINT (${data.longitude} ${data.latitude})`;
  }
  if (type === 'line' && Array.isArray(data) && data.length > 0) {
    return `LINESTRING (${data
      .reduce((acc, curr) => `${acc}${curr.longitude} ${curr.latitude}, `, '')
      .slice(0, -2)})`;
  }

  if (type === 'polygon' && Array.isArray(data) && data.length > 0) {
    const firstElement =
      data.length > 0 ? `${data[0].longitude} ${data[0].latitude}` : '';
    return `POLYGON ((${data.reduce(
      (acc, curr) => `${acc}${curr.longitude} ${curr.latitude}, `,
      '',
    )}${firstElement}))`;
  }

  return null;
};

export const parseWkt = (wkt) => {
  const point = /POINT\s*\(([^)]*)\)/;
  const line = /LINESTRING\s*\(([^)]*)\)/;
  const polygon = /POLYGON\s*\(\(([^)]*)\)(?:,\s*\(([^)]*)\))?\)/;
  const coordinate = /(\d+(?:\.\d+)?)\s+(\d+(?:\.\d+)?)/g;

  const getCoordinate = (data) => {
    const coordinate = /(-?\d+(?:\.\d+)?)\s(-?\d+(?:\.\d+)?)/g;
    const [, longitude, latitude] = coordinate.exec(data);
    return {
      latitude: Number(latitude),
      longitude: Number(longitude),
    };
  };

  let match = point.exec(wkt);
  if (match) {
    const [, innerText] = match;
    return {
      type: 'point',
      data: getCoordinate(innerText),
    };
  }

  const getCoordinates = (data) => data.match(coordinate).map(getCoordinate);

  match = line.exec(wkt);
  if (match) {
    const [, innerText] = match;
    return {
      type: 'line',
      data: getCoordinates(innerText),
    };
  }

  match = polygon.exec(wkt);
  if (match) {
    const [, area, ...holes] = match;
    return {
      type: 'area',
      data: getCoordinates(area),
      holes: holes
        ? holes.filter((hole) => hole).map((hole) => getCoordinates(hole))
        : [],
    };
  }
  return null;
};

export const formatBytes = (bytes, decimals = 2) => {
  if (bytes === 0) return '0 byte';

  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ['Byte', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return `${parseFloat((bytes / k ** i).toFixed(dm))} ${sizes[i]}`;
};

export const isAccessTokenExpired = ({
  requestTime,
  expiresIn,
  constraintTime = 0,
}) => {
  const diff = moment(moment()).diff(requestTime);
  // console.log(`access token expired? ${diff + constraintTime > expiresIn} diff: ${diff} conTime: ${constraintTime}, ${expiresIn}`);
  return diff + constraintTime > expiresIn;
};

export const nextAccessTokenCheckTime = (
  requestTime,
  expiresIn,
  refreshTime = 5 * 60000,
) => {
  const diff = moment(moment()).diff(requestTime);
  return Math.min(expiresIn - diff + 200, refreshTime);
};

export const renderNode = (node) => {
  if (node.name === 'img') {
    const a = node.attribs;
    return <Image key={a.src} src={a.src} height={a.height} width={a.width} />;
  }
  return undefined;
};

export const formatToFilename = (filename) =>
  filename.replace(/[/\\?%*:.|"<>]/g, '');

export const isValidFilename = (filename) =>
  filename === filename.replace(/[/\\?%*:|"<>]/g, '');

/**
 * Generates a string used for the name of the tracklogs from the startTime and endTime
 * @param {date} startTime
 * @param {date} endTime
 */
export const generateFormattedNameFromStartAndEndTime = (
  startTime = new Date(),
  endTime = new Date(),
) => {
  const offset = new Date().getTimezoneOffset() * 60000;
  return `${new Date(startTime - offset).toISOString().slice(0, 19)}${new Date(
    endTime - offset,
  )
    .toISOString()
    .slice(10, 19)}`.replace(/T/g, '-');
};

/**
|--------------------------------------------------
| SELECTORS
|--------------------------------------------------
*/

/**
 * Return the currently selected server object
 * @param {array of objects} serverList
 */
export const getSelectedServer = (serverList) =>
  serverList.find((server) => server.isSelected) || {};

/**
 * Return the currently selected database object
 * @param {array of objects} selectedServer - Target server object
 */
export const getSelectedDatabase = (selectedServer) => {
  const { databases = {} } = selectedServer;
  const { data: databaseList = [] } = databases;

  return databaseList.find((database) => database.isSelected) || {};
};

/**
 * Return the currently selected observation object
 * @param {array of objects} selectedDatabase - Target database object
 */
export const getSelectedObservation = (selectedDatabase) => {
  const { observations = {} } = selectedDatabase;
  const { data: observationList = [] } = observations;

  return observationList.find((observation) => observation.isSelected) || {};
};

export const getPreviouslySelectedDatabases = (serverList) => {
  const previouslySelectedDatabases = [];

  serverList.forEach((server) => {
    if (server.databases?.data && server.user && server.user?.accessToken) {
      server.databases.data.forEach((database) => {
        if (database.observations?.data?.length > 0 && database.projectUrl) {
          previouslySelectedDatabases.push({
            databaseId: database.id,
            databaseName: database.name,
            projectUrl: database.projectUrl,
            userName: server.user.userName,
            authObject: {
              serverId: server.id,
              serverUrl: server.url,
              accessToken: server.user.accessToken,
              refreshToken: server.user.refreshToken,
              expiresIn: server.user.expiresIn,
              requestTime: server.user.requestTime,
            },
          });
        }
      });
    }
  });
  return previouslySelectedDatabases;
};

/**
 * Return database list reducer states of the selected server
 * @param {object} selectedServer - Targer server object
 */
export const getDatabaseState = (selectedServer) => {
  const { databases = {} } = selectedServer;
  const { data: databaseList = [], fetching = false, error = null } = databases;

  return {
    databaseList,
    fetching,
    error,
  };
};

/**
 * Return observation list reducer states of the selected database
 * @param {object} selectedDatabase - Target database object
 */
export const getObservationState = (selectedDatabase) => {
  const { observations = {} } = selectedDatabase;
  const {
    data: observationList = [],
    fetching = false,
    error = null,
    gameFetching = false,
  } = observations;

  return {
    observationList: observationList.filter(
      (observation) => !observation.hidden,
    ),
    fetching,
    error,
    gameFetching,
  };
};

/**
 * Return user states beloning to the selected server
 * @param {object} selectedServer - Target server object
 */
export const getUserState = (selectedServer) => {
  const { user = {} } = selectedServer;
  const {
    fetching = false,
    error = null,
    accessToken = null,
    refreshToken = null,
    userName = null,
    expiresIn = null,
    requestTime = null,
  } = user;

  return {
    fetching,
    error,
    accessToken,
    refreshToken,
    userName,
    expiresIn,
    requestTime,
  };
};

/**
 * Return measurement list reducer states beloning to the selected observation
 * @param {object} selectedObservation - Target observation object
 */
export const getMeasurementState = (selectedObservation) => {
  const { measurements = {} } = selectedObservation;
  const {
    data: measurementList = [],
    fetching = false,
    error = null,
  } = measurements;

  return {
    measurementList,
    fetching,
    error,
  };
};

/**
 * Return form states beloning to the selected observation
 * @param {object} selectedObservation - Target observation object
 */
export const getFormState = (selectedObservation) => {
  const { form = {} } = selectedObservation;
  const {
    dataTypes = [],
    permanentSamplePlotInfos = [],
    fetching = false,
    error = null,
    stickinesses = {},
    lastPickedValues = {},
  } = form;

  return {
    dataTypes,
    permanentSamplePlotInfos,
    fetching,
    error,
    stickinesses,
    lastPickedValues,
  };
};

/**
 * Return toplist state belonging to the selected database
 * @param {obser} selectedDatabase - Target database object
 */
export const getTopListState = (selectedDatabase) => {
  const { toplist: toplistReducer = {} } = selectedDatabase;
  const { data: toplist = [], fetching = false, error = null } = toplistReducer;

  return {
    toplist,
    fetching,
    error,
  };
};

/**
 * Return all measurements
 * @param {array} serverList
 * @param {boolean} shouldIncludeSessions - Determine whether the session related null records should be returned or not
 */
export const getAllMeasurements = (
  serverList,
  shouldIncludeSessions = true,
) => {
  const measurementData = [];

  const assembleDataObject = (server, database, observation, measurement) => {
    measurementData.push({
      server: {
        id: server.id,
        url: server.url,
        name: server.name,
        user: server.user,
      },
      database: {
        id: database.id,
        name: database.name,
        projectUrl: database.projectUrl,
        game: database.game,
        projectFileSize: database.projectFileSize,
      },
      observation: {
        id: observation.id,
        name: observation.name,
        form: observation.form,
        hidden: observation.hidden,
        session: observation.session,
      },
      measurement,
    });
  };

  serverList.forEach((server) => {
    if (server.user && server.databases && server.databases.data.length) {
      server.databases.data.forEach((database) => {
        if (
          database.lastSelected &&
          database.observations &&
          database.observations.data.length
        ) {
          database.observations.data.forEach((observation) => {
            if (
              observation.measurements &&
              observation.measurements.data.length &&
              observation.form &&
              observation.form.dataTypes.length
            ) {
              observation.measurements.data.forEach((measurement) => {
                if (!shouldIncludeSessions) {
                  if (measurement.data) {
                    assembleDataObject(
                      server,
                      database,
                      observation,
                      measurement,
                    );
                  }
                } else {
                  assembleDataObject(
                    server,
                    database,
                    observation,
                    measurement,
                  );
                }
              });
            }
          });
        }
      });
    }
  });

  return measurementData;
};

/**
 * Return measurements which satisfies the filtering condition
 * @param {array} serverList
 * @param {boolean} shouldIncludeSessions - Determine whether the session related null records should be returned or not
 * @param {func} filteringCondition - A function wich returns true or false
 */
export const getFilteredMeasurements = (
  serverList,
  shouldIncludeSessions = true,
  filteringCondition = () => true,
) => {
  const measurementData = [];

  const assembleDataObject = (server, database, observation, measurement) => {
    measurementData.push({
      server: {
        id: server.id,
        url: server.url,
        name: server.name,
        user: server.user,
      },
      database: {
        id: database.id,
        name: database.name,
        projectUrl: database.projectUrl,
        game: database.game,
        projectFileSize: database.projectFileSize,
      },
      observation: {
        id: observation.id,
        name: observation.name,
        form: observation.form,
        hidden: observation.hidden,
        session: observation.session,
      },
      measurement,
    });
  };

  serverList.forEach((server) => {
    if (server.user && server.databases && server.databases.data.length) {
      server.databases.data.forEach((database) => {
        if (
          database.lastSelected &&
          database.observations &&
          database.observations.data.length
        ) {
          database.observations.data.forEach((observation) => {
            if (
              observation.measurements &&
              observation.measurements.data.length &&
              observation.form &&
              observation.form.dataTypes.length
            ) {
              observation.measurements.data.forEach((measurement) => {
                if (filteringCondition(measurement)) {
                  if (!shouldIncludeSessions) {
                    if (measurement.data) {
                      assembleDataObject(
                        server,
                        database,
                        observation,
                        measurement,
                      );
                    }
                  } else {
                    assembleDataObject(
                      server,
                      database,
                      observation,
                      measurement,
                    );
                  }
                }
              });
            }
          });
        }
      });
    }
  });

  return measurementData;
};

/**
 * Gets the file uris of all measurements
 * @param {*} serverList
 * @returns
 */
export const getUsedAttachedFileUris = (serverList) => {
  const usedAttachedFileUris = [];

  serverList.forEach((server) => {
    if (server.user && server.databases && server.databases.data.length) {
      server.databases.data.forEach((database) => {
        if (
          database.lastSelected &&
          database.observations &&
          database.observations.data.length
        ) {
          database.observations.data.forEach((observation) => {
            if (
              observation.measurements &&
              observation.measurements.data.length &&
              observation.form &&
              observation.form.dataTypes.length
            ) {
              const fileTypeColumns = observation.form.dataTypes.reduce(
                (acc, cur) => {
                  if (cur.type === 'file_id') {
                    acc.push(cur.column);
                  }
                  return acc;
                },
                [],
              );
              if (fileTypeColumns.length) {
                observation.measurements.data.forEach((measurement) => {
                  fileTypeColumns.forEach((fileTypeColumn) => {
                    if (
                      measurement?.data &&
                      measurement.data[fileTypeColumn]?.length
                    ) {
                      usedAttachedFileUris.push(
                        measurement.data[fileTypeColumn].map(
                          (file) => file.uri,
                        ),
                      );
                    }
                  });
                });
              }
            }
          });
        }
      });
    }
  });
  // since we added arrays to the return array, we need to flat them
  return usedAttachedFileUris.flat();
};

export const getLastObservation = (measurements) => {
  let maxCreatedAt = measurements[0].measurement.date;
  let lastObservation = null;
  measurements.forEach((measurementObject) => {
    if (
      new Date(maxCreatedAt) <= new Date(measurementObject.measurement.date)
    ) {
      maxCreatedAt = measurementObject.measurement.date;
      lastObservation = measurementObject;
    }
  });

  if (!lastObservation) return null;

  return {
    name: lastObservation.observation.name,
    server: lastObservation.server,
    database: lastObservation.database,
    databaseName: lastObservation.database.name,
    observation: lastObservation.observation,
    serverUrl: lastObservation.server.url,
  };
};

export const asyncForEach = async (array, callback) => {
  for (let index = 0; index < array.length; index++) {
    // eslint-disable-next-line no-await-in-loop
    await callback(array[index], index, array);
  }
};

/**
 * Group the measurements to be able to perform actions on all the measurement from the same type (observationId) but with other structure of measurement objects
 * @param {list} measurementList
 */
const groupSyncedMeasurements = (measurementList) => {
  const groupedMeasurements = {};

  measurementList.forEach(
    ({ serverId, databaseId, observationId, measurementId }) => {
      // this would be nicer
      // groupedMeasurements[server.id][database.id][observation.id] = measurement.id;

      if (groupedMeasurements[serverId] === undefined) {
        groupedMeasurements[serverId] = {};
      }
      if (groupedMeasurements[serverId][databaseId] === undefined) {
        groupedMeasurements[serverId][databaseId] = {};
      }
      if (
        groupedMeasurements[serverId][databaseId][observationId] === undefined
      ) {
        groupedMeasurements[serverId][databaseId][observationId] = [];
      }
      groupedMeasurements[serverId][databaseId][observationId].push(
        measurementId,
      );
    },
  );

  return groupedMeasurements;
};

/**
 * Group the measurements to be able to perform actions on all the measurement from the same type (observationId)
 * @param {list} measurementList
 */
export const groupMeasurements = (measurementList) => {
  if (measurementList.length > 0 && measurementList[0].serverId) {
    return groupSyncedMeasurements(measurementList);
  }
  const groupedMeasurements = {};

  measurementList.forEach(({ server, database, observation, measurement }) => {
    // this would be nicer
    // groupedMeasurements[server.id][database.id][observation.id] = measurement.id;

    if (groupedMeasurements[server.id] === undefined) {
      groupedMeasurements[server.id] = {};
    }
    if (groupedMeasurements[server.id][database.id] === undefined) {
      groupedMeasurements[server.id][database.id] = {};
    }
    if (
      groupedMeasurements[server.id][database.id][observation.id] === undefined
    ) {
      groupedMeasurements[server.id][database.id][observation.id] = [];
    }
    groupedMeasurements[server.id][database.id][observation.id].push(
      measurement.id,
    );
  });

  return groupedMeasurements;
};

/**
 * Remove blank object attributes
 * @param {Object} object
 */
export const cleanObject = (object) => {
  const newObject = { ...object };

  Object.keys(newObject).forEach((key) => {
    if (newObject[key] === null || newObject[key] === undefined) {
      delete newObject[key];
    }
  });

  return newObject;
};

/**
 * Checks whether the provided object is empty {} or not.
 */
export const isEmpty = (obj) => {
  // eslint-disable-next-line
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) return false; // eslint-disable-line
  }
  return true;
};

/**
 * Returns an array of objects containing those observations' path
 * which have an active session, aka. list createion is in progress.
 * If getOnlyForceTrackLogModes is true, only the forced or manually started tracklogs will be added to the gps handler
 * @param {array} serverList
 * @param {boolean} getOnlyForceTrackLogModes
 * @returns {array}
 */
export const getSessions = (serverList, getOnlyTrackLogSessions = false) => {
  const sessions = [];

  serverList.forEach((server) => {
    if (server.databases && server.databases.data.length) {
      server.databases.data.forEach((database) => {
        if (database.observations && database.observations.data.length) {
          database.observations.data.forEach((observation) => {
            if (
              observation.session &&
              (!getOnlyTrackLogSessions ||
                observation.session.shouldRecordTrackLog)
            ) {
              sessions.push({
                serverId: server.id,
                databaseId: database.id,
                databaseName: database.name,
                projectUrl: database.projectUrl,
                observationId: observation.id,
                observationName: observation.name,
                mustFinishAt: observation.session.mustFinishAt,
                sessionId: observation.session.id,
              });
            }
          });
        }
      });
    }
  });

  return sessions;
};

/**
 * Sort an array of object in alphabeticall order
 * according to the value of the provided object key.
 * @param {array of objects} array  - Array to be sorted
 * @param {string} key              - Key of the object located in the array
 */
export const sortAlphabeticallyByKey = (array, key) =>
  array.sort((a, b) => {
    if (a[key] < b[key]) return -1;
    if (a[key] > b[key]) return 1;
    return 0;
  });

/**
 * Removes the accents of a string. (á => 'a => a)
 * @param {string} str
 */
export const removeAccents = (str) =>
  str.normalize('NFD').replace(/[\u0300-\u036f]/g, '');

/**
 * Return session object belonging to a specific observation.
 * Path in the hierarchy tree is determined by the arguments.
 * @param {string} serverId
 * @param {string} databaseId
 * @param {string} observationId
 */
export const getSessionByPath = (
  serverId,
  databaseId,
  observationId,
  serverList,
) => {
  let session = {};
  serverList.forEach((server) => {
    if (server.id === serverId && server.databases && server.databases.data) {
      server.databases.data.forEach((database) => {
        if (
          database.id === databaseId &&
          database.observations &&
          database.observations.data
        ) {
          database.observations.data.forEach((observation) => {
            if (observation.id === observationId) {
              // eslint-disable-next-line prefer-destructuring
              session = observation.session;
            }
          });
        }
      });
    }
  });
  return session;
};

export const getFilteredTrackLogListBySelectedDatabase = (
  trackLogList,
  databaseId = null,
  dateFilterOnMapSettings = null,
) =>
  // eslint-disable-next-line implicit-arrow-linebreak
  (databaseId &&
    trackLogList.filter(
      ({ selectedDatabases, startTime, trackLogArray }) =>
        (!dateFilterOnMapSettings ||
          (dateFilterOnMapSettings &&
            new Date(startTime) >
              new Date(dateFilterOnMapSettings?.startTime) &&
            new Date(startTime) <
              new Date(dateFilterOnMapSettings?.endTime))) &&
        selectedDatabases.some(({ databaseId: id }) => databaseId === id) &&
        isValidCoordinateArray(trackLogArray, 'trackLogList'),
    )) ||
  [];

export const getTrackLogById = (trackLogList, trackLogId) =>
  trackLogId && trackLogList.find(({ trackLogId: id }) => id === trackLogId);

export const getMeasurementsForMap = (
  serverList,
  serverId = null,
  databaseId = null,
  observationId = null,
  dateFilterOnMapSettings = null,
) => {
  const locations = [];
  serverList.forEach((server) => {
    if (
      (!serverId || server.id === serverId) &&
      server.databases &&
      server.databases.data
    ) {
      server.databases.data.forEach((database) => {
        if (
          (!databaseId || database.id === databaseId) &&
          database.observations &&
          database.observations.data
        ) {
          database.observations.data.forEach((observation) => {
            if (
              (!observationId || observation.id === observationId) &&
              observation.measurements &&
              observation.measurements.data
            ) {
              observation.measurements.data.forEach((measurement) => {
                if (
                  !dateFilterOnMapSettings ||
                  (dateFilterOnMapSettings &&
                    new Date(measurement.date) >
                      new Date(dateFilterOnMapSettings?.startTime) &&
                    new Date(measurement.date) <
                      new Date(dateFilterOnMapSettings?.endTime))
                ) {
                  // TODO the obm_geometry column is hardcoded here. Make sure to add a single coordinate, and not an array. further consultations needed
                  const { obm_geometry = {} } = measurement.data || {};
                  const coordinate = obm_geometry.wktType
                    ? obm_geometry.wktValue
                    : obm_geometry;
                  if (coordinate?.latitude && coordinate?.longitude) {
                    locations.push({
                      ...measurement,
                      coordinate,
                      serverId: server.id,
                      databaseId: database.id,
                      observationName: observation.name,
                      boldYellow: observation.form.boldYellow,
                      dataTypes: observation.form.dataTypes,
                      isMetaRecord: false,
                    });
                  } else if (measurement.meta?.observationListMetaRecord) {
                    locations.push({
                      ...measurement,
                      serverId: server.id,
                      databaseId: database.id,
                      observationName: observation.name,
                      boldYellow: observation.form.boldYellow,
                      dataTypes: observation.form.dataTypes,
                      isMetaRecord: true,
                    });
                  }
                }
              });
            }
          });
        }
      });
    }
  });
  return locations;
};

export const getNonEmptyDatabases = (serverList) => {
  const databases = [];
  serverList.forEach((server) => {
    if (server.databases && server.databases.data) {
      server.databases.data.forEach((database) => {
        if (database.observations && database.observations.data) {
          if (
            database.observations.data.some(
              (observation) =>
                observation.measurements &&
                observation.measurements.data &&
                observation.measurements.data.some(
                  (measurement) =>
                    measurement.data && measurement.data.obm_geometry,
                ),
            )
          ) {
            databases.push({
              value: database.name,
              key: database.id,
              serverId: server.id,
            });
          }
        }
      });
    }
  });
  return databases;
};

/**
 * Returns the current session tracklog. It can happen that the selectedObservations session has a tracklog, cached because of application has been closed
 * @param {*} selectedObservation
 * @param {*} sessionTrackLogs
 * @returns
 */
export const getCurrentSessionTrackLog = ({ session }, sessionTrackLogs) => {
  if (session && session.id) {
    const currentSessionTrackLog = sessionTrackLogs.find(
      ({ sessionId }) => sessionId === session?.id,
    );
    if (session.sessionTrackLog?.length) {
      return {
        ...currentSessionTrackLog,
        trackLogArray: [
          ...currentSessionTrackLog.trackLogArray,
          ...session.sessionTrackLog,
        ],
      };
    }
    return currentSessionTrackLog;
  }
  return null;
};

/**
 * Gets the stored permanentSamplePlots from the database according to the forms needs.
 * @param { permanentSamplePlots } database
 * @param { form: { permanentSamplePlotInfos }} observation
 * @returns
 */
export const getPermanentSamplePlots = (database, observation) =>
  database?.permanentSamplePlots?.filter(({ plotId }) =>
    observation?.form?.permanentSamplePlotInfos?.some(
      ({ infoId }) => plotId === infoId,
    ),
  ) || [];

export const measurementDataToArray = (measurement) => {
  const itemArray = [];
  if (measurement) {
    if (measurement.date) {
      itemArray.push({
        name: parseValueToString(measurement.date, { type: 'datetime' }),
      });
    }
    if (measurement.boldYellow.length) {
      measurement.boldYellow.forEach((boldColumn) => {
        if (
          measurement.data[boldColumn] &&
          measurement.dataTypes.find(({ column }) => column === boldColumn)
        ) {
          itemArray.push({
            name: parseValueToString(
              measurement.data[boldColumn],
              measurement.dataTypes.find(({ column }) => column === boldColumn),
            ),
          });
        }
      });
    }
  }
  return itemArray;
};

export const getMeasurementsBySession = (
  serverId,
  databaseId,
  observationId,
  sessionId,
  serverList,
) => {
  const measurements = [];

  serverList.forEach((server) => {
    if (server.id === serverId && server.databases && server.databases.data) {
      server.databases.data.forEach((database) => {
        if (
          database.id === databaseId &&
          database.observations &&
          database.observations.data
        ) {
          database.observations.data.forEach((observation) => {
            if (
              observation.id === observationId &&
              observation.measurements &&
              observation.measurements.data
            ) {
              observation.measurements.data.forEach((measurement) => {
                if (
                  measurement.data &&
                  measurement.meta.sessionId === sessionId
                ) {
                  measurements.push(measurement);
                }
              });
            }
          });
        }
      });
    }
  });

  return measurements;
};

export const getSessionMetaRecord = (
  serverId,
  databaseId,
  observationId,
  sessionId,
  serverList,
) => {
  let metaRecord = null;

  serverList.forEach((server) => {
    if (server.id === serverId && server.databases && server.databases.data) {
      server.databases.data.forEach((database) => {
        if (
          database.id === databaseId &&
          database.observations &&
          database.observations.data
        ) {
          database.observations.data.forEach((observation) => {
            if (
              observation.id === observationId &&
              observation.measurements &&
              observation.measurements.data
            ) {
              observation.measurements.data.forEach((measurement) => {
                if (
                  !measurement.data &&
                  measurement.meta.observationListMetaRecord &&
                  measurement.meta.sessionId === sessionId
                ) {
                  metaRecord = measurement;
                }
              });
            }
          });
        }
      });
    }
  });

  return metaRecord;
};

export const getAllMeasurementsBySession = (
  serverId,
  databaseId,
  sessionId,
  serverList,
) => {
  const measurements = [];

  serverList.forEach((server) => {
    if (server.id === serverId && server.databases && server.databases.data) {
      server.databases.data.forEach((database) => {
        if (
          database.id === databaseId &&
          database.observations &&
          database.observations.data
        ) {
          database.observations.data.forEach((observation) => {
            if (observation.measurements && observation.measurements.data) {
              observation.measurements.data.forEach((measurement) => {
                if (measurement.meta.sessionId === sessionId) {
                  measurements.push(measurement);
                }
              });
            }
          });
        }
      });
    }
  });

  return measurements;
};

/**
 * Generates a hexcolor from a string
 * @param {*} stringId
 */
export const hashStringToHexColor = ({ string, alpha = '80' }) => {
  let hash = 0;
  for (let i = 0; i < string.length; i++) {
    // eslint-disable-next-line no-bitwise
    hash = string.charCodeAt(i) + ((hash << 5) - hash);
  }

  // eslint-disable-next-line no-bitwise
  const c = (hash & 0x00ffffff).toString(16).toUpperCase();

  return `#${'00000'.substring(0, 6 - c.length) + c}${alpha || '80'}`;
};

export const createGpxFromTrackLog = (trackLog) =>
  createGpx(
    trackLog.trackLogArray.map((item) => ({
      ...item,
      timeStamp: new Date(item.timestamp).toISOString(),
    })),
    {
      activityName: trackLog.trackLogName,
      startTime: new Date(trackLog.startTime).toISOString(),
      timeKey: 'timeStamp',
    },
  );

/**
|--------------------------------------------------
| TRACKLOG REDUCER DATA TRANSFORMATIONS
|--------------------------------------------------
*/

/**
 * Generates a tracklog name. A general tracklogs name consosts of the start and end time.
 * If the tracklog is a session tracklog, adds additional projectname and other data.
 * @param {date} startTime
 * @param {date} endTime
 * @param {array of objects} selectedDatabases
 * @param {string} sessionId
 */
export const generateTrackLogName = ({
  startTime,
  endTime,
  selectedDatabases = [],
  sessionId,
}) =>
  // eslint-disable-next-line implicit-arrow-linebreak
  selectedDatabases.length && sessionId
    ? formatToFilename(
        `${selectedDatabases[0].databaseName}_${
          selectedDatabases[0].observationName
        }_${generateFormattedNameFromStartAndEndTime(
          startTime,
          endTime,
        )}`.replace(/ /g, ''),
      )
    : formatToFilename(
        generateFormattedNameFromStartAndEndTime(startTime, endTime),
      );

/**
 * Sets the syncing prop of the tracklogs in the wholeTrackLogList, which are contained in the trackLogsToSet array.
 * @param {array of objects} trackLogsToSet
 * @param {array of objects} wholeTrackLogList
 * @param {boolean} value
 */
export const setTrackLogsSyncingProp = (
  trackLogsToSet,
  wholeTrackLogList,
  value,
) =>
  wholeTrackLogList.map((trackLog) =>
    // eslint-disable-next-line implicit-arrow-linebreak
    trackLogsToSet.some(({ trackLogId: id }) => id === trackLog.trackLogId)
      ? { ...trackLog, syncing: value }
      : trackLog,
  );

/**
 * Searches the result from the syncResult array relevant to the tracklog in the wholeTrackLogList and sets the isSynced prop and the error
 * of the database contained in the selectedDatabases array in the tracklog. A tracklog can have several databases it needs to upload, thats why we need
 * to maintain the isSynced prop and the errors separately in each related database.
 * @param {array of objects} syncResults
 * @param {array of objects} wholeTrackLogList
 * @param {boolean} syncSuccessful
 */
export const setTrackLogSyncResults = (
  syncResults,
  wholeTrackLogList,
  syncSuccessful,
) =>
  wholeTrackLogList.map((observedTrackLog) => {
    const syncResultsOfObservedTrackLog = syncResults.filter(
      ({ trackLogId: id }) => id === observedTrackLog.trackLogId,
    );

    if (syncResultsOfObservedTrackLog.length > 0) {
      const newTrackLogToReturn = { ...observedTrackLog, syncing: false };
      syncResultsOfObservedTrackLog.forEach(
        ({ databaseId, serverId, payload }) => {
          observedTrackLog.selectedDatabases.forEach((database, index) => {
            if (
              databaseId === database.databaseId &&
              serverId === database.serverId
            ) {
              newTrackLogToReturn.selectedDatabases[index] = {
                ...newTrackLogToReturn.selectedDatabases[index],
                isSynced: syncSuccessful,
                error: syncSuccessful ? null : payload,
              };
            }
          });
        },
      );
      return newTrackLogToReturn;
    }

    return observedTrackLog;
  });

/**
 * Seaches for session metarecords, and if found, do the following:
 * If the metarecord indicates that the session tracklog is uploaded or deleted, do nothing
 * Else, searches the relevant trackLog in the TrackLogList.
 * If found, sets the trackLogInfo to FOUND state and copies the tracklog into the metarecord
 * If not found, sets the trackLogInfo to NOT_FOUND:
 * @param {array} measurementList
 * @param {array} trackLogList
 * @returns
 */
export const setMeasurementMetaRecordsTrackLog = (
  measurementList,
  trackLogList,
) =>
  measurementList.map((measurementObject) => {
    const {
      measurement: {
        meta: {
          observationListMetaRecord,
          sessionId,
          sessionTrackLog,
          trackLogInfo,
        } = {},
      } = {},
    } = measurementObject;
    if (
      !observationListMetaRecord ||
      !sessionId ||
      [
        SESSION_TRACKLOG_META_INFO.UPLOADED,
        SESSION_TRACKLOG_META_INFO.DELETED,
      ].includes(trackLogInfo)
    )
      return measurementObject;

    let newTrackLogInfo = trackLogInfo;
    let newTrackLogArray = sessionTrackLog;

    const relatedSessionTrackLog = trackLogList.find(
      ({ sessionId: sessionIdInTrackLog }) => sessionIdInTrackLog === sessionId,
    );
    if (relatedSessionTrackLog?.trackLogArray) {
      newTrackLogInfo = SESSION_TRACKLOG_META_INFO.FOUND;
      newTrackLogArray = relatedSessionTrackLog.trackLogArray;
    } else {
      newTrackLogInfo = SESSION_TRACKLOG_META_INFO.NOT_FOUND;
    }

    return {
      ...measurementObject,
      measurement: {
        ...measurementObject.measurement,
        meta: {
          ...measurementObject.measurement.meta,
          trackLogInfo: newTrackLogInfo,
          sessionTrackLog: newTrackLogArray,
        },
      },
    };
  });

/**
 * Checks whether the new database is already in the selectedDatabases
 * @param {array of objects} selectedDatabases
 * @param {object} newDatabase
 * @returns
 */
export const isDatabaseInSelectedDatabases = (selectedDatabases, newDatabase) =>
  // eslint-disable-next-line implicit-arrow-linebreak
  selectedDatabases.some(
    ({ serverId, databaseId }) =>
      serverId === newDatabase.serverId &&
      databaseId === newDatabase.databaseId,
  );

export const isGeometryValueValid = ({
  type,
  geometryValue,
  toastNeeded = false,
  minSize = 1,
}) => {
  if (!geometryValue || (type === 'point') === Array.isArray(geometryValue))
    return false;

  if (type === 'line' && (geometryValue?.length || 0) < 2) {
    if (toastNeeded) Toast.show(I18n.t('error_line_length'), Toast.LONG);
    return (geometryValue?.length || 0) >= minSize;
  }
  if (type === 'polygon' && (geometryValue?.length || 0) < 3) {
    if (toastNeeded) Toast.show(I18n.t('error_polygon_length'), Toast.LONG);
    return (geometryValue?.length || 0) >= minSize;
  }
  return true;
};

/**
 * Checks whether there is a database in the selectedDatabases which belongs to the same server as the new Database
 * @param {array} selectedDatabases
 * @param {object} newDatabase
 * @returns
 */
export const isServerOfDatabaseInSelectedDatabases = (
  selectedDatabases,
  newDatabase,
) =>
  // eslint-disable-next-line implicit-arrow-linebreak
  selectedDatabases.some(({ serverId }) => serverId === newDatabase.serverId);
/**
|--------------------------------------------------
| MOCKED DATA GENERATION
|--------------------------------------------------
*/

export const generateMockedSimpleTracklog = (coord, originalTrackLog) => {
  const mockedTrackLog = [];
  for (let i = 0; i < originalTrackLog.length; i++) {
    mockedTrackLog.push({
      latitude: coord.latitude + i * 0.0005 * ((-1 * i) % 2),
      longitude: coord.longitude + i * 0.0005,
      timeStamp: originalTrackLog[i].timeStamp,
    });
  }
  return mockedTrackLog;
};

export const generateMockedSessionTrackLog = (originalTrackLog) => {
  const mockedTrackLog = [];
  for (let i = 0; i < originalTrackLog.length; i++) {
    mockedTrackLog.push({
      latitude: originalTrackLog[i].latitude + i * 0.0005 * ((-1 * i) % 2),
      longitude: originalTrackLog[i].longitude + i * 0.0005,
      timeStamp: originalTrackLog[i].timeStamp,
    });
  }
  return mockedTrackLog;
};

export * from './gpsUtils';
