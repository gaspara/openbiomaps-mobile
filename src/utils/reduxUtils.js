import { ToastAndroid } from 'react-native';
import { store } from '../config/setupStore';
import { LANG } from '../Constants';
import I18n from '../i18n';
import { getUsedAttachedFileUris, formatBytes } from './index';
import { deleteFile, getAttachedFileDirContent, getFileInfo } from './fileSystemUtils';
/**
 * Returns the currently used language, which could be the language
 * defined by the user in the settings, or the default phone/fallback language.
 * @return { id, value } - e.g. { id: 'en', value: 'English' }
 */
export const getCurrentLanguage = () => {
  const { language } = store.getState().settings;
  // Return the user defined language if provided.
  if (language.id) return language;
    
  let [langId] = I18n.currentLocale().split('-');
  if (!LANG.some((langItem) => langItem.id === langId)) {
    // Set the fallback language if the current phone language is not supported.
    langId = I18n.defaultLocale;
  }
  return LANG.find((langItem) => langItem.id === langId);
};

export const removeUnusedAttachedFiles = async (fileUris) => {
  if (!fileUris.length) return;
  const {
    servers: { data: serverList },
  } = store.getState();

  const usedAttachedFileUris = getUsedAttachedFileUris(serverList);
  fileUris.forEach((uri) => {
    if (!usedAttachedFileUris.some((usedUri) => usedUri === uri)) {
      deleteFile(uri);
    }
  });
};

export const removeAllUnusedFiles = async (showToast = true) => {
  const {
    servers: { data: serverList },
  } = store.getState();

  const usedAttachedFileUris = getUsedAttachedFileUris(serverList);
  const attachedFileDirContent = await getAttachedFileDirContent();
  const result = await Promise.all(attachedFileDirContent.map(async (existingFileUri) => {
    if (!usedAttachedFileUris.some((savedFileUri) => savedFileUri === existingFileUri)) {
      const fileInfo = await getFileInfo(existingFileUri);
      if (fileInfo.exists) {
        const delResult = await deleteFile(existingFileUri);
        if (delResult) {
          return fileInfo.size;
        }
      }
    }
    return 0;
  }));
  const deletedSize = formatBytes(result.reduce((acc, cur) => acc + cur, 0));
  if (showToast) ToastAndroid.show(I18n.t('files_deleted_with_size', { size: deletedSize }), ToastAndroid.SHORT);
};

export const removeUnusedAttachedFilesOfMeasurement = (measurementData) => {
  const {
    observation,
    measurement,
  } = measurementData;
  const attachedFileUris = [];
  observation.form?.dataTypes?.forEach((dataType) => {
    if (dataType.type === 'file_id' && dataType.column && measurement.data && measurement.data[dataType.column]?.length) {
      attachedFileUris.push(measurement.data[dataType.column].map((file) => file.uri));
    }
  });

  const flatted = attachedFileUris.flat();
  if (flatted.length) {
    removeUnusedAttachedFiles(flatted);
  }
};
