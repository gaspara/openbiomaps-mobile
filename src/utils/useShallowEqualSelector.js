import { useSelector, shallowEqual } from 'react-redux';

export const useShallowEqualSelector = (selector, { enabled = true } = {}) => {
  const equalityFn = enabled ? shallowEqual : () => true;
  return useSelector(selector, equalityFn);
};
