import { MAP_ENGINE } from '../Constants';

/**
 * This file contains the migration functions neccessary to upgrade redux store,
 * when new states are introduced.
 */

// Deletes unused permanentSamplePlots object from the forms
const migrateToVersion2 = (state) => {
  const migratedServerList = state.servers.data.map((server) => {
    if (server.databases && server.databases.data) {
      const migratedDatabaseList = server.databases.data.map((database) => {
        if (database.observations && database.observations.data) {
          const migratedObservationList = database.observations.data.map((observation) => {
            if (observation.form && observation.form.dataTypes) {
              const form = { ...observation.form };
              delete form.permanentSamplePlots;
              return {
                ...observation,
                form,
              };
            }
            return observation;
          });
          return {
            ...database,
            observations: {
              ...database.observations,
              data: migratedObservationList,
            },
          };
        }
        return database;
      });
      return {
        ...server,
        databases: { 
          ...server.databases,
          data: migratedDatabaseList,
        },
      };
    }
    return server;
  });

  return {
    ...state,
    servers: {
      ...state.servers,
      data: migratedServerList,
    },
  };
};

const migrateToVersion1 = (state) => {
  const migratedServerList = state.servers.data.map((server) => {
    if (server.databases && server.databases.data) {
      const migratedDatabaseList = server.databases.data.map((database) => {
        if (database.observations && database.observations.data) {
          const migratedObservationList = database.observations.data.map((observation) => {
            if (observation.form && observation.form.dataTypes) {
              const migratedDataTypes = observation.form.dataTypes.map((dataType) => {
                if (dataType.type === 'list' && !Array.isArray(dataType.list)) {
                  const listInArrayFormat = [];
                  const hasEmptyStringDefaultValue = dataType.list[''] === '';
                  Object.entries(dataType.list).forEach(([key, value]) => {
                    if (!hasEmptyStringDefaultValue || key !== '' || value !== '') {
                      listInArrayFormat.push({ [key]: value });
                    }
                  });
                  if (hasEmptyStringDefaultValue) {
                    listInArrayFormat.unshift({ '': '' });
                  }
                  return {
                    ...dataType,
                    list: listInArrayFormat,
                  };
                }
                return dataType;
              });
              return {
                ...observation,
                form: {
                  ...observation.form,
                  dataTypes: migratedDataTypes,
                },
              };
            }
            return observation;
          });
          return {
            ...database,
            observations: {
              ...database.observations,
              data: migratedObservationList,
            },
          };
        }
        return database;
      });
      return {
        ...server,
        databases: { 
          ...server.databases,
          data: migratedDatabaseList,
        },
      };
    }
    return server;
  });

  return {
    ...state,
    servers: {
      ...state.servers,
      data: migratedServerList,
    },
  };
};

const migrateToVersion0 = (state) => {
  let synced = 0;
  let unsynced = 0;

  state.servers.data.forEach((server) => {
    if (server.databases && server.databases.data) {
      server.databases.data.forEach((database) => {
        if (database.observations && database.observations.data) {
          database.observations.data.forEach((observation) => {
            if (observation.measurements && observation.measurements.data) {
              observation.measurements.data.forEach((measurement) => {
                if (measurement.data) {
                  if (measurement.isSynced && !measurement.errors) {
                    synced++;
                  } else {
                    unsynced++;
                  }
                }
              });
            }
          });
        }
      });
    }
  });

  return {
    ...state,
    measurementsMeta: {
      synced,
      unsynced,
    },
    settings: {
      ...state.settings,
      mapEngine: {
        id: MAP_ENGINE[0].id,
        value: MAP_ENGINE[0].value,
      },
    },
  };
};

export const migrations = {
  0: migrateToVersion0,
  1: migrateToVersion1,
  2: migrateToVersion2,
};
