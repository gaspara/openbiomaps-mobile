import React, { Component } from 'react';
import {
  Platform,
  View,
  BackHandler,
  InteractionManager,
  KeyboardAvoidingView,
} from 'react-native';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  activateKeepAwake,
  deactivateKeepAwake,
} from '@sayem314/react-native-keep-awake';
import _ from 'lodash';

import * as actions from '../../actions';
import {
  getFormState,
  getSelectedServer,
  getSelectedDatabase,
  getSelectedObservation,
} from '../../utils';
import {
  MeasurementForm,
  Spinner,
  footerStyle,
  TouchableIcon,
  Timer,
  SessionOnlyBlocker,
  EmptyList,
  showAlertDialog,
  NotificationHandler,
  PeriodicNotificationTimer,
} from '../../components';
import NavigationHandler from '../../NavigationHandler';
import { FormContainer } from '../../components/MeasurementForm/styles';
import I18n from '../../i18n';
import {
  NotificationModel,
  NOTIFICATION_TYPES,
} from '../../models/NotificationModel';
import {
  ERROR,
  GEOMETRY_TYPES,
  GPS_OBSERVER,
  PIN_SAVE_LOGIC,
} from '../../Constants';
import StateFinder from '../../StateFinder';
import withGpsSubscription, {
  GpsDefaultPropTypes,
  GpsPropTypes,
} from '../../hocs/withGpsSubscription';
// TODO: összes session elem törlés után a metarekordot is törölni, vagy nullrekordot készíteni. - MIKI

class CreateMeasurementScreen extends Component {
  // eslint-disable-next-line
  _didFocusSubscription;

  _willBlurSubscription;

  constructor(props) {
    super(props);
    const {
      navigation,
      selectedObservation: { session },
    } = props;
    this._didFocusSubscription = navigation.addListener('didFocus', () => {
      this.backPressListener = BackHandler.addEventListener(
        'hardwareBackPress',
        this.onAndroidBackPress,
      );
    });

    this.state = {
      isModified: false,
      didFinishInitialAnimation: false,
      toggleSession: session && session !== null,
      showObservationListCommonFields: false,
      isNullRecord: false,
      showTimer: true,
      metaRecordSessionId: null,
      showPeriodicNotificationTimer: false,
      periodicNotificationStartDate: null,
      showPeriodicNotificationCountDownTimer: false,
    };

    // Component references
    this.formRef = null;
    this.notificationHandler = null;
    this.backPressListener = null;
  }

  componentDidMount = () => {
    activateKeepAwake();
    const { navigation } = this.props;
    navigation.setParams({ onHeaderBackPress: this.onHeaderBackPress });
    this._willBlurSubscription = navigation.addListener('willBlur', () =>
      BackHandler.removeEventListener(
        'hardwareBackPress',
        this.onAndroidBackPress,
      ),
    );

    // Delay initial fetching/rendering in order to improve navigaiton performance.
    InteractionManager.runAfterInteractions(() => {
      setTimeout(() => {
        this.setState({ didFinishInitialAnimation: true });
        this.loadFormData();
        this.startPeriodicNotificationTimer();
      }, 500);
    });
    this.subribeToGps();
  };

  componentDidUpdate = (prevProps) => {
    const { isFormRemovedFromServer, measurement, fetching, form, gps } =
      this.props;

    if (this.notificationHandler) {
      if (isFormRemovedFromServer) {
        this.showNotification(
          NotificationModel.build(NOTIFICATION_TYPES.FORM_UNAVAILABLE),
        );
      } else if (measurement?.meta) {
        const {
          isSynced,
          meta: { sessionId },
        } = measurement;
        if (!isSynced && sessionId) {
          this.showNotification(
            NotificationModel.build(NOTIFICATION_TYPES.COPYING_SESSION_ELEMENT),
          );
        }
      }
    }

    if (prevProps.fetching && !fetching && form) {
      this.startPeriodicNotificationTimer();
    }

    this.subribeToGps();
  };

  componentWillUnmount = () => {
    const { gps } = this.props;

    if (this._didFocusSubscription) this._didFocusSubscription.remove();
    if (this._willBlurSubscription) this._willBlurSubscription.remove();
    this.backPressListener?.remove();
    deactivateKeepAwake();
    gps.unsubscribe();
  };

  getFormData = () => {
    const { showObservationListCommonFields, isNullRecord } = this.state;
    const {
      selectedObservation: { session },
      form,
      measurement: copiedMeasurement,
    } = this.props;

    if (session) {
      if (showObservationListCommonFields) {
        return form.filter(
          (item) =>
            item.apiParams.hidden === 'off' && item.apiParams.once === 'on',
        );
      }
      if (isNullRecord) {
        return form.filter(
          (item) =>
            item.apiParams.hidden === 'off' ||
            item.apiParams.auto_geometry === 'on',
        );
      }
      if (
        !_.isEmpty(copiedMeasurement) &&
        copiedMeasurement?.meta?.sessionId !== session.id
      ) {
        return form.filter((item) => item.apiParams.hidden === 'off');
      }
      return form.filter(
        (item) =>
          item.apiParams.hidden === 'off' && item.apiParams.once === 'off',
      );
    }

    return form.filter(
      (item) =>
        item.apiParams.hidden === 'off' ||
        item.apiParams.auto_geometry === 'on',
    );
  };

  onAndroidBackPress = () => {
    const { isModified, isNullRecord } = this.state;
    const { measurement } = this.props;

    if (isModified || isNullRecord) {
      this.showExitModal();
      return true;
    }
    if (!_.isEmpty(measurement)) {
      NavigationHandler.replace('ObservationCards');
      return true;
    }
    return false;
  };

  onHeaderBackPress = () => {
    const { isModified, isNullRecord } = this.state;
    const { measurement } = this.props;

    if (isModified || isNullRecord) {
      this.showExitModal();
    } else if (!_.isEmpty(measurement)) {
      NavigationHandler.replace('ObservationCards');
    } else {
      NavigationHandler.goBack();
    }
  };

  onHomePress = () => {
    const { isModified, isNullRecord } = this.state;
    if (isModified || isNullRecord) {
      this.showExitModal('Home');
    } else {
      NavigationHandler.navigate('Home');
    }
  };

  onSavePress = (measurement) => {
    const {
      showObservationListCommonFields,
      isNullRecord,
      metaRecordSessionId,
    } = this.state;
    if (showObservationListCommonFields) {
      this.saveList(measurement.data, false); // a measurement.data commonFields lesz a finishObservationList-ben
    } else if (isNullRecord) {
      // Amikor üres lista lezárás után mentünk, akkor
      // elmentjük a NULL rekordnak felvett adatot
      this.saveMeasurement(measurement);
      // módosítjuk felvett rekordok számát a korábban elkészített sessionMeta rokordban
      this.updateMeasurementNum(metaRecordSessionId);
      // eldobjuk a sessionId-t
      this.setState({ metaRecordSessionId: null });
    } else {
      this.saveMeasurement(measurement);
    }
  };

  onSaveFailed = () => {
    this.notificationHandler.hideNotification(
      NotificationModel.build(NOTIFICATION_TYPES.CREATE_FORM_PROGRESS),
    );
  };

  /**
   * Hidden form items shouldn't be displayed in the form, however the default values
   * of them should be sent to the api, hence the underlying logic.
   */
  addHiddenValuesToMeasurement = (measurement) => {
    if (measurement === null) return undefined;
    const { form } = this.props;
    const modifiedMeasurement = { ...measurement };

    form.forEach(({ apiParams, column, defaultValue, type }) => {
      if (apiParams.hidden === 'on' && apiParams.auto_geometry !== 'on') {
        modifiedMeasurement.data[column] = defaultValue;
      }

      if (
        apiParams.hidden === 'on' &&
        (type === 'time' || type === 'date' || type === 'datetime')
      ) {
        modifiedMeasurement.data[column] = new Date();
      }
    });

    return modifiedMeasurement;
  };

  // Metarekordban utólag beállítjuk a felvett rekordok számát
  updateMeasurementNum = (metaRecordSessionId) => {
    const {
      selectedServer: { id: serverId },
      selectedDatabase: { id: databaseId },
      selectedObservation: { id: observationId },
      updateMeasurementsMetaNum,
    } = this.props;
    updateMeasurementsMetaNum(
      serverId,
      databaseId,
      observationId,
      metaRecordSessionId,
    ); // actions/session.js
  };

  // felvett rekord elmentése
  saveMeasurement = (measurement) => {
    const { isNullRecord } = this.state;
    const {
      selectedServer,
      selectedDatabase,
      selectedObservation,
      createMeasurement,
      hasEnoughMemory,
      memoryUsageCritical,
      form,
      saveLastPickedColumnValues,
      isSuccessNotificationSound,
      measurement: originalMeasurement,
    } = this.props;

    if (!hasEnoughMemory) {
      this.showNotification(
        NotificationModel.build(NOTIFICATION_TYPES.NO_MEMORY),
      );
    } else {
      if (memoryUsageCritical) {
        this.showNotification(
          NotificationModel.build(NOTIFICATION_TYPES.CRITICAL_MEMORY_USAGE),
        );
      }

      this.setState({
        showPeriodicNotificationCountDownTimer: false,
      });

      form.forEach(({ type, column, list_definition }) => {
        if (
          (type === 'autocomplete' ||
            type === 'autocompletelist' ||
            (type === 'list' && list_definition?.multiselect)) &&
          measurement.data[column]
        ) {
          const filteredValues = Array.isArray(measurement.data[column])
            ? measurement.data[column].filter((i) => i)
            : measurement.data[column];
          if (filteredValues.length) {
            saveLastPickedColumnValues(
              selectedServer.id,
              selectedDatabase.id,
              selectedObservation.id,
              {
                column,
                value: filteredValues,
              },
            );
          }
        }
      });

      // if the user copies a measurement, and if it is a session element, add sessionId to the measurement
      if (!_.isEmpty(originalMeasurement)) {
        // if the copied element is synced already, then must not get a sessionId
        const newMetaSessionId = originalMeasurement?.isSynced
          ? null
          : originalMeasurement?.meta?.sessionId;
        createMeasurement(
          selectedServer.id,
          selectedDatabase.id,
          selectedObservation.id,
          this.addHiddenValuesToMeasurement({
            ...measurement,
            meta: {
              ...measurement.meta,
              sessionId: newMetaSessionId,
            },
          }),
        );
        this.updateMeasurementNum(originalMeasurement.meta.sessionId);
      } else {
        this.setState({
          showPeriodicNotificationCountDownTimer: true,
        });
        createMeasurement(
          selectedServer.id,
          selectedDatabase.id,
          selectedObservation.id,
          this.addHiddenValuesToMeasurement(measurement),
        );
      }

      if (isNullRecord) {
        this.hideNotification(
          NotificationModel.build(NOTIFICATION_TYPES.NULL_RECORD),
        );
        this.setState({ isNullRecord: false });
      }

      if (selectedDatabase.game === 'off') {
        this.setState({ isModified: false }, () => {
          this.hideNotification(
            NotificationModel.build(NOTIFICATION_TYPES.CREATE_FORM_PROGRESS),
          );
          this.showNotification(
            NotificationModel.build(
              NOTIFICATION_TYPES.CREATE_FORM_SUCCESS,
              null,
              3000,
              isSuccessNotificationSound,
            ),
          );

          if (this.formRef) {
            // When JS thread is busy autoscroll is not working, hence, the setTimeout.
            setTimeout(() => this.formRef?.scrollToIndex({ index: 0 }), 0);
          }
        });
      }
    }
  };

  // Listás adatfelvétel zárása
  //    Közös mezők hozzáírása
  //    Null rekord felvétel
  //    Normál lezárás
  saveList = (commonFields, creatingNullRecord) => {
    // A nullRecordData most már mindig null!

    const { showObservationListCommonFields } = this.state;
    const finishedSessionId = this.finishObservationList(
      true,
      commonFields,
      null,
    );

    // Minden felvett rekordhoz hozzá lesz adva az itt felvett mezők értéke
    if (showObservationListCommonFields) {
      this.hideNotification(
        NotificationModel.build(NOTIFICATION_TYPES.COMMON_FIELDS),
      );
      this.setState({ showObservationListCommonFields: false });
    }
    if (!creatingNullRecord) {
      this.setState({ isNullRecord: false });
    } else {
      this.setState({ metaRecordSessionId: finishedSessionId });
    }

    this.updateMeasurementNum(finishedSessionId);
  };

  subribeToGps() {
    const { form, gps } = this.props;
    if (form) {
      const hasGeometryType = form.some((item) =>
        GEOMETRY_TYPES.includes(item.type),
      );
      if (hasGeometryType) {
        gps.subscribe();
      }
    }
  }

  hasCommonObservationListFields = () => {
    const { form } = this.props;
    return form?.some(
      (item) => item.apiParams.once === 'on' && item.apiParams.hidden === 'off',
    );
  };

  toggleSession = () => {
    const {
      selectedObservation: { session },
    } = this.props;
    const { toggleSession } = this.state;

    if (!toggleSession) {
      this.setState({
        showTimer: true,
        toggleSession: true,
      });
      this.startObservationList();
    } else if (!session.measurementsNum) {
      // nincsenek felvett adatok, null rekord képzése
      this.showEmptyObservationListFinishedDialog(); // --> setState({ toggleSession: !this.state.toggleSession }); setState({ isNullRecord: true })
    } else if (this.hasCommonObservationListFields()) {
      this.showSaveListDialog(); // --> setState({ showObservationListCommonFields: true });
    } else {
      // egyszerű listalezárás, volt felvéve adat és nincsenek közös mezők
      this.setState({ toggleSession: false });
      this.saveList(null, false);
    }
  };

  showSaveListDialog = () => {
    showAlertDialog({
      title: I18n.t('do_you_want_to_save_the_list'),
      positiveLabel: I18n.t('ok'),
      showCancelButton: true,
      onPositiveBtnPress: this.showObservationListCommonFields,
    });
  };

  startObservationList = () => {
    const {
      startSession,
      selectedServer: { id: serverId },
      selectedDatabase: { id: databaseId },
      selectedObservation: {
        id: observationId,
        form: { listTime = null, trackLogMode = '' },
      },
    } = this.props;
    const startedAt = new Date().getTime();

    if (trackLogMode === '') {
      showAlertDialog({
        title: I18n.t('session_tracklog_alert_title'),
        message: I18n.t('session_tracklog_optional_description'),
        onPositiveBtnPress: () =>
          startSession(
            serverId,
            databaseId,
            observationId,
            startedAt,
            listTime,
            true,
          ),
        onNegativeBtnPress: () =>
          startSession(
            serverId,
            databaseId,
            observationId,
            startedAt,
            listTime,
            false,
          ),
        positiveLabel: I18n.t('session_tracklog_record'),
        negativeLabel: I18n.t('session_tracklog_dont_record'),
        showCancelButton: true,
        cancelable: false,
      });
    } else if (trackLogMode === 'force') {
      showAlertDialog({
        title: I18n.t('session_tracklog_alert_title'),
        message: I18n.t('session_tracklog_recording_started'),
        positiveLabel: I18n.t('ok'),
      });
      startSession(
        serverId,
        databaseId,
        observationId,
        startedAt,
        listTime,
        true,
      );
    } else if (trackLogMode === 'false') {
      startSession(
        serverId,
        databaseId,
        observationId,
        startedAt,
        listTime,
        false,
      );
    }
  };

  startPeriodicNotificationTimer() {
    const {
      selectedObservation: {
        form: { periodicNotificationTime = null },
      },
    } = this.props;

    if (
      periodicNotificationTime &&
      periodicNotificationTime != null &&
      periodicNotificationTime > 0
    ) {
      const periodicNotificationStartDate = new Date().getTime();

      this.setState({
        showPeriodicNotificationTimer: true,
        showPeriodicNotificationCountDownTimer: true,
        periodicNotificationStartDate: periodicNotificationStartDate,
      });
    }
  }

  finishObservationList = (
    shouldRecordMetaRecord,
    commonFields /* , nullRecordData */,
  ) => {
    const {
      endSession,
      selectedServer: { id: serverId },
      selectedDatabase: { id: databaseId },
      selectedObservation: { id: observationId },
      isAutoSyncEnabled,
      tokenRefresh,
    } = this.props;

    const finishedSessionId = endSession(
      serverId,
      databaseId,
      // A nullRecordData most  már mindig null!
      observationId,
      shouldRecordMetaRecord,
      commonFields,
      null,
    ); // actions/session.js

    // ez mindig true, ha a saveList-ből jön, csak a discardEmptyObservationList esetén false
    // azaz mindig kiírja, hogy sikerült a list rögzítés
    if (shouldRecordMetaRecord) {
      this.showNotification(
        NotificationModel.build(
          NOTIFICATION_TYPES.CREATE_LIST_SUCCESS,
          null,
          3000,
        ),
      );
      if (isAutoSyncEnabled) {
        tokenRefresh(serverId);
      }
    }
    return finishedSessionId;
  };

  showEmptyObservationListFinishedDialog = () => {
    showAlertDialog({
      title: I18n.t('do_you_want_to_save_the_list'),
      message: I18n.t('no_data_recorded_in_the_list'),
      showCancelButton: true,
      positiveLabel: I18n.t('record_empty_observation'),
      negativeLabel: I18n.t('do_not_save_the_list'),
      onPositiveBtnPress: this.showNullRecordCreationDialog,
      onNegativeBtnPress: this.discardEmptyObservationList,
    });
  };

  showNullRecordCreationDialog = () => {
    this.setState({
      showTimer: false,
      toggleSession: false,
      isNullRecord: true,
    });
    showAlertDialog({
      title: I18n.t('create_null_record'),
      message: I18n.t('create_null_record_instruction'),
      positiveLabel: I18n.t('ok'),
      onPositiveBtnPress: this.recordEmptyObservationList,
    });
    this.saveList(null, true);
  };

  recordEmptyObservationList = () => {
    this.showNotification(
      NotificationModel.build(NOTIFICATION_TYPES.NULL_RECORD),
    );
  };

  discardEmptyObservationList = () => {
    this.setState({ toggleSession: false });
    this.finishObservationList(false, null, null);
  };

  showObservationListCommonFields = () => {
    this.setState({
      toggleSession: false,
      showTimer: false,
      showObservationListCommonFields: true,
    });
    this.showNotification(
      NotificationModel.build(NOTIFICATION_TYPES.COMMON_FIELDS),
    );
  };

  hideNotification = (notification) => {
    this.notificationHandler.hideNotification(notification);
  };

  showNotification = (notification) => {
    this.notificationHandler.showNotification(notification);
  };

  showExitModal = (routeName) => {
    showAlertDialog({
      title: I18n.t('have_unsaved_changes'),
      message: I18n.t('do_you_want_to_exit'),
      showCancelButton: true,
      positiveLabel: I18n.t('bool_true'),
      negativeLabel: I18n.t('bool_false'),
      onPositiveBtnPress: routeName
        ? () => {
            NavigationHandler.navigate(routeName);
          }
        : () => NavigationHandler.goBack(),
    });
  };

  formDataChangeListener = (isModified) => {
    this.setState({ isModified });
  };

  loadFormData = () => {
    const {
      loadFormData,
      selectedServer,
      selectedDatabase,
      selectedObservation,
    } = this.props;
    loadFormData(selectedServer, selectedDatabase, selectedObservation);
  };

  /**
   * Generate unique key for instantiating new MeasurementForm components
   */
  generateMeasurementFormComponentKey = () => {
    const { showObservationListCommonFields, isNullRecord } = this.state;
    const {
      selectedObservation: { session },
    } = this.props;

    if (session) {
      if (showObservationListCommonFields) {
        return 'observation_list_common_fields';
      }
      if (isNullRecord) {
        return 'observation_list_null_record';
      }
      return 'observation_list_form';
    }

    return 'normal_form';
  };

  renderSessionTimer = () => {
    const { showTimer } = this.state;
    const { selectedObservation, measurement: copiedMeasurement } = this.props;

    if (
      copiedMeasurement?.meta &&
      copiedMeasurement.meta?.sessionId !== selectedObservation.session?.id
    ) {
      return null;
    }
    return (
      <Timer
        startedAt={selectedObservation.session?.startedAt}
        showTimer={showTimer}
        label={I18n.t('list_creation_in_progress')}
      />
    );
  };

  renderPeriodNotificationTimer = () => {
    const {
      selectedObservation: {
        form: { periodicNotificationTime = null },
      },
    } = this.props;

    const {
      showPeriodicNotificationTimer,
      periodicNotificationStartDate,
      showPeriodicNotificationCountDownTimer,
    } = this.state;

    return (
      <PeriodicNotificationTimer
        showPeriodicNotificationTimer={showPeriodicNotificationTimer}
        showPeriodicNotificationCountDownTimer={
          showPeriodicNotificationCountDownTimer
        }
        countDownTime={periodicNotificationTime}
        startTimer={periodicNotificationStartDate}
      />
    );
  };

  renderContent = () => {
    const {
      fetching,
      form,
      navigation,
      selectedServer,
      selectedDatabase,
      selectedObservation,
      measurement,
      isFormRemovedFromServer,
      savedStickinesses,
      lastPickedValues,
    } = this.props;
    const { showObservationListCommonFields, didFinishInitialAnimation } =
      this.state;

    const formData = this.getFormData();
    const key = this.generateMeasurementFormComponentKey();

    const shouldShowSpinner =
      (!form.length && fetching) || !didFinishInitialAnimation;
    const shouldShowEmptyList = !form.length || isFormRemovedFromServer;

    if (shouldShowSpinner) return <Spinner />;
    if (shouldShowEmptyList)
      return <EmptyList onRefreshPress={this.loadFormData} />;

    return (
      <MeasurementForm
        onDonePress={this.onSavePress}
        navigation={navigation}
        formDataChangeListener={this.formDataChangeListener}
        values={measurement.data}
        server={selectedServer}
        database={selectedDatabase}
        observation={selectedObservation}
        form={formData}
        lastMeasurement={selectedObservation.lastMeasurement}
        getFormRef={(ref) => {
          this.formRef = ref;
        }}
        showObservationListCommonFields={showObservationListCommonFields}
        key={key}
        savedStickinesses={savedStickinesses}
        lastPickedValues={lastPickedValues}
        onSaveFailed={this.onSaveFailed}
      />
    );
  };

  renderSessionOnlyBlocker = () => {
    const {
      navigation,
      selectedServer,
      selectedDatabase,
      selectedObservation,
    } = this.props;

    return (
      <SessionOnlyBlocker
        onPress={this.toggleSession}
        navigation={navigation}
        server={selectedServer}
        database={selectedDatabase}
        observation={selectedObservation}
      />
    );
  };

  render() {
    const {
      toggleSession,
      showObservationListCommonFields,
      isNullRecord,
      metaRecordSessionId,
      showPeriodicNotificationTimer,
    } = this.state;
    const {
      form,
      navigation,
      isFormRemovedFromServer,
      selectedObservation: { form: { observationListMode = null } = {} },
      measurement: copiedMeasurement,
    } = this.props;
    const { isSynced = null, meta: { sessionId = null } = {} } =
      copiedMeasurement;
    const {
      state: { params = {} },
    } = navigation;

    // Disable buttons if form is not available
    const disableButton = !form.length || isFormRemovedFromServer;
    const copyingElement = !_.isEmpty(copiedMeasurement);
    const copyingSessionElement = sessionId !== null;
    const showBlocker =
      observationListMode === 'force' &&
      !toggleSession &&
      !isNullRecord &&
      !showObservationListCommonFields &&
      !copyingSessionElement;
    const showSessionButton =
      observationListMode !== 'false' && !copyingElement;
    const isSyncedSessionOnlyElementCopied =
      copyingSessionElement && isSynced
        ? observationListMode === 'force'
        : false;

    return (
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        enabled
        keyboardVerticalOffset={Platform.select({ ios: 100, android: 550 })}
      >
        <View style={FormContainer.container}>
          <NotificationHandler
            ref={(ref) => {
              this.notificationHandler = ref;
            }}
            fixed={1}
          />

          {toggleSession && this.renderSessionTimer()}

          {showPeriodicNotificationTimer &&
            this.renderPeriodNotificationTimer()}

          {showBlocker ? this.renderSessionOnlyBlocker() : this.renderContent()}

          <View style={footerStyle.container}>
            <TouchableIcon
              name="home"
              onPress={this.onHomePress}
              style={[footerStyle.leftButton, footerStyle.roundedShape]}
            />
            {observationListMode &&
              !showBlocker &&
              !isSyncedSessionOnlyElementCopied && (
                <View style={footerStyle.sessionButtonWrapper}>
                  {showSessionButton && (
                    <TouchableIcon
                      name={
                        toggleSession ? 'playlist-add-check' : 'playlist-add'
                      }
                      onPress={this.toggleSession}
                      style={
                        showObservationListCommonFields
                          ? footerStyle.hidden
                          : [footerStyle.middleButton, footerStyle.roundedShape]
                      }
                      disabled={disableButton || isNullRecord}
                    />
                  )}
                  <TouchableIcon
                    name="folder-open-o"
                    iconType="FontAwesome"
                    onPress={() =>
                      NavigationHandler.replace('ObservationCards')
                    }
                    style={[footerStyle.middleButton, footerStyle.roundedShape]}
                  />
                  <TouchableIcon
                    name="done"
                    onPress={() => {
                      this.hideNotification(
                        NotificationModel.build(
                          NOTIFICATION_TYPES.CREATE_FORM_SUCCESS,
                        ),
                      );
                      if (!showObservationListCommonFields) {
                        this.showNotification(
                          NotificationModel.build(
                            NOTIFICATION_TYPES.CREATE_FORM_PROGRESS,
                          ),
                        );
                      }
                      setTimeout(() => {
                        params.handleSavePress(
                          isNullRecord,
                          metaRecordSessionId,
                        );
                      }, 0);
                    }}
                    style={[footerStyle.rightButton, footerStyle.roundedShape]}
                    disabled={disableButton}
                  />
                </View>
              )}
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

CreateMeasurementScreen.propTypes = {
  loadFormData: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
  form: PropTypes.array.isRequired,
  createMeasurement: PropTypes.func.isRequired,
  selectedServer: PropTypes.object.isRequired,
  selectedDatabase: PropTypes.object.isRequired,
  selectedObservation: PropTypes.object.isRequired,
  fetching: PropTypes.bool,
  measurement: PropTypes.object,
  startSession: PropTypes.func.isRequired,
  endSession: PropTypes.func.isRequired,
  isFormRemovedFromServer: PropTypes.bool,
  hasEnoughMemory: PropTypes.bool.isRequired,
  memoryUsageCritical: PropTypes.bool.isRequired,
  updateMeasurementsMetaNum: PropTypes.func.isRequired,
  isAutoSyncEnabled: PropTypes.bool.isRequired,
  tokenRefresh: PropTypes.func.isRequired,
  savedStickinesses: PropTypes.object,
  saveLastPickedColumnValues: PropTypes.func.isRequired,
  lastPickedValues: PropTypes.object.isRequired,
  gps: GpsPropTypes,
};

CreateMeasurementScreen.defaultProps = {
  fetching: false,
  measurement: {},
  isFormRemovedFromServer: false,
  savedStickinesses: {},
  gps: GpsDefaultPropTypes,
};

const mapStateToProps = (
  {
    servers: { data: serverList },
    settings: { isAutoSyncEnabled, pinSaveLogic, isSuccessNotificationSound },
    memory: { hasEnoughMemory, memoryUsageCritical },
  },
  props,
) => {
  const {
    measurementData: { server, database, observation, measurement } = {},
  } = props.navigation.state.params;

  const selectedServer = server || getSelectedServer(serverList);
  const selectedDatabase = database || getSelectedDatabase(selectedServer);
  // TODO: Refactor StateFindor to return server, database and observation object at the same time.
  const selectedObservation =
    (observation &&
      new StateFinder(serverList)
        .getServer(server.id)
        .getDatabase(database.id)
        .getObservation(observation.id).value) ||
    getSelectedObservation(selectedDatabase);
  const {
    dataTypes: form,
    fetching,
    error,
    stickinesses: savedStickinesses,
    lastPickedValues,
  } = getFormState(selectedObservation);
  const isFormRemovedFromServer =
    error === ERROR.FORM_UNAVAILABLE || selectedObservation.hidden;
  return {
    selectedServer,
    selectedDatabase,
    selectedObservation,
    form,
    fetching,
    measurement,
    isFormRemovedFromServer,
    hasEnoughMemory,
    memoryUsageCritical,
    isAutoSyncEnabled,
    savedStickinesses:
      pinSaveLogic === PIN_SAVE_LOGIC.DONT_SAVE ? {} : savedStickinesses,
    lastPickedValues,
    isSuccessNotificationSound,
  };
};

const mapDispatchToProps = (dispatch) => {
  const {
    loadFormData,
    createMeasurement,
    startSession,
    endSession,
    updateMeasurementsMetaNum,
    tokenRefresh,
    saveLastPickedColumnValues,
  } = actions;

  return bindActionCreators(
    {
      loadFormData,
      createMeasurement,
      startSession,
      endSession,
      updateMeasurementsMetaNum,
      tokenRefresh,
      saveLastPickedColumnValues,
    },
    dispatch,
  );
};

const getGpsSubscriptionOptions = ({ selectedObservation }) => ({
  observer: `${GPS_OBSERVER.FORM}-${selectedObservation.id}`,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withGpsSubscription(CreateMeasurementScreen, getGpsSubscriptionOptions));
