import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';
import styles from './styles';

const Answer = ({
  value,
  onValueChange,
  text,
}) => (
  <TouchableOpacity onPress={() => onValueChange(!value)}>
    <View style={styles.answerContainer}>
      <Text style={styles.text}>{text}</Text>
      <Icon
        size={30}
        color="#7EC667"
        name={value ? 'check-box' : 'check-box-outline-blank'}
      />
    </View>
  </TouchableOpacity>
);

Answer.propTypes = {
  text: PropTypes.string.isRequired,
  value: PropTypes.bool,
  onValueChange: PropTypes.func.isRequired,
};

Answer.defaultProps = {
  value: false,
};

export default Answer;
