import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions';
import I18n from '../../i18n';
import ObservationList from '../../components/ObservationList';
import { getObservationState, getSelectedServer, getSelectedDatabase } from '../../utils';

const mapStateToProps = ({ servers: { data: serverList } }) => {
  const selectedServer = getSelectedServer(serverList);
  const selectedDatabase = getSelectedDatabase(selectedServer);
  const { observationList, fetching, error, gameFetching } = getObservationState(selectedDatabase);

  // Game
  const trainings = observationList.filter(observation => 
    observation.game && observation.game.isEnabled);

  const enabledTrainings = trainings.filter(training => !training.game.isValidatedByBackend);
  enabledTrainings.sort((a, b) => a.game.qorder - b.game.qorder);

  let [currentTraining] = enabledTrainings;

  if (!enabledTrainings.length) {
    const maxQorder = Math.max(...trainings.map(({ game }) => game.qorder));
    currentTraining = trainings.find(({ game }) => Number(game.qorder) === maxQorder);
  }

  const observations = observationList.map((observation) => {
    if (observation.game) {
      return {
        ...observation,
        description: I18n.t('game_observation'),
        disabled: observation.id !== (currentTraining && currentTraining.id),
      };
    }
    return selectedDatabase.game === 'on' && enabledTrainings.length
      ? { ...observation, disabled: true }
      : { ...observation };
  });

  return {
    selectedServer,
    selectedDatabase,
    observationList: observations,
    fetching: fetching || gameFetching,
    error,
  };
};

const mapDispatchToProps = (dispatch) => {
  const { loadObservations, selectObservation } = actions;

  return bindActionCreators(
    {
      loadObservations,
      selectObservation,
    },
    dispatch,
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ObservationList);
