import { StyleSheet } from 'react-native';
import { Colors } from '../../colors';

export default StyleSheet.create({
  baseContainer: {
    flex: 1,
    padding: 100,
  },
  disabled: {
    backgroundColor: Colors.opacMantisGreen,
  },
  enabled: {
    backgroundColor: Colors.mantisGreen,
  },
});
