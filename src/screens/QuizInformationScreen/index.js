import React, { Component } from 'react';
import { ScrollView, View } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import HTMLView from 'react-native-htmlview';
import { footerStyle, TouchableIcon } from '../../components';
import styles from './styles';
import { renderNode, getSelectedServer, getSelectedDatabase, getSelectedObservation } from '../../utils';
import NavigationHandler from '../../NavigationHandler';

class QuizInformationScreen extends Component {
  handleDonePress = () => {
    const { title = '' } = this.props.navigation.state.params;
    NavigationHandler.navigate('QuizQuestions', { title });
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView contentContainerStyle={styles.container} removeClippedSubviews>
          <HTMLView value={this.props.content} stylesheet={styles} renderNode={renderNode} />
        </ScrollView>
        <View style={footerStyle.container}>
          <TouchableIcon name="home" onPress={() => NavigationHandler.navigate('Home')} style={[footerStyle.leftButton, footerStyle.roundedShape]} />
          <View style={footerStyle.rightButtonContainer} >
            <TouchableIcon name="done" onPress={this.handleDonePress} style={[footerStyle.rightButton, footerStyle.roundedShape]} />
          </View>
        </View>
      </View>
    );
  }
}

QuizInformationScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
  content: PropTypes.string.isRequired,
};

const mapStateToProps = ({ servers: { data: serverList } }) => {
  const selectedServer = getSelectedServer(serverList);
  const selectedDatabase = getSelectedDatabase(selectedServer);
  const selectedObservation = getSelectedObservation(selectedDatabase);

  const content = selectedObservation && selectedObservation.game && selectedObservation.game.html;

  return { content };
};

export default connect(
  mapStateToProps,
  null,
)(QuizInformationScreen);
