import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  a: {
    color: '#7EC667',
  },
  container: {
    marginHorizontal: 10,
    paddingVertical: 10,
  },
});
