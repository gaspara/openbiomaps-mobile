import { StyleSheet } from 'react-native';
import { Colors } from '../../colors';

export default StyleSheet.create({
  bottomContainer: {
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  floatingContainer: { 
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    width: '100%',
    paddingStart: 15,
  },
  mapContainer: { 
    flex: 1, 
    justifyContent: 'flex-end', 
  },
  container: {
    flex: 1,
    width: '100%',
    justifyContent: 'flex-end',
  },
  topContainer: {
    flexDirection: 'column',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  showOnlyOneContainer: {
    backgroundColor: Colors.mantisGreen,
    borderColor: Colors.mantisGreen,
    borderRadius: 15,
    borderWidth: 2,
    margin: 10,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },
  roundedContainer: {
    borderWidth: 1,
    borderRadius: 10,
  },
  showOnlyOneText: {
    color: 'black',
    flex: 1,
    textAlign: 'center',
    fontSize: 17,
  },
  errorContainer: {
    backgroundColor: 'rgba(0, 0, 0, 0.87)',
    padding: 10,
  },
  myLocationIcon: {
    transform: [{ rotate: '-45deg' }],
    color: '#333',
  },
  myLocationButton: {
    backgroundColor: 'white',
    margin: 15,
    padding: 10,
    transform: [{ rotate: '45deg' }],
    borderRadius: 10,
    elevation: 2,
  },
  showTrackLogButton: {
    backgroundColor: 'white',
    margin: 15,
    padding: 10,
    borderRadius: 10,
    elevation: 2,
  },
  createMeasurementIcon: {
    transform: [{ rotate: '-45deg' }],
    color: '#7EC667',
  },
  createMeasurementButton: {
    backgroundColor: 'white',
    margin: 15,
    padding: 10,
    transform: [{ rotate: '45deg' }],
    borderRadius: 10,
    elevation: 2,
  },
  errorText: {
    color: 'white',
  },
  firstUseContainer: {
    backgroundColor: 'rgba(0,0,0,.5)',
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  firstUseTextContainer: {
    backgroundColor: 'rgba(0,0,0,.5)',
    position: 'absolute',
    height: 64,
    bottom: 25,
    right: 80,
    left: 0,
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  firstUseText: {
    color: 'white',
    fontSize: 16,
    flex: 1,
  },

  /*floatingContainer: { 
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    width: '100%',
    paddingStart: 15,
  },

  bottomContainer: {
    flexDirection: 'column',
    alignItems: 'flex-end',
  },


  content: {
    flex: 1,
    flexDirection: 'column'
  },
  mapContainer: { 
    flex: 1, 
    justifyContent: 'flex-end', 
  },

  myLocationButton: {
    backgroundColor: 'white',
    margin: 15,
    padding: 10,
    transform: [{ rotate: '45deg' }],
    borderRadius: 10,
    elevation: 2,
  },

  myLocationIcon: {
    transform: [{ rotate: '-45deg' }],
    color: '#333',
  },

  showTrackLogButton: {
    backgroundColor: 'white',
    margin: 15,
    padding: 10,
    borderRadius: 10,
    elevation: 2,
  },





  container: {
    flex: 1,
    width: '100%',
    justifyContent: 'flex-end',
  },
  topContainer: {
    flexDirection: 'column',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  showOnlyOneContainer: {
    backgroundColor: Colors.mantisGreen,
    borderColor: Colors.mantisGreen,
    borderRadius: 15,
    borderWidth: 2,
    margin: 10,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },
  roundedContainer: {
    borderWidth: 1,
    borderRadius: 10,
  },
  showOnlyOneText: {
    color: 'black',
    flex: 1,
    textAlign: 'center',
    fontSize: 17,
  },
  errorContainer: {
    backgroundColor: 'rgba(0, 0, 0, 0.87)',
    padding: 10,
  },
  myLocationIcon: {
    transform: [{ rotate: '-45deg' }],
    color: '#333',
  },
  myLocationButton: {
    backgroundColor: 'white',
    margin: 15,
    padding: 10,
    transform: [{ rotate: '45deg' }],
    borderRadius: 10,
    elevation: 2,
  },

  createMeasurementIcon: {
    transform: [{ rotate: '-45deg' }],
    color: '#7EC667',
  },
  createMeasurementButton: {
    backgroundColor: 'white',
    margin: 15,
    padding: 10,
    transform: [{ rotate: '45deg' }],
    borderRadius: 10,
    elevation: 2,
  },
  errorText: {
    color: 'white',
  },
  firstUseContainer: {
    backgroundColor: 'rgba(0,0,0,.5)',
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  firstUseTextContainer: {
    backgroundColor: 'rgba(0,0,0,.5)',
    position: 'absolute',
    height: 64,
    bottom: 25,
    right: 80,
    left: 0,
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  firstUseText: {
    color: 'white',
    fontSize: 16,
    flex: 1,
  },
  */
});
