import React, { Component } from 'react';
import { View, BackHandler, InteractionManager } from 'react-native';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as actions from '../../actions';
import I18n from '../../i18n';
import { MeasurementForm, footerStyle, TouchableIcon, Spinner } from '../../components';
import NavigationHandler from '../../NavigationHandler';
import { FormContainer } from '../../components/MeasurementForm/styles';
import { showAlertDialog } from '../../components/AlertDialog';
import { getFormState } from '../../utils';
import NotificationHandler from '../../components/NotificationHandler';
import { NotificationModel, NOTIFICATION_TYPES } from '../../models/NotificationModel';
import { ERROR, GEOMETRY_TYPES, GPS_OBSERVER } from '../../Constants';
import { removeUnusedAttachedFilesOfMeasurement } from '../../utils/reduxUtils';
import withGpsSubscription, { GpsDefaultPropTypes, GpsPropTypes } from '../../hocs/withGpsSubscription';

class EditMeasurementScreen extends Component {
  // eslint-disable-next-line
  _didFocusSubscription;

  _willBlurSubscription;

  constructor(props) {
    super(props);

    this._didFocusSubscription = props.navigation.addListener('didFocus', () => {
      this.backPressListener = BackHandler.addEventListener('hardwareBackPress', this.onAndroidBackPress);
    });

    this.state = {
      isModified: false,
      didFinishInitialAnimation: false,
    };

    this.notificationHandler = null;
    this.backPressListener = null;
  }

  componentDidMount = () => {
    const { navigation, form, gps } = this.props;

    navigation.setParams({ onHeaderBackPress: this.onHeaderBackPress });
    this._willBlurSubscription = navigation.addListener('willBlur', () => this.backPressListener?.remove());

    // Delay initial fetching/rendering in order to improve navigaiton performance.
    InteractionManager.runAfterInteractions(() => {
      setTimeout(() => {
        this.setState({ didFinishInitialAnimation: true });
      }, 500);
    });

    const hasGeometryType = form.some((item) => GEOMETRY_TYPES.includes(item.type));
    if (hasGeometryType) {
      gps.subscribe();
    }
  };

  componentDidUpdate = () => {
    const { isFormRemovedFromServer } = this.props;

    if (this.notificationHandler) {
      if (isFormRemovedFromServer) {
        this.showNotification(NotificationModel.build(NOTIFICATION_TYPES.FORM_UNAVAILABLE));
      } else {
        const { measurement: { meta: { sessionId } } } = this.props;
        if (sessionId) {
          this.showNotification(NotificationModel.build(NOTIFICATION_TYPES.EDITING_SESSION_ELEMENT));
        }
      }
    }
  };

  componentWillUnmount = () => {
    const { gps } = this.props;
    
    if (this._didFocusSubscription) this._didFocusSubscription.remove();
    if (this._willBlurSubscription) this._willBlurSubscription.remove();
    this.backPressListener?.remove();

    gps.unsubscribe();
  };

  getFormData = () => {
    const { form, observation, measurement } = this.props;
    const isElementInOpenedSession = observation.session?.id && measurement.meta?.sessionId
      && observation.session?.id === measurement.meta?.sessionId;
    return form.filter((item) => (item.apiParams.hidden === 'off' && !(isElementInOpenedSession && item.apiParams.once === 'on')));
  };

  onAndroidBackPress = () => {
    const { isModified } = this.state;

    if (isModified) {
      this.showExitModal();
      return true;
    }

    NavigationHandler.replace('ObservationCards');
    return true;
  };

  onHeaderBackPress = () => {
    const { isModified } = this.state;

    if (isModified) { 
      this.showExitModal();
      return;
    }

    NavigationHandler.replace('ObservationCards');
  };

  onHomePress = () => {
    const { isModified } = this.state;

    if (isModified) {
      this.showExitModal('Home');
      return;
    }

    NavigationHandler.navigate('Home');
  };

  showExitModal = (routeName) => {
    showAlertDialog({
      title: I18n.t('have_unsaved_changes'),
      message: I18n.t('do_you_want_to_exit'),
      showCancelButton: true,
      positiveLabel: I18n.t('bool_true'),
      negativeLabel: I18n.t('bool_false'),
      onPositiveBtnPress: routeName
        ? () => NavigationHandler.navigate(routeName)
        : () => NavigationHandler.goBack(),
    });
  };

  showNotification = (notification) => {
    this.notificationHandler.showNotification(notification);
  };

  hideNotification = (notification) => {
    this.notificationHandler.hideNotification(notification);
  };

  formDataChangeListener = (isModified) => {
    this.setState({ isModified });
  };

  onSavePress = (measurement) => {
    const {
      server,
      database,
      observation,
      updateMeasurement,
      measurement: measurementBeingEdited,
    } = this.props;

    updateMeasurement(server.id, database.id, observation.id, measurement);
    removeUnusedAttachedFilesOfMeasurement({ observation, measurement: measurementBeingEdited });
    NavigationHandler.replace('ObservationCards', { showEditedMessageParam: true });
  };

  render() {
    const { server, database, observation, measurement, navigation } = this.props;
    const { state: { params = {} } } = navigation;
    const { didFinishInitialAnimation } = this.state;

    if (!didFinishInitialAnimation) return <Spinner />;

    return (
      <View style={FormContainer.container}>
        <NotificationHandler ref={(ref) => { this.notificationHandler = ref; }} />

        <MeasurementForm
          id={measurement.id}
          onDonePress={this.onSavePress}
          navigation={navigation}
          values={measurement.data}
          errors={measurement.errors}
          server={server}
          database={database}
          observation={observation}
          form={this.getFormData()}
          formDataChangeListener={this.formDataChangeListener}
          isEditing
        />

        <View style={footerStyle.container}>
          <TouchableIcon name="home" onPress={this.onHomePress} style={[footerStyle.leftButton, footerStyle.roundedShape]} />
          <TouchableIcon
            name="done"
            onPress={() => params.handleSavePress()}
            style={[footerStyle.rightButton, footerStyle.roundedShape]}
          />
        </View>
      </View>
    );
  }
}

EditMeasurementScreen.propTypes = {
  server: PropTypes.object.isRequired,
  database: PropTypes.object.isRequired,
  observation: PropTypes.object.isRequired,
  measurement: PropTypes.object.isRequired,
  form: PropTypes.array.isRequired,
  navigation: PropTypes.object.isRequired,
  updateMeasurement: PropTypes.func.isRequired,
  isFormRemovedFromServer: PropTypes.bool,
  gps: GpsPropTypes,
};

EditMeasurementScreen.defaultProps = {
  isFormRemovedFromServer: false,
  gps: GpsDefaultPropTypes,
};

const mapStateToProps = (state, props) => {
  const {
    measurementData: {
      server,
      database,
      observation,
      measurement,
    },
  } = props.navigation.state.params;

  const { error, dataTypes: form } = getFormState(observation);
  const isFormRemovedFromServer = error === ERROR.FORM_UNAVAILABLE || observation.hidden;

  return {
    server,
    database,
    observation,
    measurement,
    form,
    isFormRemovedFromServer,
  };
};

const mapDispatchToProps = (dispatch) => {
  const { updateMeasurement } = actions;

  return bindActionCreators(
    {
      updateMeasurement,
    },
    dispatch,
  );
};

const getGpsSubscriptionOptions = ({ observation }) => ({ observer: `${GPS_OBSERVER.FORM}-${observation.id}` });

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withGpsSubscription(
  EditMeasurementScreen, 
  getGpsSubscriptionOptions,
));
