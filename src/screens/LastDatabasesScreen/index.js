import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions';
import DatabaseList from '../../components/DatabaseList';
import { getDatabaseState, getObservationState, getSelectedServer, getSelectedDatabase } from '../../utils';

const mapStateToProps = ({ servers: { data: serverList } }) => {
  const selectedServer = getSelectedServer(serverList);
  const selectedDatabase = getSelectedDatabase(selectedServer);
  const { databaseList, fetching, error } = getDatabaseState(selectedServer);
  const { observationList } = getObservationState(selectedDatabase);

  return {
    selectedServer,
    databaseList: databaseList
      .filter(database => 
        database.lastSelected && database.observations && database.observations.data.length && !database.hidden)
      .sort((a, b) => new Date(b.lastSelected) - new Date(a.lastSelected)),
    fetching,
    error,
    observationList,
  };
};

const mapDispatchToProps = (dispatch) => {
  const { loadDatabases, selectDatabase } = actions;

  return bindActionCreators(
    {
      loadDatabases,
      selectDatabase,
    },
    dispatch,
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DatabaseList);
