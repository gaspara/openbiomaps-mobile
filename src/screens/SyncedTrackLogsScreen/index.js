import React, { Component } from 'react';
import { View, InteractionManager } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions';
import NavigationHandler from '../../NavigationHandler';
import I18n from '../../i18n';
import {
  footerStyle, 
  TouchableIcon,
  showAlertDialog,
  Spinner,
  TrackLogList,
} from '../../components';
import { Colors } from '../../colors';

class SyncedTrackLogsScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      didFinishInitialAnimation: false,
    };
  }

  componentDidMount = () => {
    // Delay initial fetching/rendering in order to improve navigaiton performance.
    InteractionManager.runAfterInteractions(() => {
      setTimeout(() => {
        this.setState({ didFinishInitialAnimation: true });
      }, 500);
    });
  }

  onDeletePress = (trackLogId) => {
    showAlertDialog({
      title: I18n.t('delete_tracklog_dialog_title'), 
      message: I18n.t('delete_dialog_message'),
      showCancelButton: true, 
      positiveLabel: I18n.t('bool_true'),
      negativeLabel: I18n.t('bool_false'),
      onPositiveBtnPress: () => this.props.deleteSyncedTrackLog(trackLogId),
    });
  }

  onViewMapPress = (trackLogId) => {
    NavigationHandler.navigate('ObservationMap', {
      trackLogId,
    });
  }

  onDownloadPress = (trackLogId) => {
    const { exportTrackLog } = this.props;
    exportTrackLog(trackLogId);
  }

  onDeleteAllPress = () => {
    showAlertDialog({
      title: I18n.t('delete_all_synced_dialog_title'), 
      message: I18n.t('delete_dialog_message'),
      showCancelButton: true, 
      positiveLabel: I18n.t('bool_true'),
      negativeLabel: I18n.t('bool_false'),
      onPositiveBtnPress: this.props.deleteAllTrackLog,
    });
  }

  render() {
    const { didFinishInitialAnimation } = this.state;
    const { trackLogList } = this.props;

    if (!didFinishInitialAnimation) return <Spinner />;

    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          <TrackLogList 
            trackLogList={trackLogList} 
            onDeletePress={this.onDeletePress}
            onViewMapPress={this.onViewMapPress}
            onDownloadPress={this.onDownloadPress}
          />
        </View>
        <View style={footerStyle.narrowContainer}>
          <TouchableIcon
            name="delete"
            label={I18n.t('delete_all')}
            labelStyle={footerStyle.deleteButtonLabel}
            onPress={this.onDeleteAllPress}
            size={22}
            color={Colors.darkRed}
            style={[footerStyle.singleButton, footerStyle.roundedShape]}
          />
        </View>
      </View>
    );
  }
}

SyncedTrackLogsScreen.propTypes = {
  trackLogList: PropTypes.arrayOf(PropTypes.object).isRequired,
  deleteAllTrackLog: PropTypes.func.isRequired,
  deleteSyncedTrackLog: PropTypes.func.isRequired,
  exportTrackLog: PropTypes.func.isRequired,
};

const mapStateToProps = ({ trackLog: { trackLogList } }) => {
  // Filter out null records that contain only session related info.
  const syncedTrackLogList = trackLogList.filter(({ selectedDatabases }) => selectedDatabases.some(({ isSynced }) => isSynced));

  return {
    trackLogList: syncedTrackLogList,
  };
};

const mapDispatchToProps = (dispatch) => {
  const { deleteSyncedTrackLog, deleteAllTrackLog, exportTrackLog } = actions;

  return bindActionCreators(
    {
      deleteAllTrackLog,
      deleteSyncedTrackLog,
      exportTrackLog,
    },
    dispatch,
  );
};
  
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SyncedTrackLogsScreen);
