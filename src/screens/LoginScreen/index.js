import React, { Component } from 'react';
import {
  View,
  KeyboardAvoidingView,
  TouchableOpacity,
  TouchableHighlight,
  Text
} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import PropTypes from 'prop-types';
import * as Animatable from 'react-native-animatable';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions';
import I18n from '../../i18n';
import styles from './styles';
import { TextInput, Spinner } from '../../components';
import { Colors } from '../../colors';
import NavigationHandler from '../../NavigationHandler';
import { getUserState, getSelectedServer } from '../../utils';
import { ERROR } from '../../Constants';
import { showAlertDialog } from '../../components/AlertDialog';

// eslint-disable-next-line no-undef
const shouldPrefillValues = __DEV__;
const TEST_USER = {
  userName: 'bence.kovacs@mindtechapps.com',
  password: 'test1234',
};

class LoginScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      auth: {
        userName: shouldPrefillValues ? TEST_USER.userName : '',
        password: shouldPrefillValues ? TEST_USER.password : '',
      },
      isPasswordHidden: true,
      emptyFields: false,
    };
  }

  // if the state of the user fetching sticks to true, we change it here
  componentDidMount = () => {
    const { selectedServer, fetching, initAuthFetch } = this.props;

    if (fetching) {
      initAuthFetch(selectedServer.id);
    }
  };

  componentDidUpdate = (prevProps, prevState) => {
    const {
      userName,
      accessToken,
      error,
      serverName,
      selectedServer: {
        id: serverId,
        databases: { data: databaseList = [] } = {},
      },
      measurementsMeta,
    } = this.props;
    const { emptyFields } = this.state;

    // Handle login success
    if (!prevProps.accessToken && accessToken) {
      // Reset database selection after successfull login 
      // to avoid confusing navigation flow based on selected entites in the hierarchy tree.
      this.props.resetDatabaseSelection();

      // update measurementsMeta
      this.props.updatePersistedMetaStates(measurementsMeta);

      if (prevProps.userName !== null && prevProps.userName !== '' && prevProps.userName.toLowerCase() !== userName.toLowerCase()) {
        this.props.purgePersistedStates(serverId);

        NavigationHandler.navigate(
          'DatabaseList',
          { title: serverName },
          null,
          NavigationHandler.navigate('DatabaseListScreen'),
        );
      } else if (databaseList.some(database => database.lastSelected && database.observations.data.length)) {
        NavigationHandler.navigate(
          'DatabaseList',
          { title: serverName },
          null,
          NavigationHandler.navigate('LastDatabasesScreen'),
        );
      } else {
        NavigationHandler.navigate(
          'DatabaseList',
          { title: serverName },
          null,
          NavigationHandler.navigate('DatabaseListScreen'),
        );
      }
    }

    // Animate error container when error message is received or the input fields are empty
    if ((!prevProps.error && error) || (!prevState.emptyFields && emptyFields)) {
      this.errorContainer.shake(800);
    }
  };

  componentWillUnmount = () => {
    const { selectedServer, clearAuthError, error } = this.props;

    if (error !== ERROR.INVALID_REFRESH_TOKEN) {
      clearAuthError(selectedServer.id);
    }
  };

  onLoginPress = () => {
    const {
      auth: { userName, password },
    } = this.state;
    const { login, selectedServer, userName: prevUserName } = this.props;

    // Set emptyfileds back to false in case of every login btn press
    // to handle error container animation in one place (see: componentDidUpdate)
    this.setState({ emptyFields: false }, () => {
      if (userName && password) {
        if (prevUserName && prevUserName.toLowerCase() !== userName.toLowerCase()) {
          showAlertDialog({
            title: I18n.t('default_alert_title'),
            message: I18n.t('new_login_alert', { name: prevUserName }),
            onPositiveBtnPress: () => { login(userName, password, selectedServer.id); },
            showCancelButton: true,
          });
        } else {
          login(userName, password, selectedServer.id);
        }
      } else {
        this.setState({ emptyFields: true });
      }
    });
  };

  updateAuthState = key => (value) => {
    this.setState(prevState => ({
      ...prevState,
      auth: {
        ...prevState.auth,
        [key]: value.trim(),
      },
    }));
  };

  togglePasswordVisibility = () => {
    this.setState({ isPasswordHidden: !this.state.isPasswordHidden });
  };

  getErrorMessage = () => {
    if (this.state.emptyFields) {
      return I18n.t('empty_fields');
    }

    if (this.props.error === ERROR.INVALID_REFRESH_TOKEN) {
      return I18n.t('invalid_refresh_token');
    }

    return this.props.error;
  }

  render() {
    const {
      auth: { userName, password },
    } = this.state;
    const { fetching } = this.props;

    if (userName && password && fetching) return <Spinner />;

    return (
      <KeyboardAvoidingView style={styles.keyboardAvoidingContainer}>
        <View style={styles.container}>

          <View style={styles.inputFieldContainer}>
            <TextInput
              label={I18n.t('username')}
              value={userName}
              onChangeText={this.updateAuthState('userName')}
              keyboardType="email-address"
              autoCapitalize='none' />
          </View>

          <View style={styles.inputFieldContainer}>
            <TextInput
              label={I18n.t('password')}
              value={password}
              secureTextEntry={this.state.isPasswordHidden}
              onChangeText={this.updateAuthState('password')}
              autoCapitalize='none' />

            <TouchableOpacity style={styles.passwordButtonIcon}>
              <Icon
                name={this.state.isPasswordHidden ? "eye-with-line" : "eye"}
                size={25}
                onPress={this.togglePasswordVisibility}
              />
            </TouchableOpacity>
          </View>

          {
            this.getErrorMessage() && <Animatable.View
              style={styles.errorContainer}
              useNativeDriver
              ref={(ref) => {
                this.errorContainer = ref;
              }}>
              <Text style={styles.errorText} adjustsFontSizeToFit={true}>
                {this.getErrorMessage()}
              </Text>
            </Animatable.View>
          }

          <View style={styles.loginButtonContainer}>
            <TouchableHighlight
              underlayColor={Colors.asparagusGreen}
              style={styles.loginButton}
              onPress={this.onLoginPress}>
              <Text style={styles.buttonText} adjustsFontSizeToFit={true}>{I18n.t('login')}</Text>
            </TouchableHighlight>
          </View>

        </View>
      </KeyboardAvoidingView>
    );
  }
}

LoginScreen.propTypes = {
  login: PropTypes.func.isRequired,
  initAuthFetch: PropTypes.func.isRequired,
  clearAuthError: PropTypes.func.isRequired,
  accessToken: PropTypes.string,
  fetching: PropTypes.bool,
  error: PropTypes.string,
  serverName: PropTypes.string.isRequired,
  selectedServer: PropTypes.object.isRequired,
  resetDatabaseSelection: PropTypes.func.isRequired,
  userName: PropTypes.string,
  purgePersistedStates: PropTypes.func.isRequired,
  updatePersistedMetaStates: PropTypes.func.isRequired,
  measurementsMeta: PropTypes.object.isRequired,
};

LoginScreen.defaultProps = {
  userName: null,
  accessToken: null,
  fetching: false,
  error: null,
};

const mapStateToProps = ({ servers: { data: serverList }, measurementsMeta }, { navigation }) => {
  const selectedServer = getSelectedServer(serverList);
  const { userName, accessToken, fetching, error } = getUserState(selectedServer);

  const {
    state: {
      params: { title: serverName },
    },
  } = navigation;

  return {
    selectedServer,
    userName,
    accessToken,
    fetching,
    error,
    serverName,
    measurementsMeta,
  };
};

const mapDispatchToProps = (dispatch) => {
  const { login, clearAuthError, resetDatabaseSelection, purgePersistedStates, updatePersistedMetaStates, initAuthFetch } = actions;

  return bindActionCreators(
    {
      login,
      clearAuthError,
      resetDatabaseSelection,
      purgePersistedStates,
      updatePersistedMetaStates,
      initAuthFetch,
    },
    dispatch,
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginScreen);
