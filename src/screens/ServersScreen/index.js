import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions';
import ServerList from '../../components/ServerList';

const mapStateToProps = ({ servers: { data: serverList, fetching, error } }) => ({
  serverList: serverList.filter(server => !server.hidden),
  fetching,
  error,
});

const mapDispatchToProps = (dispatch) => {
  const { loadServers, selectServer } = actions;

  return bindActionCreators(
    {
      loadServers,
      selectServer,
    },
    dispatch,
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ServerList);
