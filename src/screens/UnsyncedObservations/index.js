import React, { Component } from 'react';
import {
  View,
  InteractionManager,
  ActivityIndicator,
  Text,
  TouchableOpacity,
} from 'react-native';
import {
  activateKeepAwake,
  deactivateKeepAwake,
} from '@sayem314/react-native-keep-awake';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { throttle, isEmpty } from 'lodash';
import * as actions from '../../actions';
import NavigationHandler from '../../NavigationHandler';
import { getFilteredMeasurements, getLastObservation } from '../../utils';
import I18n from '../../i18n';
import {
  NotificationModel,
  NOTIFICATION_TYPES,
} from '../../models/NotificationModel';
import { ERROR } from '../../Constants';
import {
  footerStyle,
  TouchableIcon,
  Spinner,
  showAlertDialog,
  ObservationOneLineList,
  NotificationHandler,
} from '../../components';
import client from '../../config/apiClient';

class UnsyncedObservationsScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showEditedNotification: false,
      didFinishInitialAnimation: false,
    };
    this.notificationHandler = null;
    this.throttledToggleSync = throttle(this.toggleSync, 2000);
  }

  componentDidMount = () => {
    activateKeepAwake();
    const { navigation } = this.props;
    navigation.dangerouslyGetParent()?.setParams({
      toggleSync: this.throttledToggleSync,
    });

    // Delay initial fetching/rendering in order to improve navigaiton performance.
    InteractionManager.runAfterInteractions(() => {
      setTimeout(() => {
        this.setState({ didFinishInitialAnimation: true });
      }, 500);
    });
    if (navigation.state.params?.showEditedMessageParam) {
      this.showEditedMessage();
    }
  };

  componentDidUpdate = () => {
    if (this.notificationHandler) {
      this.handleEditedNotification();
      this.handleAuthError();
      this.handleSyncProgressNotification();
      this.handleTrackLogSyncProgressNotification();
      this.handleNetWorkError();
      this.handleUserFetchingNotification();
    }
  };

  componentWillUnmount = () => {
    deactivateKeepAwake();
  };

  handleAuthError = () => {
    if (this.props.showAuthError) {
      const notification = NotificationModel.build(
        NOTIFICATION_TYPES.AUTH_ERROR,
        this.renderLoginNavButton(),
      );
      this.notificationHandler.showNotification(notification);
    } else {
      this.notificationHandler.hideNotification(
        NotificationModel.build(NOTIFICATION_TYPES.AUTH_ERROR),
      );
    }
  };

  handleUserFetchingNotification = () => {
    if (this.props.fetchingUsers.length) {
      const notification = NotificationModel.build(
        NOTIFICATION_TYPES.COMMUNICATING_WITH_THE_SERVER,
        this.renderSpinner(),
      );
      this.notificationHandler.showNotification(notification);
    } else {
      this.notificationHandler.hideNotification(
        NotificationModel.build(
          NOTIFICATION_TYPES.COMMUNICATING_WITH_THE_SERVER,
        ),
      );
    }
  };

  handleNetWorkError = () => {
    const { networkError, serverError } = this.props;
    if (networkError || serverError) {
      const unknownErrors = {};
      let networkNotification = null;
      switch (networkError) {
        case ERROR.LATE_RESPONSE_TRIAL_CALL:
          networkNotification = NotificationModel.build(
            NOTIFICATION_TYPES.LATE_RESPONSE_TRIAL_CALL,
          );
          break;
        case ERROR.NETWORK_ERROR:
          networkNotification = NotificationModel.build(
            NOTIFICATION_TYPES.NETWORK_ERROR,
          );
          break;
        default: {
          if (networkError) unknownErrors[networkError] = true;
        }
      }

      let serverNotification = null;
      switch (serverError) {
        case ERROR.LATE_RESPONSE_TRIAL_CALL:
          serverNotification = NotificationModel.build(
            NOTIFICATION_TYPES.LATE_RESPONSE_TRIAL_CALL,
          );
          break;
        case ERROR.NETWORK_ERROR:
          serverNotification = NotificationModel.build(
            NOTIFICATION_TYPES.NETWORK_ERROR,
          );
          break;
        default:
          if (serverError) unknownErrors[serverError] = true;
      }

      if (networkNotification)
        this.notificationHandler.showNotification(networkNotification);
      if (
        serverNotification &&
        serverNotification.id !== networkNotification?.id
      ) {
        this.notificationHandler.showNotification(serverNotification);
      }

      if (!isEmpty(unknownErrors)) {
        this.notificationHandler.showNotification(
          NotificationModel.build(
            NOTIFICATION_TYPES.UNKNOWN_ERROR,
            <View>
              {Object.keys(unknownErrors).map((message) => (
                <Text key={message} style={{ textAlign: 'center' }}>
                  {message}
                </Text>
              ))}
            </View>,
          ),
        );
      }
    } else {
      this.notificationHandler.hideNotification(
        NotificationModel.build(NOTIFICATION_TYPES.NETWORK_ERROR),
      );
      this.notificationHandler.hideNotification(
        NotificationModel.build(NOTIFICATION_TYPES.LATE_RESPONSE_TRIAL_CALL),
      );
      this.notificationHandler.hideNotification(
        NotificationModel.build(NOTIFICATION_TYPES.UNKNOWN_ERROR),
      );
    }
  };

  handleSyncProgressNotification = () => {
    if (this.props.syncing) {
      const notification = NotificationModel.build(
        NOTIFICATION_TYPES.SYNCING_IN_PROGRESS,
        this.renderSpinner(
          this.props.syncing,
          NOTIFICATION_TYPES.SYNCING_IN_PROGRESS,
        ),
      );

      this.notificationHandler.showNotification(notification);
    } else {
      this.notificationHandler.hideNotification(
        NotificationModel.build(NOTIFICATION_TYPES.SYNCING_IN_PROGRESS),
      );
    }
  };

  handleTrackLogSyncProgressNotification = () => {
    if (this.props.syncingTrackLogsNumber) {
      const notification = NotificationModel.build(
        NOTIFICATION_TYPES.SYNCING_TRACKLOGS_IN_PROGRESS,
        this.renderSpinner(
          this.props.syncingTrackLogsNumber,
          NOTIFICATION_TYPES.SYNCING_TRACKLOGS_IN_PROGRESS,
        ),
      );

      this.notificationHandler.showNotification(notification);
    } else {
      this.notificationHandler.hideNotification(
        NotificationModel.build(
          NOTIFICATION_TYPES.SYNCING_TRACKLOGS_IN_PROGRESS,
        ),
      );
    }
  };

  handleEditedNotification = () => {
    const { isSuccessNotificationSound } = this.props;
    if (this.state.showEditedNotification) {
      const notification = NotificationModel.build(
        NOTIFICATION_TYPES.EDIT_SUCCEEDED,
        null,
        3000,
        isSuccessNotificationSound,
      );
      this.notificationHandler.showNotification(notification);
    } else {
      this.notificationHandler.hideNotification(
        NotificationModel.build(NOTIFICATION_TYPES.EDIT_SUCCEEDED),
      );
    }
  };

  onEditPress = (measurementData) => {
    const { shortcutObservations } = this.props;
    const shortcutObservationIds = [];

    shortcutObservations.map((shortcut) =>
      shortcutObservationIds.push(shortcut['observation']['id']),
    );
    NavigationHandler.replace('EditMeasurement', {
      title: measurementData.observation.name,
      description: measurementData.database.name,
      measurementData,
      pinned: shortcutObservationIds.includes(measurementData.observation.id),
      createNewForm: true,
    });
  };

  onAddNumInd = (measurementData, numInd, number) => {
    if (measurementData.measurement.data[numInd] !== undefined) {
      const newData = measurementData.measurement;
      let newNumber = parseInt(newData.data[numInd], 10);
      newNumber = newNumber + number < 0 ? 0 : newNumber + number;
      if (newNumber !== newData.data[numInd]) {
        newData.data[numInd] = newNumber.toString();
        this.props.updateMeasurement(
          measurementData.server.id,
          measurementData.database.id,
          measurementData.observation.id,
          newData,
        );
      }
    }
  };

  onDeletePress = (measurementData) => {
    showAlertDialog({
      title: I18n.t('delete_dialog_title'),
      message: I18n.t('delete_dialog_message'),
      showCancelButton: true,
      positiveLabel: I18n.t('bool_true'),
      negativeLabel: I18n.t('bool_false'),
      onPositiveBtnPress: () =>
        this.props.removeFromMeasurementList(measurementData),
    });
  };

  onSchemaPress = (measurementData) => {
    NavigationHandler.replace('CreateMeasurement', {
      title: measurementData.observation.name,
      description: measurementData.database.name,
      measurementData,
    });
  };

  toggleSync = () => {
    const { serverList, tokenRefresh, status, allMeasurements } = this.props;
    if (['none', 'unknown'].includes(status)) {
      showAlertDialog({
        title: I18n.t('no_connection'),
        message: I18n.t('sync_requires_net'),
        positiveLabel: I18n.t('ok'),
      });
    } else {
      let syncStarted = false;
      serverList.forEach((server) => {
        if (
          allMeasurements.some(
            ({ server: { id }, measurement, observation }) =>
              server.id === id &&
              !measurement.isSynced &&
              (!measurement.meta?.sessionId ||
                measurement.meta?.sessionId !== observation.session?.id),
          )
        ) {
          tokenRefresh(server.id, {
            syncMeasurementsAfter: true,
            syncTrackLogsAfter: true,
          });
          syncStarted = true;
        }
      });
      if (!syncStarted) {
        showAlertDialog({
          message: `${I18n.t('no_data_to_sync')} ${I18n.t(
            'sync_tracklogs_manually_hint',
          )}`,
        });
      }
    }
  };

  showEditedMessage = () => {
    this.setState({ showEditedNotification: true }, () => {
      setTimeout(() => {
        this.setState({ showEditedNotification: false });
      }, 3000);
    });
  };

  navigateToServerScreen = () => {
    NavigationHandler.replace('ServerList');
  };

  navigateToLastObservation = ({
    name: observationName,
    server,
    database,
    databaseName,
    observation,
    serverUrl,
  }) => {
    const {
      selectObservation,
      selectDatabase,
      selectServer,
      resetDatabaseSelection,
      resetServerSelection,
    } = this.props;

    client.prefix = serverUrl;

    resetDatabaseSelection();
    resetServerSelection();

    selectServer(server.id);
    selectDatabase(server.id, database.id);
    selectObservation(server.id, database.id, observation.id);

    NavigationHandler.replace('CreateMeasurement', {
      title: observationName,
      description: databaseName,
      measurementData: {
        server,
        database,
        observation,
      },
    });
  };

  renderLoginNavButton = () => (
    <TouchableIcon
      name="account-circle"
      label={I18n.t('login')}
      onPress={this.navigateToServerScreen}
      labelStyle={{ color: 'white' }}
      color="white"
      style={[
        footerStyle.singleButton,
        footerStyle.roundedShape,
        { padding: 0 },
      ]}
    />
  );

  renderSpinner = (num = 0, notificationType) => (
    <View style={{ flexDirection: 'row' }}>
      {num !== 0 && (
        <Text style={{ color: 'white' }}>
          {notificationType === NOTIFICATION_TYPES.SYNCING_IN_PROGRESS
            ? I18n.t('syncing_with_num_of_left', { num })
            : I18n.t('syncing_tracklogs_with_num_of_left', { num })}
        </Text>
      )}
      <ActivityIndicator
        size="small"
        color="white"
        style={{ marginStart: 3 }}
      />
    </View>
  );

  render() {
    const { didFinishInitialAnimation } = this.state;
    const { allMeasurements, lastObservation } = this.props;

    if (!didFinishInitialAnimation) return <Spinner />;

    return (
      <View style={{ flex: 1 }}>
        <NotificationHandler
          ref={(ref) => {
            this.notificationHandler = ref;
          }}
        />
        <View style={{ flex: 1 }}>
          <ObservationOneLineList
            measurements={allMeasurements}
            type="unsynced"
            onEditPress={this.onEditPress}
            onDeletePress={this.onDeletePress}
            onSchemaPress={this.onSchemaPress}
            onAddNumInd={this.onAddNumInd}
          />
        </View>
        {lastObservation && (
          <View style={footerStyle.narrowContainer}>
            <TouchableOpacity
              style={[footerStyle.roundedShape, footerStyle.narrowButton]}
              onPress={() => {
                this.navigateToLastObservation(lastObservation);
              }}
            >
              <Text numberOfLines={1} style={footerStyle.narrowFooterText}>
                {I18n.t('jump_to_latest_observation', {
                  observationName: lastObservation.name,
                })}
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }
}

UnsyncedObservationsScreen.propTypes = {
  allMeasurements: PropTypes.arrayOf(PropTypes.object).isRequired,
  updateMeasurement: PropTypes.func.isRequired,
  removeFromMeasurementList: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
  tokenRefresh: PropTypes.func.isRequired,
  serverList: PropTypes.array.isRequired,
  status: PropTypes.string.isRequired,
  showAuthError: PropTypes.bool,
  networkError: PropTypes.string,
  serverError: PropTypes.string,
  syncing: PropTypes.number,
  lastObservation: PropTypes.object,
  selectObservation: PropTypes.func.isRequired,
  selectDatabase: PropTypes.func.isRequired,
  selectServer: PropTypes.func.isRequired,
  resetDatabaseSelection: PropTypes.func.isRequired,
  resetServerSelection: PropTypes.func.isRequired,
  fetchingUsers: PropTypes.array.isRequired,
  syncingTrackLogsNumber: PropTypes.number.isRequired,
};

UnsyncedObservationsScreen.defaultProps = {
  showAuthError: false,
  networkError: null,
  serverError: null,
  syncing: false,
  lastObservation: null,
};

const mapStateToProps = ({
  servers: { data: serverList, error: serverError },
  network: { status, error: networkError },
  trackLog: { trackLogList },
  settings: { isSuccessNotificationSound },
  shortcutObservations,
}) => {
  // Filter out null records that contain only session related info.
  const allMeasurements = getFilteredMeasurements(
    serverList,
    false,
    (measurement) => !measurement?.isSynced,
  );
  const syncing = allMeasurements.filter(
    ({ measurement }) => measurement.syncing,
  ).length;
  const syncingTrackLogsNumber = trackLogList.filter(
    ({ syncing }) => syncing,
  ).length;
  const lastObservation =
    allMeasurements.length === 0 ? null : getLastObservation(allMeasurements);
  // Show auth error only when there are unsynced measurements
  // belonging to a server with auth error.
  let showAuthError = false;
  const fetchingUsers = [];
  serverList.forEach((serverItem) => {
    const { user = {}, id: serverId, url: serverUrl } = serverItem;
    if (
      !showAuthError &&
      user.error === ERROR.INVALID_REFRESH_TOKEN &&
      allMeasurements.some(
        ({ server, measurement }) =>
          server.id === serverId && !measurement.isSynced,
      )
    ) {
      showAuthError = true;
    }
    if (user.fetching)
      fetchingUsers.push({ serverUrl, userName: user.userName });
  });

  return {
    allMeasurements,
    serverList,
    status,
    showAuthError,
    networkError,
    serverError,
    syncing,
    lastObservation,
    fetchingUsers,
    syncingTrackLogsNumber,
    shortcutObservations,
    isSuccessNotificationSound,
  };
};

const mapDispatchToProps = (dispatch) => {
  const {
    updateMeasurement,
    removeFromMeasurementList,
    tokenRefresh,
    selectObservation,
    selectDatabase,
    selectServer,
    resetDatabaseSelection,
    resetServerSelection,
  } = actions;

  return bindActionCreators(
    {
      updateMeasurement,
      removeFromMeasurementList,
      tokenRefresh,
      selectObservation,
      selectDatabase,
      selectServer,
      resetDatabaseSelection,
      resetServerSelection,
    },
    dispatch,
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UnsyncedObservationsScreen);
