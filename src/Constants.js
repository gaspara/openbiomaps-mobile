import { FileSystem } from 'react-native-unimodules';
import packageJson from '../package.json';

export const APP_VERSION = `OBM_mobile-r4_${packageJson.version}`;
export const UNSYNCED_MEASUREMENTS_NUM_LIMIT = 500;
/* DYNAMIC_CODE_UPDATE >>>
 * custom_app_version */
/* <<< DYNAMIC_CODE_UPDATE */

export const LANG = [
  { id: 'hu', value: 'Magyar' },
  { id: 'en', value: 'English' },
  { id: 'ro', value: 'Română' },
  { id: 'es', value: 'Español' },
];

export const MAP_ENGINE = [
  { id: 'google', value: 'Google Maps' },
  { id: 'osm', value: 'OpenStreetMap' },
];

export const ERROR = {
  INVALID_REFRESH_TOKEN: 'Invalid refresh token',
  INVALID_ACCESS_TOKEN: 'The access token provided is invalid',
  EXPIRED_REFRESH_TOKEN: 'Refresh token has expired',
  FORM_UNAVAILABLE: 'Form unavailable',
  REQUEST_TIMEOUT: 'Request timeout',
  ACCESS_TOKEN_MISSING: 'Access token is missing',
  REFRESH_TOKEN_MISSING: 'Refresh token is missing',
  FORM_ACCESS_DENIED: 'Form access denied.',
  NETWORK_ERROR: 'Network Error',
  LATE_RESPONSE_TRIAL_CALL: 'Late response of the server checking call',
  DATA_ALREADY_UPLOADED: 'Data already uploaded!',
  UNKNOWN_ERROR: 'Unknown error',
  SERVER_ERRROR: 'Internal Server Error',
  PERMISSION_DENIED: 'Permission denied',
  WRONG_CONTENT_URI_FILE_PATH_ERROR: 'For input string',
  EMPTY_TRACKLOG: 'Tracklog does not contain any point',
  ROOT_DIR_SET: 'Root directory set',
};

export const GPS_ERROR = {
  1: 'location_permission_denied',
  2: 'no_gps',
  3: 'weak_gps',
  4: 'play_services_not_available',
  5: 'location_settings_not_appropriate',
};

export const GPS_OBSERVER = {
  MAP: 'map',
  TRACKLOG: 'tracklog',
  FORM: 'form',
  SESSION_TRACKLOG: 'sessiontracklog',
};

export const REQUEST_ERROR = {
  WRONG_FILE_URI: 'Could not retrieve file for uri',
};

export const PIN_SAVE_LOGIC = {
  SAVE: 'pin_save_option', // save the pin's state into the stickinesses prop of the form's reducer after exiting form
  DONT_SAVE: 'pin_dont_save_option', // dont save the state and let it be a local state
  SAVE_SYNC_DEL: 'pin_save_but_delete_after_sync', // save into redux but wipe out after syncing measurement of the specified observation type
};

export const MEMORY_ALERT_PERCENTAGE = 0.8;
export const MEMORY_LOW_LIMIT = 100;

export const TRACKLOG_ACCURACY_LIMIT = 50;
export const POSITION_ACCURACY_LIMIT = 50;

export const SESSION_TRACKLOG_DEFAULT_UPDATE_INTERVAL = 2000;

export const WKT_TYPES = ['point', 'line', 'polygon'];

export const GEOM_EDIT_MODE = {
  USE_EXISTING: 'use_existing',
  MAKE_NEW: 'make_new',
};

export const SESSION_TRACKLOG_META_INFO = {
  FOUND: 'tracklog_found',
  NOT_FOUND: 'tracklog_not_found',
  DELETED: 'tracklog_deleted_manually',
  UPLOADED: 'tracklog_already_uploaded',
  NOT_SET: 'tracklog_info_not_set',
};

export const FILE_STORAGE_DIRECTORY = `${FileSystem.documentDirectory}attachedFiles/`;

export const FALLBACK_POSITION = { latitude: 0, longitude: 0 };
export const DELTAS = { latitudeDelta: 0.0082, longitudeDelta: 0.0051 };

export const GEOMETRY_TYPES = ['wkt', 'point', 'line', 'polygon'];
