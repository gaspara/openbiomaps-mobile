import { combineReducers } from 'redux';
import { stateMapper } from './itemProperty';
import { SERVER_LIST_TYPE, DATABASE_LIST_TYPE } from '../actions/types';
import list from './list';
import database from './database';
import user from './user';

const databaseList = list(database, DATABASE_LIST_TYPE);

const initialState = {
  id: null,
  url: null,
  name: null,
  isSelected: false,
  lastSelected: null,
  hidden: null,
};

const serverReducer = combineReducers({
  ...stateMapper(initialState, SERVER_LIST_TYPE),
  user,
  databases: databaseList,
});

export default serverReducer;
