import {
  START_GPS_RECORDING,
  STOP_GPS_RECORDING,
  SUBSCRIBE_TO_GPS,
  UNSUBSCRIBE_FROM_GPS,
  SET_GPS_POSITION,
  SET_GPS_ERROR,
} from '../actions/types';
  
const initialState = {
  position: {
    latitude: null,
    longitude: null,
    accuracy: null,
    timestamp: null,
  },
  isRecording: false,
  error: {
    code: null,
    message: null,
  },
  observers: [],
};
  
export default (state = initialState, action) => {
  switch (action.type) {
    case START_GPS_RECORDING: {
      return {
        ...state,
        ...initialState,
        isRecording: true,
      };
    }
    case STOP_GPS_RECORDING: {
      return {
        ...state,
        ...initialState,
      }; 
    }
    case SUBSCRIBE_TO_GPS: {
      const { observer } = action.payload;
      return {
        ...state,
        observers: state.observers.concat(observer),
      };
    }
    case UNSUBSCRIBE_FROM_GPS: {
      const { observer } = action.payload;
      return {
        ...state,
        observers: state.observers.filter((item) => item !== observer),
      };
    }
    case SET_GPS_POSITION: {
      const { position } = action.payload;
      return {
        ...state,
        position: {
          ...state.position,
          ...position,
        },
        error: {
          ...initialState.error,
        },
      };
    }
    case SET_GPS_ERROR: {
      const { error } = action.payload;
      return {
        ...state,
        position: {
          ...initialState.position,
        },
        error: {
          ...state.error,
          ...error,
        },
      };
    }
    default:
      return state;
  }
};
