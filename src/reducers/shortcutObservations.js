import { 
  ADD_TO_SHORTCUT_OBSERVATION_LIST, 
  REMOVE_FROM_SHORTCUT_OBSERVATION_LIST, 
  PURGE_SHORTCUT_OBSERVATION_LIST,
} from '../actions/types';
  
export default (state = [], action) => {
  switch (action.type) {
    case ADD_TO_SHORTCUT_OBSERVATION_LIST: {
      const index = state.findIndex(({ observation }) => 
        observation.id === action.payload.observation.id);

      // if not in array, just concat it
      if (index < 0) {
        return state.concat(action.payload);
      }
    
      // if already there, replace it
      const newArray = [...state];
      newArray[index] = action.payload;
      return newArray;
    }
    case REMOVE_FROM_SHORTCUT_OBSERVATION_LIST: {
      const { serverId, databaseId, observationId } = action;
      const filteredArray = state.filter(({ server, database, observation }) => {
        if (
          server.id === serverId && 
            database.id === databaseId && 
            observation.id === observationId) {
          return false;
        }
        return true;
      });
      return filteredArray;
    }
    case PURGE_SHORTCUT_OBSERVATION_LIST:
      return [];
    default:
      return state;
  }
};
