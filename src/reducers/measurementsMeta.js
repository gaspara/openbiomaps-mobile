import { 
  MEASUREMENT_CREATE_SUCCESS, 
  MEASUREMENT_UPLOAD_SUCCESS, 
  DELETE_UNSYNCED_MEASUREMENT, 
  DELETE_SYNCED_MEASUREMENT,
  DELETE_ALL_SYNCED_MEASUREMENT,
  REPAIR_SYNCED_MEASUREMENT,
  REPAIR_UNSYNCED_MEASUREMENT,
} from '../actions/types';

const initialState = {
  synced: 0,
  unsynced: 0,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case MEASUREMENT_CREATE_SUCCESS:
      return {
        ...state,
        unsynced: state.unsynced + 1,
      };
    case MEASUREMENT_UPLOAD_SUCCESS: 
      return {
        ...state,
        synced: state.synced + action.value,
        unsynced: Math.max(state.unsynced - action.value, 0),
      }; 
    case DELETE_SYNCED_MEASUREMENT:
      return {
        ...state,
        synced: Math.max(state.synced - action.value, 0),
      };
    case DELETE_UNSYNCED_MEASUREMENT:
      return {
        ...state,
        unsynced: Math.max(state.unsynced - action.value, 0),
      };
    case DELETE_ALL_SYNCED_MEASUREMENT:
      return {
        ...state,
        synced: 0,
      };
    case REPAIR_UNSYNCED_MEASUREMENT:
      return {
        ...state,
        unsynced: action.value,
      };
    case REPAIR_SYNCED_MEASUREMENT:
      return {
        ...state,
        synced: action.value,
      };
    default:
      return state;
  }
};
