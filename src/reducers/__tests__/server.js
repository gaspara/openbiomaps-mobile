import reducer from '../server';
import { SELECT_SERVER } from '../../actions/types';

describe('server reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(null);
  });

  it('should handle SELECT_SERVER', () => {
    const state = {
      id: 0,
      name: 'server 0',
    };

    const nextState = {
      id: 1,
      name: 'server 1',
    };

    expect(reducer(state, {
      type: SELECT_SERVER,
      payload: nextState,
    })).toEqual(nextState);
  });
});
