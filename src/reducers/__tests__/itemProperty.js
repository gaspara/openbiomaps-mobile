import { reducer } from '../itemProperty';
import { ADD_TO_DATABASE_LIST, DATABASE_LIST_TYPE } from '../../actions/types';

describe('item property reducer', () => {
  it('should return the new property', () => {
    const action = {
      type: ADD_TO_DATABASE_LIST,
      payload: 'test',
    };
    expect(reducer(null, action, DATABASE_LIST_TYPE)).toEqual('test');
  });
});
