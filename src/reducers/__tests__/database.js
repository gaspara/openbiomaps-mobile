import databaseReducer from '../database';
import list from '../list';
import { ADD_TO_DATABASE_LIST, DATABASE_LIST_TYPE } from '../../actions/types';

const databaseList = list(databaseReducer, DATABASE_LIST_TYPE);

describe('database reducer', () => {
  it('should return the initial state', () => {
    expect(databaseReducer(undefined, {})).toEqual({
      description: null,
      game: 'off',
      id: null,
      isSelected: false,
      lastSelected: null,
      name: null,
      observations: [],
      projectUrl: null,
      settingsUrl: null,
      toplist: [],
    });
  });

  it('should handle ADD_TO_DATABASE_LIST', () => {
    expect(databaseList(undefined, {
      type: ADD_TO_DATABASE_LIST,
      payload: {
        id: 0,
        name: 'database 1',
        settingsUrl: 'http://test-case.url',
        description: 'test',
      },
    })).toEqual([{
      id: 0,
      isSelected: false,
      name: 'database 1',
      observations: [],
      projectUrl: null,
      settingsUrl: 'http://test-case.url',
      description: 'test',
      game: 'off',
      lastSelected: null,
      toplist: [],
    }]);
  });

  it('should handle ADD_TO_DATABASE_LIST if we have a default state', () => {
    const state = [{
      id: 0,
      isSelected: false,
      name: 'database 1',
      observations: [],
      settingsUrl: 'http://test-case.url',
      description: 'test',
      game: 'off',
      lastSelected: null,
      toplist: [],
    }];

    const action = {
      type: ADD_TO_DATABASE_LIST,
      payload: {
        id: 1,
        name: 'database 2',
        settingsUrl: 'http://test-case.url',
        description: 'test',
      },
    };

    expect(databaseList(state, action)).toEqual([{
      id: 0,
      isSelected: false,
      name: 'database 1',
      observations: [],
      settingsUrl: 'http://test-case.url',
      description: 'test',
      game: 'off',
      lastSelected: null,
      toplist: [],
    }, {
      id: 1,
      isSelected: false,
      name: 'database 2',
      observations: [],
      projectUrl: null,
      settingsUrl: 'http://test-case.url',
      description: 'test',
      game: 'off',
      lastSelected: null,
      toplist: [],
    }]);
  });
});
