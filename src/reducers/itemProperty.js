

export const reducer = (state = null, action, type) => {
  switch (action.type) {
    case `ADD_TO_${type}_LIST`:
      // user != because we want to filter the undefined and null values
      return action.payload != null ? action.payload : state;
    default:
      return state;
  }
};

const isObject = value => value && typeof value === 'object' && value.constructor === Object;

/**
 * Connect each property to the reducer
 * @param {object} initialState - The object of the mapped properties
 * @param {string} type - The unique part of the property's type
 * @param {string} propertyKey - The key of the property
 */
function connectToReducer(initialState, type, propertyKey) {
  return (state = initialState, action) => {
    // const subState = state ? state[propertyKey] : null;
    const subState = isObject(state) ?
      state[propertyKey] : state;

    const subAction = {
      type: action.type,
      payload: action.payload ? action.payload[propertyKey] : action.payload,
    };

    return reducer(subState, subAction, type);
  };
}

/**
 * Mapping trough the state object and connect the properties to the reducer
 * @param {object} initialState - The object of the mapped properties
 * @param {string} type - The unique part of the property's type
 */
export const stateMapper = (initialState, type) => {
  const keys = Object.keys(initialState);
  const res = {};
  keys.forEach((key) => {
    res[key] = connectToReducer(initialState, type, key);
  });
  return res;
};

