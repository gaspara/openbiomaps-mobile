import 'react-native-get-random-values'; // Required as polyfill for the global crypto.getRandomValues used by uuid
import { v4 as uuidv4 } from 'uuid';

import {
  START_TRACKLOG_RECORDING,
  STOP_TRACKLOG_RECORDING,
  SAVE_CURRENT_TRACKLOG,
  SAVE_SESSION_TRACKLOG,
  DELETE_TRACKLOG,
  DELETE_ALL_SYNCED_TRACKLOG,
  START_SYNC_FOR_TRACKLOG_LIST,
  STOP_SYNC_FOR_TRACKLOG_LIST,
  SYNC_SUCCESS_TRACKLOG_LIST,
  FETCH_SYNC_ERROR_TRACKLOG_LIST,
  INIT_CURRENT_TRACKLOG,
  DELETE_CURRENT_TRACKLOG,
  ADD_TO_CURRENT_TRACKLOG,
  EDIT_CURRENT_TRACKLOG,
  ADD_POINT_TO_GLOBAL_TRACKLOG,
  ADD_POINT_TO_SESSION_TRACKLOGS,
  ADD_NEW_SESSIONS_TO_TRACKLOGS,
  EDIT_SESSION_TRACKLOGS,
} from '../actions/types';
import { generateTrackLogName, setTrackLogsSyncingProp, setTrackLogSyncResults } from '../utils';

const initialState = {
  trackLogList: [],
  recordingInProgress: false,
  currentTrackLog: null,
  currentSessionTrackLogs: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case START_TRACKLOG_RECORDING:
      return {
        ...state,
        recordingInProgress: true,
      };
    case STOP_TRACKLOG_RECORDING:
      return {
        ...state,
        recordingInProgress: false,
      };
    case INIT_CURRENT_TRACKLOG: {
      const { trackLog } = action.payload;
      return {
        ...state,
        currentTrackLog: trackLog,
      };
    }
    case ADD_TO_CURRENT_TRACKLOG: {
      const { point } = action.payload;
      return {
        ...state,
        currentTrackLog: state.currentTrackLog ? {
          ...state.currentTrackLog,
          trackLogArray: [...state.currentTrackLog.trackLogArray, point],
        } : null,
      };
    }
    case ADD_POINT_TO_GLOBAL_TRACKLOG: {
      const { point } = action.payload;
      return {
        ...state,
        currentTrackLog: {
          ...state.currentTrackLog,
          trackLogArray: [...state.currentTrackLog.trackLogArray, point],
        },
      };
    }
    case ADD_POINT_TO_SESSION_TRACKLOGS: {
      const { point } = action.payload;
      return {
        ...state,
        currentSessionTrackLogs: state.currentSessionTrackLogs.map((sessionTrackLog) => ({
          ...sessionTrackLog,
          trackLogArray: [...sessionTrackLog.trackLogArray, point],
        })),
      };
    }
    case ADD_NEW_SESSIONS_TO_TRACKLOGS: {
      const { newSessions } = action.payload;
      return {
        ...state,
        currentSessionTrackLogs: [...state.currentSessionTrackLogs, ...newSessions],
      };
    }
    case EDIT_SESSION_TRACKLOGS: {
      const { sessionTrackLogs } = action.payload;
      return {
        ...state,
        currentSessionTrackLogs: sessionTrackLogs,
      };
    }
    case EDIT_CURRENT_TRACKLOG: {
      const { trackLog } = action.payload;
      return {
        ...state,
        currentTrackLog: {
          ...state.currentTrackLog,
          ...trackLog,
        },
      };
    }
    case SAVE_CURRENT_TRACKLOG: {
      return state.currentTrackLog ? {
        ...state,
        trackLogList: [...state.trackLogList, {
          ...state.currentTrackLog,
          trackLogId: uuidv4(),
          syncing: false,
          endTime: Date.now(),
          trackLogName: generateTrackLogName({ ...state.currentTrackLog, endTime: Date.now() }),
          selectedDatabases: state.currentTrackLog.selectedDatabases?.map((database) => ({ ...database, isSynced: false })) || [],
        }],
        currentTrackLog: null,
      } : state;
    }
    case SAVE_SESSION_TRACKLOG: {
      const { trackLog } = action.payload;
      const {
        startTime,
        sessionId,
        selectedDatabases,
      } = trackLog;
      return {
        ...state,
        trackLogList: [...state.trackLogList, {
          ...trackLog,
          trackLogId: uuidv4(),
          syncing: false,
          endTime: Date.now(),
          trackLogName: generateTrackLogName({ startTime, endTime: Date.now(), selectedDatabases, sessionId }),
          selectedDatabases: selectedDatabases.map((database) => ({ ...database, isSynced: false })),
        }],
      };
    }
    case DELETE_TRACKLOG: {
      const { trackLogId: idToDelete } = action.payload;
      return {
        ...state,
        trackLogList: state.trackLogList.filter(({ trackLogId }) => trackLogId !== idToDelete),
      };
    }
    case DELETE_CURRENT_TRACKLOG: 
      return {
        ...state,
        currentTrackLog: null,
      };
    case DELETE_ALL_SYNCED_TRACKLOG:
      return {
        ...state,
        trackLogList: state.trackLogList.filter(({ selectedDatabases }) => selectedDatabases.some(({ isSynced }) => !isSynced)),
      };
    case START_SYNC_FOR_TRACKLOG_LIST: {
      const { trackLogs } = action.payload;
      return {
        ...state,
        trackLogList: setTrackLogsSyncingProp(trackLogs, state.trackLogList, true),
      };
    }
    case STOP_SYNC_FOR_TRACKLOG_LIST: {
      const { trackLogs } = action.payload;
      return {
        ...state,
        trackLogList: setTrackLogsSyncingProp(trackLogs, state.trackLogList, false),
      };
    }
    case SYNC_SUCCESS_TRACKLOG_LIST: {
      const { syncResults } = action.payload;
      return {
        ...state,
        trackLogList: setTrackLogSyncResults(syncResults, state.trackLogList, true),
      };
    }
    case FETCH_SYNC_ERROR_TRACKLOG_LIST: {
      const { syncResults } = action.payload;
      return {
        ...state,
        trackLogList: setTrackLogSyncResults(syncResults, state.trackLogList, false),
      };
    }
    default:
      return state;
  }
};
