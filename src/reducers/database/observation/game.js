import { GAME_FETCH_SUCCESS, QUIZ_PASSED } from '../../../actions/types';

export default (state = null, action) => {
  switch (action.type) {
    case GAME_FETCH_SUCCESS:
      return {
        ...state,
        ...action.payload,
      };
    case QUIZ_PASSED:
      return {
        ...state,
        passedQuiz: true,
      };
    default:
      return state;
  }
};

