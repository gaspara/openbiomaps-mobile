import {
  SET_SYNC_STATUS,
  SYNC_DATA_START,
  ADD_TO_MEASUREMENT_LIST,
  UPDATE_MEASUREMENT,
} from '../../../actions/types';

const initialState = {
  isSynced: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_MEASUREMENT_LIST: {
      const { id, data, meta } = action.measurement;
      return {
        ...state,
        id,
        data,
        meta,
        date: action.date,
      };
    }
    case SYNC_DATA_START: {
      return {
        ...state,
        syncing: true,
        isSynced: false,
      };
    }
    case SET_SYNC_STATUS: {
      const { payload: { status, errors } } = action;
      return {
        ...state,
        errors,
        syncing: false,
        isSynced: status,
      };
    }
    case UPDATE_MEASUREMENT: {
      const { data, meta } = action.measurement;

      return {
        ...state,
        syncing: false,
        isSynced: false,
        data,
        meta: {
          ...state.meta,
          ...meta,
        },
        errors: null,
      };
    }
    default:
      return state;
  }
};
