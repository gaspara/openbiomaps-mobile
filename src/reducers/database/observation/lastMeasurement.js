import {
  ADD_TO_MEASUREMENT_LIST,
} from '../../../actions/types';

export default (state = {}, action) => {
  switch (action.type) {
    case ADD_TO_MEASUREMENT_LIST: {
      const { id, data } = action.measurement;
      if (data) {
        return {
          id,
          ...data,
        };
      }
      return state;
    }
    default:
      return state;
  }
};
