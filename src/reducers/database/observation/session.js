import {
  START_SESSION,
  END_SESSION,
  ADD_TO_MEASUREMENT_LIST,
  ADD_TRACKLOG_TO_SESSION,
} from '../../../actions/types';

/**
 * @param {object} state - { id, startedAt, mustFinishAt, measurementsNum, sessionTrackLog, shouldRecordTrackLog }
 */
export default (state = null, action) => {
  switch (action.type) {
    case START_SESSION: {
      const { id, startedAt, mustFinishAt, shouldRecordTrackLog } = action;
      return {
        ...state,
        id,
        startedAt,
        mustFinishAt,
        shouldRecordTrackLog,
      };
    }
    case END_SESSION:
      return null;
    case ADD_TO_MEASUREMENT_LIST: {
      if (state) {
        return {
          ...state,
          measurementsNum: state.measurementsNum ? state.measurementsNum + 1 : 1,
        };
      }
      return state;
    }
    case ADD_TRACKLOG_TO_SESSION: {
      const { sessionTrackLog } = action;
      if (state) {
        return {
          ...state,
          sessionTrackLog: state.sessionTrackLog ? [
            ...state.sessionTrackLog,
            ...sessionTrackLog,
          ] : sessionTrackLog,
        };
      }
      return state;
    }
    default:
      return state;
  }
};
