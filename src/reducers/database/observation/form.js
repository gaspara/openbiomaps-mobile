import {
  FORM_DATA_FETCH_SUCCESS,
  FORM_DATA_FETCH_START,
  FORM_DATA_FETCH_ERROR,
  FORM_DATA_SAVE_STICKINESSES,
  FORM_DATA_REMOVE_STICKINESSES,
  FORM_DATA_SAVE_LAST_PICKED_COLUMN_VALUE,
  FORM_DATA_PURGE_LAST_PICKED_COLUMN_VALUE,
} from '../../../actions/types';

const initialState = {
  fetching: false,
  error: null,
  dataTypes: [],
  permanentSamplePlotInfos: [],
  boldYellow: [],
  loginEmail: null,
  loginName: null,
  numInd: null,
  observationListMode: 'false',
  listTime: null,
  stickinesses: {},
  lastPickedValues: {},
  trackLogMode: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FORM_DATA_FETCH_START:
      return {
        ...state,
        fetching: true,
        error: null,
      };
    case FORM_DATA_FETCH_ERROR:
      return {
        ...state,
        fetching: false,
        error: action.error,
      };
    case FORM_DATA_FETCH_SUCCESS:
      return {
        ...state,
        fetching: false,
        error: null,
        ...action.payload,
      };
    case FORM_DATA_SAVE_STICKINESSES:
      return {
        ...state,
        stickinesses: action.payload,
      };
    case FORM_DATA_REMOVE_STICKINESSES:
      return {
        ...state,
        stickinesses: {},
      };
    case FORM_DATA_SAVE_LAST_PICKED_COLUMN_VALUE: {
      const { column, value } = action.payload;
      const tempLastPicked = state.lastPickedValues || {};
      if (!tempLastPicked[column]?.includes(value)) {
        if (tempLastPicked[column] === undefined) tempLastPicked[column] = [];

        tempLastPicked[column].unshift(value);
        tempLastPicked[column] = tempLastPicked[column].slice(0, 15);
        return {
          ...state,
          lastPickedValues: tempLastPicked,
        };
      } else if (tempLastPicked[column]?.includes(value)) {
        const selectedIndex = tempLastPicked[column]?.indexOf(value);
        tempLastPicked[column]?.splice(selectedIndex, 1);
        tempLastPicked[column].unshift(value);
        tempLastPicked[column] = tempLastPicked[column].slice(0, 15);

        return {
          ...state,
          lastPickedValues: tempLastPicked,
        };
      }
      return state;
    }
    case FORM_DATA_PURGE_LAST_PICKED_COLUMN_VALUE:
      return {
        ...state,
        lastPickedValues: {},
      };
    default:
      return state;
  }
};
