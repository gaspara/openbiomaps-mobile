import { CHANGE_SETTINGS_DATA } from '../actions/types';
import { MAP_ENGINE, PIN_SAVE_LOGIC } from '../Constants';

const initialState = {
  isBackupEnabled: true,
  isAutoSyncEnabled: false,
  isSuccessNotificationSound: false,
  language: {
    id: null,
    value: null,
  },
  mapEngine: {
    id: MAP_ENGINE[0].id,
    value: MAP_ENGINE[0].value,
  },
  pinSaveLogic: PIN_SAVE_LOGIC.DONT_SAVE,
  dateFilterOnMapSettings: {
    filterData: false,
    startTime: null,
    endTime: null,
    setTime: null,
  },
  externalBackupDirectory: null,
  externalTracklogDirectory: null,
  externalTrackLogExportDirectory: null,
  externalExportDirectory: null,
  trackLogUpdateInterval: 1000,
  distanceFilter: 1,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_SETTINGS_DATA: {
      const { key, value } = action;
      return {
        ...state,
        [key]: value,
      };
    }
    default:
      return state;
  }
};
