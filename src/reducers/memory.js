import { MEMORY_USAGE_NORMALIZED, TOO_MUCH_MEMORY_USED, SHOULD_SEND_MEMORY_ALERT } from '../actions/types';

const INITIAL_STATE = { hasEnoughMemory: true, memoryUsageCritical: false };

export default (state = INITIAL_STATE, { type }) => {
  switch (type) {
    case MEMORY_USAGE_NORMALIZED:
      return {
        ...state,
        hasEnoughMemory: true,
        memoryUsageCritical: false,
      };
    case TOO_MUCH_MEMORY_USED:
      return {
        ...state,
        hasEnoughMemory: false,
      };
    case SHOULD_SEND_MEMORY_ALERT:
      return {
        ...state,
        memoryUsageCritical: true,
      };
    default:
      return state;
  }
};

