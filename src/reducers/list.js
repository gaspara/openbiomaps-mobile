import { GAME_CONTENT_FETCH_START, GAME_CONTENT_FETCH_SUCCESS, GAME_CONTENT_FETCH_ERROR, SERVER_LIST_TYPE, OBSERVATION_LIST_TYPE, FORM_DATA_PURGE_LAST_PICKED_COLUMN_VALUE } from '../actions/types';

const INITIAL_STATE = {
  data: [],
  fetching: false,
  error: null,
};

export default function list(reducer, listType) {
  return (state = INITIAL_STATE, action) => {
    const {
      id,
      action: innerAction,
    } = action;

    switch (action.type) {
      case `${listType}_FETCH_START`:
        return {
          ...state,
          fetching: true,
          error: null,
        };
      case `${listType}_FETCH_SUCCESS`: {
        // Update local array based on array coming from the server
        let list = action.payload.map((listItem) => {
          const orig = state.data.find(item => item.id === listItem.id);
          if (orig) {
            if (orig.hidden) {
              orig.hidden = false;
            }
            if (listType === OBSERVATION_LIST_TYPE && orig.formVersion !== listItem.formVersion) {
              const purgedOrig = reducer(orig, { type: FORM_DATA_PURGE_LAST_PICKED_COLUMN_VALUE });
              return {
                ...purgedOrig,
                ...listItem,
              };
            }
            return {
              ...orig,
              ...listItem,
            };
          }
          return { ...listItem };
        });

        // Find and hide items deleted from the server
        if (list.length <= state.data.length) {
          const removedItems = [];
          state.data.forEach((item) => {
            if (!list.some(listItem => listItem.id === item.id)) {
              if (listType !== SERVER_LIST_TYPE) {
                removedItems.push({
                  ...item,
                  hidden: true,
                  lastSelected: null,
                  session: null,
                });
              } else {
                removedItems.push({
                  ...item,
                  hidden: true,
                  lastSelected: null,
                });
              }
            }
          });
          // Items that are deleted from the server aren't deleted locally
          // instead they are kept and hidden from the user (hidden: true)
          // sometimes an item is deleted only temporarily, hidden flag has to be reseted after every call
          list = [...list, ...removedItems];
        }

        return {
          ...state,
          data: list,
          fetching: false,
          error: null,
        };
      }
      case `${listType}_FETCH_ERROR`:
        return {
          ...state,
          fetching: false,
          error: action.error,
        };
      case `ADD_TO_${listType}_LIST`:
        return {
          ...state,
          data: state.data.concat(reducer(undefined, action)),
        };
      case `REMOVE_FROM_${listType}_LIST`: {
        const newArray = state.data.filter(stateItem => stateItem.id !== id);
        return {
          ...state,
          data: newArray,
        };
      }
      case `REMOVE_MORE_FROM_${listType}_LIST`: {
        // the id here is an array of id-s
        const newArray = state.data.filter(stateItem => !id.includes(stateItem.id));
        return {
          ...state,
          data: newArray,
        };
      }
      case `SELECT_${listType}`: {
        const { id, date } = action;
        const list = state.data.map((item) => {
          if (item.id === id) {
            return {
              ...item,
              isSelected: true,
              lastSelected: date,
            };
          }
          return { ...item, isSelected: false };
        });
        
        return {
          ...state,
          data: list,
        };
      }
      case `UPDATE_${listType}`: {
        const { id, payload } = action;
        const newData = state.data.map((item) => {
          if (item.id === id) {
            return { ...payload };
          }
          return item;
        });
        return {
          ...state, 
          data: newData,
        };
      }
      case `PERFORM_IN_${listType}_LIST`: {
        const list = state.data.map(item => ((item.id === id) 
          ? reducer(item, innerAction) 
          : item));

        return {
          ...state,
          data: list,
        };
      }
      case `PERFORM_ON_THE_WHOLE_${listType}_LIST`: {
        // console.log('i have to perform on the whole list an inneraction:');
        // console.log(innerAction);
        const list = state.data.map(item => (id.includes(item.id)
          ? reducer(item, innerAction)
          : item));
        return {
          ...state,
          data: list,
        };
      }
      case `RESET_${listType}_SELECTION`: {
        const list = state.data.map(item => ({
          ...item,
          isSelected: false,
        }));
        
        return {
          ...state,
          data: list,
        };
      }
      case `PURGE_${listType}_DATA`: {
        return {
          ...state,
          data: [],
        };
      }
      case GAME_CONTENT_FETCH_START: 
        return {
          ...state,
          gameFetching: true,
          error: null,
        };
      case GAME_CONTENT_FETCH_SUCCESS:
        return {
          ...state,
          gameFetching: false,
          error: null,
        };
      case GAME_CONTENT_FETCH_ERROR:
        return {
          ...state,
          gameFetching: false,
          error: action.error,
        };
      case `CLEAR_${listType}_ERROR`:
        return {
          ...state,
          fetching: false,
          error: null,
        };
      default:
        return state;
    }
  };
}
