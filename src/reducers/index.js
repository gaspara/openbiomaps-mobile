import { persistCombineReducers, createTransform, createMigrate } from 'redux-persist';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import FilesystemStorage from 'redux-persist-filesystem-storage';
import { FileSystem } from 'react-native-unimodules';
import { SERVER_LIST_TYPE } from '../actions/types';

import list from './list';
import network from './network';
import server from './server';
import shortcutObservations from './shortcutObservations';
import settings from './settings';
import backup from './backup';
import gps from './gps';
import measurementsMeta from './measurementsMeta';
import memory from './memory';
import mapSelection from './mapSelection';
import messages from './messages';
import trackLog from './tracklog';
import { migrations } from '../migrations/migrations';

const MIGRATION_DEBUG = false;

const setPersistedState = createTransform(
  (inboundState) => inboundState,
  (outboundState) => outboundState,
  { whitelist: ['servers'] },
);

FilesystemStorage.config({
  storagePath: `${FileSystem.documentDirectory.replace('file://', '')}/persistStore`,
});

const config = {
  key: 'root',
  version: 2,
  storage: FilesystemStorage,
  transforms: [setPersistedState],
  stateReconciler: autoMergeLevel2,
  blacklist: ['network', 'backup', 'messages', 'gps'],
  migrate: createMigrate(migrations, { debug: MIGRATION_DEBUG }),
  timout: 0, // this will prevent the redux to reinitialize the state after a timeout. Maybe only devTools bug what can be avoided with it but worth a try source: https://github.com/rt2zz/redux-persist/issues/717
};

const serverList = list(server, SERVER_LIST_TYPE);

export default persistCombineReducers(config, {
  servers: serverList,
  shortcutObservations,
  settings,
  backup,
  gps,
  network,
  measurementsMeta,
  memory,
  mapSelection,
  messages,
  trackLog,
});
