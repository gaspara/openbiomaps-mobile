import { useCallback, useEffect, useRef } from 'react';

import SystemSetting from 'react-native-system-setting';

import { showAlertDialog } from './components';
import I18n from './i18n';

const AirplaneModeHandler = () => {
  const airplaneListener = useRef(null);

  const showAirplaneModeDialog = useCallback(() => {
    showAlertDialog({
      title: I18n.t('default_alert_title'),
      message: I18n.t('airplane_gps_error'),
      onPositiveBtnPress: () => {
        SystemSetting.switchAirplane();
      },
      showCancelButton: true,
    });
  }, []);

  const checkAirplaneMode = useCallback(async () => {
    const isAirplaneModeEnabled = await SystemSetting.isAirplaneEnabled();
    if (isAirplaneModeEnabled) {
      showAirplaneModeDialog();
    }
  }, [showAirplaneModeDialog]);

  const addAirplaneModeListener = useCallback(async () => {
    airplaneListener.current = await SystemSetting.addAirplaneListener((airplaneModeEnabled) => {
      if (airplaneModeEnabled) {
        showAirplaneModeDialog();
      }
    });
  }, [showAirplaneModeDialog]);

  const removeAirplaneModeListener = useCallback(() => {
    if (airplaneListener.current) {
      SystemSetting.removeListener(airplaneListener.current);
    }
  }, []);

  useEffect(() => {
    addAirplaneModeListener();
    return () => {
      removeAirplaneModeListener();
    };
  }, [addAirplaneModeListener, removeAirplaneModeListener]);

  useEffect(() => {
    checkAirplaneMode();
  }, [checkAirplaneMode]);

  return null;
};

export default AirplaneModeHandler;
