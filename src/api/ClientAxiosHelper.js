import * as Sentry from '@sentry/react-native';
import axios from 'axios';
import { isAccessTokenExpired, queryString, decodeQueryString } from '../utils';
import { ERROR, REQUEST_ERROR } from '../Constants';
import {
  dispatchNewTokens,
  dispatchNetworkError,
  dispatchClearNetworkError,
} from './ClientReduxHelper';
import ClientLogger, { ClientLoggerUseCases } from './ClientLogger';

const refreshLogic = async (config, client) => {
  // console.log('refresh logic called');
  // console.log(config);
  const { refreshToken, serverId, serverUrl } = config.authObject;
  // ask a new refresh token:
  try {
    ClientLogger.log(
      ClientLoggerUseCases.REFRESHING_IN_REFRESHLOGIC,
      config.authObject,
    );
    const newAuthObject = await client.refresh({ refreshToken, serverUrl });
    ClientLogger.log(
      ClientLoggerUseCases.DISPATCH_NEW_AUTHOBJECT,
      newAuthObject,
    );
    dispatchNewTokens(serverId, newAuthObject);
    // console.log('refresh hivas megtortent, új a token');

    const queryObj = decodeQueryString(config.data);
    queryObj.access_token = newAuthObject.accessToken;

    return { ...config, data: queryString(queryObj), authObject: null };
  } catch (e) {
    Sentry.captureMessage(`
      Interceptor calls refreshLogic
      serverUrl from config.authObject: ${serverUrl}
      serverId from config.authObject: ${serverId}
    `);
    Sentry.captureException(e);
    // az errort a refresh hívás kezelje és dobja, vagy a refresher interceptor
    throw e;
  }
};

export const createMainAxiosInstance = (client) => {
  const mainInstance = axios.create({
    timeout: 5000,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
    },
  });

  // set the default request interceptor
  // checks the accessToken based on expiresIn time, and refreshes if needed
  mainInstance.interceptors.request.use(
    async (config) => {
      // check if access token is valid
      // console.log('config a req interceptorban:');
      // console.log(config);
      // console.log(config);
      let newConfig = config;
      try {
        if (!config.data?.includes('put_data')) {
          ClientLogger.log(ClientLoggerUseCases.NEW_REQUEST, config.data);
        }
      } catch (e) {
        // console.log(e);
        // Do nothing
      }
      if (config.authObject) {
        if (isAccessTokenExpired(config.authObject)) {
          try {
            ClientLogger.log(
              ClientLoggerUseCases.ACCESS_EXPIRED_CALL_REFRESH_LOGIC,
              config.authObject,
            );
            newConfig = await refreshLogic(config, client);
          } catch (e) {
            return Promise.reject(e);
          }
        }

        return { ...newConfig, authObject: null };
      }
      return config;
    },
    (error) => Promise.reject(error),
  );

  // Set axios default response interceptor
  mainInstance.interceptors.response.use(
    ({ data, config }) => {
      // Handling errors occuring when the status code is 2xx, but the api return something else
      // console.log('interceptor data:');
      // console.log(data);

      if (data.status && data.status !== 'success') {
        return Promise.reject(data.message);
      }
      try {
        if (
          !config.data?.includes('put_data') &&
          !config.data?.includes('tracklog')
        ) {
          dispatchClearNetworkError();
        }
      } catch (e) {
        // Do nothing
      }

      return data;
    },
    async (error) => {
      console.log('AXIOS ERROR');
      console.error(error);
      console.log('AXIOS ERROR');
      // console.log('error a main interceptorban:');
      // console.log(error, error.message);

      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        /* 
        console.log(error.response);
        console.log('response data: ');
        console.log(error.response.data);
        console.log('response status: ');
        console.log(error.response.status);
        console.log('response headers: ');
        console.log(error.response.headers);
        */
        if (
          error.response?.data.message === ERROR.INVALID_ACCESS_TOKEN ||
          error.response.data.error === 'invalid_token'
        ) {
          ClientLogger.log(ClientLoggerUseCases.INVALID_ACCESS_TOKEN);
          return Promise.reject(ERROR.INVALID_ACCESS_TOKEN);
        }

        if (error.response.data.message === ERROR.DATA_ALREADY_UPLOADED) {
          return Promise.reject(ERROR.DATA_ALREADY_UPLOADED);
        }

        if (error.response.data?.error) {
          ClientLogger.log(
            ClientLoggerUseCases.UNEXPECTED_OAUTH_ERROR,
            error.response.data.error,
          );
          return Promise.reject(error.response.data.error); // unexpected oauth error messages
        } else if (error.response.data?.message) {
          // TODO maybe the response.status should come before response.data
          return Promise.reject(error.response.data.message); // pds error messages
        } else if (error.response.status >= 400) {
          return Promise.reject(error.response.statusText);
        }

        // TODO: the main goal here is enumerate all possible response message and send only ERROR.something constants
        // return Promise.reject(error.response.data.message);
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        // console.log('the request was made but no response');
        // console.log(error.request._hasError, error.request._response);
        if (error.request._hasError && error.request._response) {
          if (error.request._response.includes(REQUEST_ERROR.WRONG_FILE_URI)) {
            return Promise.reject(REQUEST_ERROR.WRONG_FILE_URI);
          }
          return Promise.reject(error.request._response);
        }
      } else {
        // Something happened in setting up the request that triggered an Error
        // console.log('other');
        // console.log('Error', error.message);
      }

      // Handling request timeout error
      if (error?.message.includes('timeout')) {
        dispatchNetworkError(ERROR.NETWORK_ERROR);
        return Promise.reject(ERROR.REQUEST_TIMEOUT);
      }
      // Handling other errors
      ClientLogger.log(
        ClientLoggerUseCases.OTHER_ERROR_IN_MAIN_INTERCEPTOR,
        error.message,
      );
      return Promise.reject(error.message || ERROR.UNKNOWN_ERROR);
    },
  );

  return mainInstance;
};

export const createRefreshAxiosInterceptor = () => {
  const refresherInstance = axios.create({
    timeout: 5000,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
    },
  });

  // set the refresh interceptor
  refresherInstance.interceptors.response.use(
    ({ data, config }) => {
      if (config.data?.includes('request_time') && data.data >= 6) {
        dispatchNetworkError(ERROR.LATE_RESPONSE_TRIAL_CALL);
        return Promise.reject(ERROR.LATE_RESPONSE_TRIAL_CALL);
      }
      dispatchClearNetworkError();
      return data;
    },
    (error) => {
      // console.log('error a refresh interceptorban:');
      // console.log(error);

      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        /*
            console.log('response data: ');
            console.log(error.response.data);
            console.log('response status: ');
            console.log(error.response.status);
            console.log('response headers: ');
            console.log(error.response.headers);
            */

        if (error.response.data.message) {
          return Promise.reject(error.response.data.message);
        }
        if (error.response.data.error_description) {
          return Promise.reject(error.response.data.error_description);
        }
        if (error.response.status >= 400) {
          return Promise.reject(error.response.statusText);
        }
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        // console.log('the request was made but no response');
        // console.log(error.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        // console.log('other');
        // console.log('Error', error.message);
        // Handling request timeout error
      }

      if (error?.message.includes('timeout')) {
        dispatchNetworkError(ERROR.NETWORK_ERROR);
        return Promise.reject(ERROR.REQUEST_TIMEOUT);
      }
      // Handling other errors
      return Promise.reject(error.message);
    },
  );

  return refresherInstance;
};
