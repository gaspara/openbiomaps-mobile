import I18n from 'react-native-i18n';
import hu from './locales/hu';
import en from './locales/en';
import ro from './locales/ro';
import es from './locales/es';

I18n.fallbacks = true;

I18n.translations = {
  hu,
  en,
  ro,
  es,
};

I18n.defaultLocale = 'en';

export default I18n;
