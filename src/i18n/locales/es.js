export default {
  username: 'Electrónico',
  password: 'Contraseña',
  login: 'Acceso',
  login_error: 'Se produjo un error durante el proceso de autenticación',
  empty_fields: 'Todos los puntos son obligatorios',
  invalid_username_password: 'Electrónico o contraseña invalido',
  database_screen_title: 'Seleccionar projecto',
  observation_screen_title: 'Seleccionar formulario',
  settings_online_profile: 'Configuración de perfil online',
  gps_timeout: 'Conectando a GPS…',
  gps_unavailable: 'La señal GPS no está disponible.',
  gps_disabled_hint: 'Se requiere permiso de ubicación para poder utilizar la aplicación correctamente.',
  error: 'Error',
  position_input_hint: '¿Dónde está la ubicación de tu observación?\n' +
    'Presiona prolongadamente en el sitio seleccionado.',
  form: 'Formulario',
  error_text_out_of_range: 'El texto debe tener entre {{min}} y {{max}} caracteres de longitud',
  error_number_out_of_range: 'El número debe estar entre {{min}} y {{max}}',
  error_required: 'Faltan puntos necesarios',
  error_nan: 'El valor dado no es un número.',
  bool_true: 'Sí',
  bool_false: 'No',
  task_screen_title: 'Tarea',
  questions_screen_title: 'Preguntas',
  game_observation: 'Observación del juego',
  player: 'jugador/a',
  score: 'puntos',
  toplist: 'lista de resultados',
  game: 'juego',
  quiz_error: 'Una o más respuestas son incorrectas.',
  measurements: 'Observaciones',
  measurement_nonvalid: 'Incorrecto',
  measurement_syncing: 'Sincronizando',
  measurement_pending: 'Esperando subida',
  measurement_uploaded: 'Subido',
  remove: 'eliminar',
  image_picker_title: 'Toma una foto o elige una de la galería.',
  cancel: 'Cancelar',
  camera: 'Cámara',
  gallery: 'Galería',
  measurement_updated_message: 'Registro actualizado.',
  latest_tab: 'Más reciente',
  offline_available_tab: 'Descargado',
  all_tab: 'Todo',
  missing_form_data: 'No se pudieron cargar los datos del formulario.',
  invalid_fields: '¡Comprueba los puntos con fondo rojo!',
  empty_list: 'Los datos no están disponibles.',
  needs_net: 'Se requiere una conexión de internét para cargar los datos.',
  image_load_failed: 'No se pudo cargar la imagen.',
  image_needs_net: 'Se requiere una conexión de internét para cargar la imagen.',
  img_loading: 'Cargando imagen…',
  measurement_under_editing: 'Modificando',
  ok: 'OK',
  form_item: 'Formulario {{count}}',
  collect_data: 'Formularios',
  collected_data: 'Datos registrados',
  map: 'Mapa',
  settings: 'Configuraciones',
  export: 'Exportar',
  servers: 'Servidores',
  choose_server: 'Seleccionar servidor',
  no_previous_measurements: 'Los registros anteriores no están disponibles',
  location_permission_denied: 'Permiso de ubicación denegado.',
  no_previous_observation_form: 'El formulario {{count}} no está disponible.',
  successful_form_creation: 'Registro de datos exitoso.',
  create_new_form: '¿Quieres registrar una nueva observación?',
  have_unsaved_changes: 'Tienes cambios no guardados.',
  do_you_want_to_exit: '¿Quieres salir del programa?',
  auto_backup: 'Copia de seguridad automática',
  restore_backup: 'Restaurando copia de seguridad',
  backup_confirm_title: '¿Estás seguro de restaurar datos a una versión anterior?',
  restore: 'Rstaurar',
  no_backup: 'La versión de copia de seguridad anterior no está disponible. ¿Te gustaría buscar copias de seguridad en el sistema de archivos manualmente?',
  invalid_refresh_token: 'Usted no se ha autentificado.',
  uncertain_location: 'Posición inexacta',
  no_gps: 'La señal GPS no está disponible',
  change_project: 'Cambiar proyecto',
  change_server: 'Cambiar servidor',
  fixed_position: 'Posición fija',
  synced: 'Sincronizado',
  unsynced: 'No sincronizado',
  no_synced_data: 'No hay datos sincronizados.',
  no_unsynced_data: 'Sin datos no sincronizados.',
  date: 'fecha',
  no_field_data: 'No hay datos registrados!',
  delete_dialog_title: '¿Estás seguro de eliminar esta observación?',
  delete_dialog_message: 'Los datos ya cargados en el servidor seguirán estando disponibles allí.',
  no_connection: 'La conexión de internet no está disponible.',
  sync_requires_net: 'Se necesita internet para la sincronización de datos.',
  backup: 'Copia de seguridad',
  sync: 'Sincronización',
  auto_sync: 'Sincronizacion automatica',
  form_is_not_available: 'El formulario no está disponible en el servidor!',
  about: 'Sobre',
  version: 'Versión',
  list_creation_in_progress: 'La creación de la sesión está en curso',
  general: 'General',
  language: 'Idioma',
  sync_with_wifi: 'Sincronizando datos automáticamente con la conexión wifi.',
  map_engine: 'Motor de mapas',
  do_you_want_to_save_the_list: '¿Quieres guardar la sesión?',
  no_data_recorded_in_the_list: 'No se registraron datos durante la observación. ¿No observó nada durante el seguimiento o no quiere guardar la sesión?',
  record_empty_observation: 'No observé nada',
  do_not_save_the_list: 'No quiero guardar la sesión',
  lot_of_unsynced_measurements: 'Tienes muchas observaciones no sincronizadas. ¡Sincronícalos lo antes posible!',
  has_measurement_older_than_one_week: 'Tienes observaciones no sincronizadas de más de una semana. ¡Sincronícalos lo antes posible!',
  fiel_common_fields: 'Complete los campos de la sesión común.',
  successful_list_creation: 'Grabación de sesión exitosa.',
  logout: 'Cerrar sesión',
  logout_dialog_title: '¿Seguro que quieres cerrar sesión?',
  logout_dialog_message: 'Tiene datos no sincronizados en el servidor.',
  logout_success: 'Cierre de sesión exitoso',
  logout_success_from: 'Se ha desconectado con éxito del siguiente servidor: {{server}}',
  null_record: 'registro nulo',
  create_null_record: 'Crear registro nulo',
  create_null_record_instruction: 'En este caso, crea un registro nulo. Escriba "nulo" en los campos de texto requeridos y "0" en los campos numéricos requeridos, además, proporcione los datos apropiados en los otros campos donde es necesario y luego guarda la observación.',
  not_enough_memory: 'No hay suficiente memoria para realizar la operación. ¡Sincronice tus medidas para liberar memoria!',
  critical_memory_usage: 'No hay suficiente memoria disponible, sincronica tus medidas!',
  internet_connection: 'Conectado al internet',
  time_spent: 'Tiempo transcurrido',
  delete_all: 'Eliminar todo',
  no_or_weak_network: 'No tienes conexión o tu conexión no es fuerte. Intenta sincronizar con una señal más fuerte.',
  measurement_sync_started: 'Sincronización empezado',
  server_communication: 'Comunicando con el servidor, comprobando condiciones …',
  new_login_alert: 'Debido a un nuevo inicio de sesión que no coincide con el anterior, que es\n' +
    '{{name}}\n' +
    ', la base de datos se reinicializará. Los datos anteriores estarán disponibles en la copia de seguridad.',
  default_alert_title: 'Alerta',
  external_permission_denied: 'Permiso de ubicación externa denegado',
  external_permission_denied_backup_creation: 'Permiso de ubicación externa denegado: las copias de seguridad no se guardarán en el directorio externo',
  possible_crash_backup_offer: 'Es posible que debido a una falla inesperada, tus datos se hayan perdido. ¿Quieres restaurarlos? Si no está seguro de que faltan tus datos, puedes restaurarlos manualmente en el menú de configuraciónes.',
  syncing_with_num_of_left: 'Sincronizando: quedan {{num}}',
  airplane_gps_error: 'No se puede alcanzar la señal GPS porque estás en modo avión. ¿Quieres apagarlo?',
  no_measurements: 'No hay datos registrados',
  close: 'Cerrar',
  pin_status_save: 'Guardando el estado del pin',
  pin_save_option: 'Guardar siempre',
  pin_dont_save_option: 'Reinicializar antes de ingresar al formulario',
  pin_save_but_delete_after_sync: 'Guardar estados, pero reinicializar después de la sincronización',
  shortcut_to_home_screen_title: 'Crea un acceso directo en la pantalla de inicio.',
  shortcut_to_home_screen_message: 'Si creas un acceso directo a este formulario, podrás abrirlo rápidamente y estará disponible sin conexión.',
  create_shortcut: 'crear',
  no_shortcut_observations: 'No hay atajos disponibles',
  create_shortcut_suggestion: 'Puede crear un acceso directo en la pantalla del selector de formularios.',
  delete_shortcut_title: 'Eliminar acceso directo',
  delete_shortcut_message: '¿Estás seguro de eliminar este acceso directo? El formulario estará disponible en la pantalla de selección de formularios.',
  error_title_export: 'Exportación fallida',
  error_text_export: 'No se ha creado el archivo de exportación :(',
  error_title_file_writing_permission: 'Escritura de archivo no concedida :(',
  error_text_file_writing_permission: 'Se necesita permiso para escribir el archivo de exportación.',
  ask_me_later: 'Pregúntame luego',
  ext_writing_permission: 'Permiso de escritura externo',
  obm: 'OBM',
  export_finished: 'Exportación finalizada',
  start_export: 'Iniciar exportación',
  export_path: 'Ruta de exportación',
  message_title: 'Tienes mensajes no leidas',
  personal_messages: 'personal',
  comment_notifications: 'sección de comentarios',
  system_messages: 'sistema',
  only_email: 'solo correo electrónico',
  jump_to_latest_observation: 'Ir al formulario más reciente: {{observationName}}',
  tracklog: 'Registro de ruta',
  tracklog_update_time: 'Hora de actualización del tracklog (s)',
  tracklog_time_description: 'Al establecer el tiempo de actualización en 0, se detiene la grabación del track log',
  tracklog_update_distance: 'Filtro de distancia de tracklog (m)',
  tracklog_distance_description: 'Si es distinto de 0, el tracklog se actualizará solo si el dispositivo se movió al menos la cantidad establecida aquí.',
  not_integer: 'El número no es entero',
  copying_session_element: 'Estás copiando un elemento de sesión',
  editing_session_element: 'Estás editando un elemento de sesión',
  wrong_file_uri: 'Un archivo o imagen que has adjuntado está eliminada o su ubicación puede haber cambiado. Intenta quitar y volver a adjuntar antes de cargar.',
  missing_file: 'Falta un archivo :(',
  missing: 'falta :(',
  error_file_copy: 'Algunos de los archivos no están copiados porque no existen!',
  session_only_blocker_message: 'Este es un formulario que solo se puede usar en el modo de creación de sesión. Para iniciar la creación de la sesión, usa el botón de abajo.',
  form_creation_in_progress: 'Registro de datos…',
  list_time_remaining_alert: 'Te quedan {{timeRemaining}} minutos para finalizar la creación de la sesión del formulario: {{observationName}}!',
  list_time_expired_alert: 'Tienes que finalizar la creación de la sesión del formulario: {{observationName}}!',
  copy: 'Copiar',
  clipboard_message: 'Copiar al portapapeles exitoso!',
  position_update: 'La hora de actualización de las posiciones GPS en los formularios (s)',
  position_update_title: 'Intervalo de actualización de posición',
  performance_alert_title: 'Alerta de rendimiento',
  frequent_position_update_alert_message: 'Este intervalo de actualización es muy rápido y puede causar problemas de rendimiento, como un mayor uso de memoria y batería.',
  setting_distance_filter_alert_message: 'Establecer una distancia para filtrar las coordenadas sobrescribirá el tiempo de actualización',
  map_settings_modal_title: 'Puedes configurar cómo se muestran tus datos aquí',
  tracklog_visibility_setting_title: 'Visibilidad de tracklogs',
  date_based_map_filter_title: 'Filtrar datos por fecha',
  date_based_map_filter_start: 'Inicio del intervalo',
  date_based_map_filter_end: 'Fin del intervalo',
  date_based_map_filter_shift_button: 'cambiar el intervalo considerando el tiempo actual',
  tracklog_recording_in_progress: 'grabación en curso',
  tracklog_not_recording: 'no grabando',
  no_messages_title: 'No tienes mensajes nuevos',
  error_title_max_file: 'Error de subida',
  error_text_max_file: 'Archivo demasiado grande para subir',
  tracklog_switched_on: 'Grabación de tracklog en progreso',
  tracklog_switched_off: 'Se desactivó la grabación del tracklog',
  recorded_tracklogs: 'Tracklogs grabados',
  tracklogs: 'Tracklogs',
  delete_tracklog_dialog_title: '¿Estás seguro de eliminar este tracklog?',
  delete_all_synced_dialog_title: '¿Estás seguro de eliminar todos los datos sincronizados?',
  create_backup: 'Crear copia de seguridad',
  successful_backup_creation: '¡Creación de copia de seguridad exitosa!',
  possible_crash_external_backup_offer: 'Hemos encontrado una copia de seguridad externa de la aplicación. ¿Quieres cargarlo? Puede restaurar las copias de seguridad manualmente más tarde en el menú Configuraciónes.',
  choose_backup_from_filesystem: 'Elije una copia de seguridad del sistema de archivos …',
  search_backup_manually: 'Buscar copia de seguridad manualmente',
  invalid_backup_file: 'El archivo que eligiste no es válido. Asegúra de que sea una copia de seguridad real creada por la aplicación',
  no_database_assigned_to_tracklog_while_sync: 'Ninguno de los tracklogs abajo tienen un proyecto para cargar',
  no_database_assigned_to_tracklog: 'Ningún proyecto asignado, no se puede subir',
  showing_only_one_tracklog: 'Mostrando solo el tracklog seleccionado con el nombre:',
  export_tracklog_alert_title: 'Exportando un tracklog',
  export_tracklog_alert_message: '¿Estás seguro de exportar este tracklog? El archivo "{{trackLogName}}.gpx" estará disponible en {{fileUri}}',
  successful_tracklog_creation: '¡Exportación del tracklog exitosa!',
  push_to_turn_on: 'presiona aquí para encenderlo',
  push_to_turn_off: 'presiona aquí para apagarlo',
  play_services_not_available: 'El servicio Google Play no está instalado o tiene una versión anterior',
  location_settings_not_appropriate: 'La configuración de ubicación no está satisfecha. Compruebe si ha desactivado el GPS o el modo Avión activado',
  weak_gps: 'La señal del GPS es bastante débil. Intenta apagar y encender el gps, o desactiva la optimización de la batería para esta aplicación.',
  measurements_visibility_setting_title: 'Visibilidad de las observaciones',
  late_response_trial_call: 'El servidor respondió lentamente, no es seguro subir datos. Vuelva a intentarlo más tarde o póngase en contacto con el administrador del servidor.',
  no_data_to_sync: 'No hay datos para subir!',
  wrong_content_uri: "No se pudo abrir el archivo. Intenta elegir este archivo a través de los directorios 'root' de almacenamiento, en lugar de un acceso directo como 'Descargas'.",
  lost_communication_invalid_refresh_token: 'No se puede comunicar con el servidor. Aún puedes usar los formularios descargados, pero considera cerrar la sesión e iniciarla de nuevo para cargar más datos.',
  unexpected_component_error: 'Ocurrió un error inesperado en este componente. Por favor contacta el administrador.',
  tracklog_export: 'Exportación de tracklog',
  syncing_tracklogs_with_num_of_left: 'Sincronizando tracklog: quedan {{num}}',
  distance_filter: 'Filtro de distancia (m)',
  turn_off_gps_alert_message: 'Actualmente, los siguientes componentes utilizan el GPS',
  turn_off_gps_alert_title: 'Desactivar el GPS',
  location_permission_never_ask_again: 'Permiso de localización revocado por el usuario.',
  location_tracking_channel_name: 'Canal de seguimiento de la localización',
  location_tracking_channel_description: 'Rastrea la localización del usuario',
  tracking_location_updates: 'Seguimiento de las actualizaciones de la ubicación',
  error_line_length: 'La línea debe tener al menos 2 puntos',
  tracklog_length: 'La longitud de este tracklog es {{length}}',
  session_tracklog_recorded: 'El tracklog de la sesión se ha registrado con éxito, longitud: {{length}}',
  root_dir_selected: 'No puedes establecer el directorio raíz de tu teléfono por razones de seguridad',
  external_dir_warning: 'Advertencia El directorio seleccionado será leído y escrito por OpenBioMaps. No elija ningún directorio que contenga archivos que no sean OBM!',
  position_input_array_mode_hint: 'Si quieres configurarlo manualmente, elige abajo para editar los datos existentes o hacer uno nuevo.',
  session_tracklog_optional_description: 'Encienda la grabación, por favor. Esto nos ayuda a medir la distancia recorrida, una información muy importante en el proceso de interpretación de los datos.',
  position_input_hint_type: 'Estás grabando un {{tipo}}.',
  position_input_hint_point_press: 'Pulse prolongadamente en el lugar seleccionado!',
  position_input_hint_array_press: 'Pulse prolongadamente para añadir un nuevo punto al {{tipo}}',
  position_input_mode_use_existing: 'editar',
  position_input_mode_make_new: 'reiniciar',
  delete_last_element: 'Eliminar el último elemento',
  choose_edit_mode_first: 'Elija primero el modo de edición!',
  error_polygon_length: 'El polígono debe tener al menos 3 puntos!',
  session_tracklog_empty: 'El tracklog de la sesión grabada no contiene puntos!',
  error_title_filename: 'Error en el nombre del archivo',
  error_text_filename: 'El nombre del archivo contiene un carácter prohibido.',
  background_location_permission_title: 'Recogida de la ubicación en el fondo',
  background_location_permission_message: 'Esta aplicación utiliza datos de localización para permitir la recogida de datos bióticos. Permitir el acceso a los datos de localización es necesario para que la aplicación funcione correctamente, incluso si se ejecuta en segundo plano o no se está utilizando en ese momento.\n' +
    'En la página de configuración, seleccione "Permitir todo el tiempo" para garantizar que la función GPS funcione sin problemas.',
  session_tracklog_alert_title: 'Recogida de la ubicación en el fondo',
  session_tracklog_recording_started: 'Recogida de la ubicación en el fondo!',
  session_tracklog_record: 'Empezar a grabar',
  session_tracklog_dont_record: 'No grabar',
  global_tracklog_interval_null_error: 'El tiempo de actualización del tracklog se establece en 0. Esto no permite la grabación global del tracklog.',
  sync_tracklogs_manually_hint: 'Si quieres sincronizar los tracklogs, puedes hacerlo en la lista de tracklogs.',
  camera_permission_denied: 'Permiso de cámara denegado.',
  external_folder: 'Carpeta {{tipo}}',
  select_external_folder_description: 'Hay que marcar la carpeta para crear y guardar el archivo {{type}}.',
  select: 'Seleccione',
  storage_settings: 'Almacenamiento',
  delete_unused_files: 'Elimina los archivos no utilizados adjuntados anteriormente.',
  files_deleted_with_size: '{{tamaño}} se ha liberado espacio en disco',
  wkt_type: 'Tipo de datos: {{wktType}}',
  wkt_array_length: '{{wktType}} longitud: {{length}}',
  current_tracklog: 'Tracklog actual',
  current_tracklog_length: 'La longitud del tracklog global actual: {{longitud}}',
  current_tracklog_databases: 'El tracklog se cargará en las siguientes bases de datos:',
  current_tracklog_no_database: 'No hay ninguna base de datos asignada al tracklog. Para ello, abra un formulario relacionado con esa base de datos.',
  current_tracklog_servers: 'El tracklog se subirá a los siguientes servidores:',
  current_tracklog_no_server: 'No hay ningún servidor asignado al tracklog. Para ello, abra un formulario relacionado con ese servidor.',
  view_on_map: 'ver en el mapa',
  point: 'Punto',
  line: 'Línea',
  polygon: 'Polígono',
  tracklog_global_in_progress: 'Grabación del tracklog en curso',
  tracklog_session_in_progress: 'Grabación del tracklog de la sesión en curso',
  pernament_sample_plots_visibility: 'visibilidad de las parcelas permanentes de muestreo',
  periodical_notification_message: 'Registre una observación!',
  iosBackupFolderPlace: 'Files > On my iPhone > OpenBioMaps >',
  choose_backup_from_filesystem_positive: 'Búsqueda de archivos',
  choose_backup_from_filesystem_message: 'Copia de seguridad del sistema de archivos',
  choose_backup_from_filesystem_title: 'Elija Copia de seguridad del sistema de archivos',
  gps: 'GPS',
  auto_backup_description: 'Escribe los datos recogidos en la aplicación en el directorio de copia de seguridad especificado durante la instalación, cada vez que se registra una nueva observación.',
  create_backup_description: 'Realiza copias de seguridad ocasionales de los datos actuales en el directorio de copia de seguridad especificado durante la instalación.',
  delete_unused_files_description: 'Limpia los archivos adjuntos atascados (fotos) que no estén relacionados con los datos.',
  distance_filter_description: 'No actualizará la posición GPS si te mueves menos de la distancia especificada.',
  notification_sound_form_save: 'Señal sonora al grabar correctamente los datos'
}