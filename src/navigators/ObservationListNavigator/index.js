/* eslint-disable react/prop-types */
import React from 'react';
import { Text } from 'react-native';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { LastObservationsScreen, ObservationScreen } from '../../screens';
import I18n from '../../i18n';
import { Colors } from '../../colors';

export default createMaterialTopTabNavigator(
  {
    LastObservationsScreen: {
      screen: LastObservationsScreen,
      navigationOptions: {
        tabBarLabel: ({ tintColor }) => (
          <Text style={{ color: tintColor }}>{I18n.t('offline_available_tab').toUpperCase()}</Text>
        ),
      },
    },
    ObservationListScreen: {
      screen: ObservationScreen,
      navigationOptions: {
        tabBarLabel: ({ tintColor }) => (
          <Text style={{ color: tintColor }}>{I18n.t('all_tab').toUpperCase()}</Text>
        ),
      },
    },
  },
  {
    initialRouteName: 'ObservationListScreen',
    tabBarOptions: {
      indicatorStyle: {
        backgroundColor: 'black',
      },
      activeTintColor: 'black',
      inactiveTintColor: 'rgba(0, 0, 0, 0.61)',
      style: {
        backgroundColor: Colors.mantisGreen,
      },
    },
  },
);
