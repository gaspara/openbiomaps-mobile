/* eslint-disable react/prop-types */
import React from 'react';
import { Text } from 'react-native';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { ObservationScreen } from '../../screens';
import Toplist from '../../components/Toplist';
import I18n from '../../i18n';

export default createMaterialTopTabNavigator(
  {
    ObservationSelectScreen: {
      screen: ObservationScreen,
      navigationOptions: {
        tabBarLabel: ({ tintColor }) => (
          <Text style={{ color: tintColor }}>{I18n.t('game').toUpperCase()}</Text>
        ),
      },
    },
    Toplist: {
      screen: Toplist,
      navigationOptions: {
        tabBarLabel: ({ tintColor }) => (
          <Text style={{ color: tintColor }}>{I18n.t('toplist').toUpperCase()}</Text>
        ),
      },
    },
  },
  {
    initialRouteName: 'ObservationSelectScreen',
    tabBarOptions: {
      indicatorStyle: {
        backgroundColor: 'black',
      },
      activeTintColor: 'black',
      inactiveTintColor: 'rgba(0, 0, 0, 0.61)',
      style: {
        backgroundColor: '#7EC667',
      },
    },
  },
);
