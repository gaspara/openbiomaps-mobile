import I18n from '../i18n';
import { Colors } from '../colors';

const NOTIFICATION_TYPES = {
  SESSION: 'SESSION',
  AUTH_ERROR: 'AUTH_ERROR',
  UNSYNCED_MEASUREMENTS: 'UNSYNCED_MEASUREMENTS',
  OLD_MEASUREMENTS: 'OLD_MEASUREMENTS',
  FORM_UNAVAILABLE: 'FORM_UNAVAILABLE',
  CREATE_FORM_SUCCESS: 'CREATE_FORM_SUCCESS',
  CREATE_FORM_PROGRESS: 'CREATE_FORM_PROGRESS',
  COMMON_FIELDS: 'COMMON_FIELDS',
  CREATE_LIST_SUCCESS: 'CREATE_LIST_SUCCESS',
  NULL_RECORD: 'NULL_RECORD',
  NO_MEMORY: 'NO_MEMORY',
  CRITICAL_MEMORY_USAGE: 'CRITICAL_MEMORY_USAGE',
  NETWORK_ERROR: 'NETWORK_ERROR',
  EDIT_SUCCEEDED: 'EDIT_SUCCEEDED',
  COMMUNICATING_WITH_THE_SERVER: 'COMMUNICATING_WITH_THE_SERVER',
  SYNCING_IN_PROGRESS: 'SYNCING_IN_PROGRESS:',
  SYNCING_TRACKLOGS_IN_PROGRESS: 'SYNCING_TRACKLOGS_IN_PROGRESS',
  COPYING_SESSION_ELEMENT: 'COPYING_SESSION_ELEMENT',
  EDITING_SESSION_ELEMENT: 'EDITING_SESSION_ELEMENT',
  NO_SERVER_ASSIGNED_TO_TRACKLOG_ERROR: 'NO_SERVER_ASSIGNED_TO_TRACKLOG_ERROR',
  LATE_RESPONSE_TRIAL_CALL: 'LATE_RESPONSE_TRIAL_CALL',
  UNKNOWN_ERROR: 'UNKNOWN_ERROR',
  LOST_COMMUNICATION_WITH_THE_SERVER: 'LOST_COMMUNICATION_WITH_THE_SERVER',
};

class NotificationModel {
  constructor(id, stringRef, color, component = null, animation = null, dismissTime = null, enableSound = false, sound = false,) {
    this.id = id;
    this.message = stringRef !== '' ? I18n.t(stringRef) : '';
    this.color = color;
    this.component = component;
    this.animation = animation;
    this.dismissTime = dismissTime;
    this.sound = sound;
    this.enableSound = enableSound;
  }

  static build = (type, component = null, dismissTime = null, enableSound = false) => {
    switch (type) {
      case NOTIFICATION_TYPES.SESSION:
        return new NotificationModel(
          0,
          'list_creation_in_progress',
          Colors.blue,
          component,
        );
      case NOTIFICATION_TYPES.AUTH_ERROR:
        return new NotificationModel(
          1,
          'invalid_refresh_token',
          Colors.flamingo,
          component,
        );
      case NOTIFICATION_TYPES.UNSYNCED_MEASUREMENTS:
        return new NotificationModel(
          2,
          'lot_of_unsynced_measurements',
          Colors.orange,
        );
      case NOTIFICATION_TYPES.OLD_MEASUREMENTS:
        return new NotificationModel(
          3,
          'has_measurement_older_than_one_week',
          Colors.yellow,
        );
      case NOTIFICATION_TYPES.FORM_UNAVAILABLE:
        return new NotificationModel(
          4,
          'form_is_not_available',
          Colors.flamingo,
        );
      case NOTIFICATION_TYPES.CREATE_FORM_SUCCESS:
        return new NotificationModel(
          5,
          'successful_form_creation',
          Colors.asparagusGreen,
          null,
          null,
          dismissTime,
          enableSound,
          true
        );
      case NOTIFICATION_TYPES.COMMON_FIELDS:
        return new NotificationModel(
          6,
          'fiel_common_fields',
          Colors.blue,
        );
      case NOTIFICATION_TYPES.CREATE_LIST_SUCCESS:
        return new NotificationModel(
          7,
          'successful_list_creation',
          Colors.asparagusGreen,
          null,
          null,
          dismissTime,
        );
      case NOTIFICATION_TYPES.NULL_RECORD:
        return new NotificationModel(
          8,
          'create_null_record',
          Colors.blue,
        );
      case NOTIFICATION_TYPES.NO_MEMORY:
        return new NotificationModel(
          9,
          'not_enough_memory',
          Colors.red,
        );
      case NOTIFICATION_TYPES.CRITICAL_MEMORY_USAGE:
        return new NotificationModel(
          10,
          'critical_memory_usage',
          Colors.blue,
        );
      case NOTIFICATION_TYPES.NETWORK_ERROR:
        return new NotificationModel(
          11,
          'no_or_weak_network',
          Colors.orange,
        );
      case NOTIFICATION_TYPES.EDIT_SUCCEEDED:
        return new NotificationModel(
          12,
          'measurement_updated_message',
          Colors.blue,
        );
      case NOTIFICATION_TYPES.COMMUNICATING_WITH_THE_SERVER:
        return new NotificationModel(
          13,
          'server_communication',
          Colors.blue,
          component,
        );
      case NOTIFICATION_TYPES.SYNCING_IN_PROGRESS:
        return new NotificationModel(
          14,
          '',
          Colors.blue,
          component,
        );
      case NOTIFICATION_TYPES.COPYING_SESSION_ELEMENT:
        return new NotificationModel(
          15,
          'copying_session_element',
          Colors.blue,
        );
      case NOTIFICATION_TYPES.EDITING_SESSION_ELEMENT:
        return new NotificationModel(
          16,
          'editing_session_element',
          Colors.blue,
        );
      case NOTIFICATION_TYPES.CREATE_FORM_PROGRESS:
        return new NotificationModel(
          17,
          'form_creation_in_progress',
          Colors.asparagusGreen,
        );
      case NOTIFICATION_TYPES.NO_SERVER_ASSIGNED_TO_TRACKLOG_ERROR:
        return new NotificationModel(
          18,
          'no_server_assigned_to_tracklog_while_sync',
          Colors.orange,
          null,
          null,
          dismissTime,
        );
      case NOTIFICATION_TYPES.LATE_RESPONSE_TRIAL_CALL:
        return new NotificationModel(
          19,
          'late_response_trial_call',
          Colors.orange,
        );
      case NOTIFICATION_TYPES.UNKNOWN_ERROR:
        return new NotificationModel(
          20,
          '',
          Colors.orange,
          component,
        );
      case NOTIFICATION_TYPES.LOST_COMMUNICATION_WITH_THE_SERVER:
        return new NotificationModel(
          21,
          'lost_communication_invalid_refresh_token',
          Colors.orange,
        );
      case NOTIFICATION_TYPES.SYNCING_TRACKLOGS_IN_PROGRESS:
        return new NotificationModel(
          22,
          '',
          Colors.blue,
          component,
        );
      default:
        return null;
    }
  };
}

export { NOTIFICATION_TYPES, NotificationModel };
