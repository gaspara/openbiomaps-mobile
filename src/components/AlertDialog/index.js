import { Alert } from 'react-native';
import I18n from '../../i18n';

/**
 * Unified alert dialog box, uses strings from i18n
 * @param {String}   title                   - REQ - The title of the alert dialog
 * @param {String}   message                 - REQ - The dialog text
 * @param {callback} onPositiveBtnPress      - OPT - callback for the positive button press event
 * @param {callback} onNegativeBtnPress      - OPT - callback for the negative button press event
 * @param {Bool}     showCancelButton        - OPT - Show the cancel button or not, default: false
 * @param {String}   negativeLabel           - OPT - Label of the negative button, default: Cancel
 * @param {String}   positiveLabel           - OPT - Label of the positive button, default: OK
 */
export const showAlertDialog = ({  
  title,
  message,
  onPositiveBtnPress = () => {},
  onNegativeBtnPress = () => {},
  showCancelButton = false,
  negativeLabel = I18n.t('cancel'),
  positiveLabel = I18n.t('ok'),
  cancelable = true
}) => {
  if (showCancelButton) {
    Alert.alert(
      title,
      message,
      [
        { text: negativeLabel, onPress: () => onNegativeBtnPress() },
        { text: positiveLabel, onPress: () => onPositiveBtnPress() },
      ],
      { cancelable: cancelable },
    );
  } else {
    Alert.alert(
      title,
      message,
      [{ text: positiveLabel, onPress: () => onPositiveBtnPress() }],
      { cancelable: false },
    );
  }
};
