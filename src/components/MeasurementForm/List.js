import React from 'react';
import PropTypes from 'prop-types';
import { List as styles } from './styles';
import { Picker } from '../Picker';
import { Colors } from '../../colors';
import { ListAsButtons } from '../ListAsButtons';

const getItems = (data, listAsButtons) => {
  const items = [];
  data.forEach((object) => {
    const [key, value] = Object.entries(object)[0];
    if (listAsButtons) {
      if (key) {
        items.push({ key, value });
      }
    } else {
      items.push({ key, value });
    }
  });
  return items;
};

const getCurrentValue = (data, currValue) =>
  Object.values(
    data.find((object) => {
      const [key] = Object.keys(object);
      return key === currValue;
    }) || { '': '' },
  );

const List = ({
  shortName,
  data,
  listAsButtons,
  value,
  onValueChange,
  error,
  obligatory,
  unfoldingList,
}) => {
  const [currentValue] = getCurrentValue(data, value);

  const items = getItems(data, listAsButtons);

  if (listAsButtons) {
    return (
      <ListAsButtons
        data={items}
        value={currentValue}
        onItemSelect={onValueChange}
      />
    );
  }

  return (
    <Picker
      unfoldingList={unfoldingList}
      data={items}
      value={unfoldingList ? value : currentValue}
      title={shortName}
      onItemSelect={onValueChange}
      customPickerStyle={{
        baseContainer: [
          styles.container,
          obligatory && styles.obligatory,
          error && styles.error,
        ],
      }}
      touchableIconProps={{
        color: Colors.darkGray,
        size: 30,
        vectorIconStyle: {
          margin: -5,
        },
      }}
    />
  );
};

List.propTypes = {
  shortName: PropTypes.string,
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  listAsButtons: PropTypes.bool.isRequired,
  value: PropTypes.any,
  onValueChange: PropTypes.func.isRequired,
  error: PropTypes.string,
  obligatory: PropTypes.bool,
  unfoldingList: PropTypes.bool,
};

List.defaultProps = {
  value: null,
  error: null,
  obligatory: false,
};

export default List;
