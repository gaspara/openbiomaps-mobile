import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Boolean as styles } from './styles';
import { Colors } from '../../colors';
import { TouchableIcon } from '../TouchableIcon';


const getIconName = (value) => {
  if (value === true) {
    return 'check-box';
  }
  if (value === false) {
    return 'indeterminate-check-box';
  }
  return 'check-box-outline-blank';
};

const getIconColor = (error, editable, obligatory) => {
  if (error) {
    return Colors.flamingo; 
  } else if (!editable) {
    return Colors.silver;
  } else if (obligatory) {
    return Colors.brickRed;
  }
  return Colors.mantisGreen;
};

const Boolean = ({ value, onValueChange, error, editable, obligatory }) => (
  <View style={{flex:1,flexDirection: "row",}}>
  <TouchableOpacity
    onPress={() => onValueChange(!value)}
    style={styles.container}
    disabled={!editable}>
    <Icon
      size={45}
      color={getIconColor(error, editable, obligatory)}
      name={getIconName(value)}
    />
  </TouchableOpacity>

  <TouchableIcon style={styles.icon} name="delete" size={35} onPress={
    () => onValueChange(null)
  }/>
  </View>
);

Boolean.propTypes = {
  value: PropTypes.bool,
  onValueChange: PropTypes.func.isRequired,
  error: PropTypes.string,
  editable: PropTypes.bool,
  obligatory: PropTypes.bool,
};

Boolean.defaultProps = {
  value: null,
  error: null,
  editable: true,
  obligatory: false,
};

export default Boolean;
