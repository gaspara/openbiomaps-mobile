import React, { Fragment } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';

import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

import { Colors } from '../../colors';
import ErrorBoundary from '../ErrorBoundary';

import {
  // CRings,
  DatePicker,
  GeometryTypeChooser,
  FilePicker,
  List,
  AutoComplete,
  Boolean,
  FormInput,
  AutoCompleteList,
  MultiSelectList,
} from './FormListItems';
import { MeasurementFormListItem as styles } from './styles';

/**
 *
 * @param {object} item
 * @param {function} onChange
 * @param {object} path - Object containing the followings { server, database, observaiton }
 * @param {boolean} isEditing - Whether the measurement is being edited or created
 */
const renderItem = (item, onChange, path, isEditing) => {
  switch (item.type) {
    // case 'crings':
    //   return <CRings data={item.list} values={item.value} onValueChange={onChange} />;
    case 'date':
      return (
        <DatePicker
          edit={isEditing}
          value={item.value}
          onValueChange={onChange}
          dateOnly
          error={item.error}
          obligatory={item.obl === '1' || item.obl === '3'}
        />
      );
    case 'time':
      return (
        <DatePicker
          edit={isEditing}
          value={item.value}
          onValueChange={onChange}
          timeOnly
          error={item.error}
          obligatory={item.obl === '1' || item.obl === '3'}
        />
      );
    case 'datetime':
      return (
        <DatePicker
          edit={isEditing}
          value={item.value}
          onValueChange={onChange}
          error={item.error}
          obligatory={item.obl === '1' || item.obl === '3'}
        />
      );
    case 'point':
    case 'line':
    case 'polygon':
    case 'wkt':
      return (
        <GeometryTypeChooser
          type={item.type}
          column={item.column}
          valueFromParent={
            item.type === 'wkt' ? item.value?.wktValue : item.value
          }
          valueFromParentType={
            item.type === 'wkt' ? item.value?.wktType : item.type
          }
          error={item.error}
          obligatory={item.obl === '1' || item.obl === '3'}
          autoGeometry={item.apiParams.auto_geometry === 'on'}
          isEditing={isEditing}
          // eslint-disable-next-line react/jsx-props-no-spreading
          {...path}
        />
      );
    case 'file_id':
      return (
        <FilePicker
          items={item.value}
          onChange={onChange}
          error={item.error}
          obligatory={item.obl === '1' || item.obl === '3'}
          maximumSize={path.database.projectFileSize}
        />
      );
    case 'text':
      return (
        <FormInput
          value={item.value}
          onChangeText={onChange}
          error={item.error}
          editable={item.apiParams.readonly !== 'on'}
          obligatory={item.obl === '1' || item.obl === '3'}
        />
      );
    case 'numeric':
      return (
        <FormInput
          value={item.value}
          onChangeText={onChange}
          keyboardType="numeric"
          error={item.error}
          editable={item.apiParams.readonly !== 'on'}
          obligatory={item.obl === '1' || item.obl === '3'}
        />
      );
    case 'list': {
      if (item.list_definition?.multiselect) {
        return (
          <MultiSelectList
            data={item.list}
            onValueChange={onChange}
            value={item.value}
            error={item.error}
            obligatory={item.obl === '1' || item.obl === '3'}
            lastPickedData={item.lastPickedData[item.column]
              ?.flat()
              .filter((value, index, self) => self.indexOf(value) === index)}
          />
        );
      }
      return (
        <List
          shortName={item.shortName}
          data={item.list}
          unfoldingList={item.apiParams.unfolding_list === 'on'}
          listAsButtons={item.apiParams.list_elements_as_buttons === 'on'}
          onValueChange={onChange}
          value={item.value}
          error={item.error}
          obligatory={item.obl === '1' || item.obl === '3'}
        />
      );
    }
    case 'autocomplete':
      return (
        <AutoComplete
          shortName={item.shortName}
          value={item.value}
          unfoldingList={item.apiParams.unfolding_list === 'on'}
          data={
            Array.isArray(item.genlist)
              ? item.genlist
                  .filter((listItem) => listItem)
                  .map((listItem) =>
                    typeof listItem === 'string'
                      ? listItem
                      : Object.values(listItem)[0],
                  )
              : []
          }
          lastPickedData={item.lastPickedData[item.column]
            ?.flat()
            .filter(
              (value, index, self) =>
                typeof value === 'string' && self.indexOf(value) === index,
            )}
          onValueChange={onChange}
          error={item.error}
          obligatory={item.obl === '1' || item.obl === '3'}
        />
      );
    case 'autocompletelist':
      return (
        <AutoCompleteList
          shortName={item.shortName}
          value={
            item.value &&
            (Array.isArray(item.value) ? item.value : [item.value])
          }
          data={
            Array.isArray(item.genlist)
              ? item.genlist
                  .filter((listItem) => listItem)
                  .map((listItem) =>
                    typeof listItem === 'string'
                      ? listItem
                      : Object.values(listItem)[0],
                  )
              : []
          }
          lastPickedData={item.lastPickedData[item.column]
            ?.flat()
            .filter((value, index, self) => self.indexOf(value) === index)}
          onValueChange={onChange}
          error={item.error}
          obligatory={item.obl === '1' || item.obl === '3'}
        />
      );
    case 'boolean':
      return (
        <Boolean
          value={item.value}
          onValueChange={onChange}
          error={item.error}
          editable={item.apiParams.readonly !== 'on'}
          obligatory={item.obl === '1' || item.obl === '3'}
        />
      );
    default:
      return (
        <FormInput
          value={item.value}
          onChangeText={onChange}
          error={item.error}
          editable={item.apiParams.readonly !== 'on'}
          obligatory={item.obl === '1' || item.obl === '3'}
        />
      );
  }
};

const renderRequired = ({ obl, error }) =>
  (obl === '1' || obl === '3') && (
    <View style={styles.requiredContainer}>
      <Text style={[styles.requiredText, error && styles.error]}> *</Text>
    </View>
  );

/**
 * red, if:
 * ha a default sticky és a mentett nem egyezik, és a jelenlegi sticky sem egyezik a default-tal
 * ha egyezik a sticky, de a default value a formnak nem az apiről lehivott, és a def sticky true
 *
 */
const styleSelector = ({
  value = null,
  defaultValue,
  isSticky,
  isSavedSticky,
  apiParams,
}) => [
  isSticky ? styles.rotated : null,
  ((isSavedSticky !== null &&
    isSavedSticky !== undefined &&
    isSavedSticky !== (apiParams.sticky === 'on') &&
    isSticky !== (apiParams.sticky === 'on')) ||
    (isSticky &&
      apiParams.sticky === 'on' &&
      isSavedSticky &&
      defaultValue !== value)) && { color: Colors.flamingo },
];

/**
 * auto_geometry should be there because it has no default value, and we need the persons location in the
 * background
 * @param {*} param0
 */
const ListItem = ({
  item,
  onChange,
  onPinPress,
  server,
  database,
  observation,
  showObservationListCommonFields,
  isEditing,
}) => (
  <>
    <View
      style={
        item.apiParams.auto_geometry === 'on' &&
        item.apiParams.hidden === 'on' && { display: 'none' }
      }
    >
      <View style={styles.row}>
        <Text style={[item.error ? styles.error : styles.text]}>
          {item.shortName}
        </Text>
        {renderRequired(item)}
        <TouchableOpacity
          onPress={onPinPress}
          style={
            showObservationListCommonFields
              ? styles.hidden
              : styles.stickyButton
          }
        >
          <Icon name="pin" style={styleSelector(item)} size={18} />
        </TouchableOpacity>
      </View>
      <ErrorBoundary>
        {renderItem(
          item,
          onChange,
          { server, database, observation },
          isEditing,
        )}
      </ErrorBoundary>
      {item.error && (
        <Text style={[styles.error, styles.errorContainer]}>{item.error}</Text>
      )}
    </View>
  </>
);

ListItem.propTypes = {
  item: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  onPinPress: PropTypes.func.isRequired,
  server: PropTypes.object.isRequired,
  database: PropTypes.object.isRequired,
  observation: PropTypes.object.isRequired,
  showObservationListCommonFields: PropTypes.bool,
  isEditing: PropTypes.bool,
};

ListItem.defaultProps = {
  showObservationListCommonFields: false,
  isEditing: false,
};

export default ListItem;
