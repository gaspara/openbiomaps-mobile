import React from 'react';
import { FlatList } from 'react-native';

import PropTypes from 'prop-types';

import ListItem from './MeasurementFormListItem';
import { MeasurementForm as styles } from './styles';

const Form = ({
  form,
  values,
  onValueChange,
  errors,
  stickinesses,
  savedStickinesses,
  onPinPress,
  getFormRef,
  server,
  database,
  observation,
  showObservationListCommonFields,
  isEditing,
  lastPickedValues,
}) => {
  const getFilteredList = (item, filter) => {
    const {
      list, 
      filtering_list: {
        list_labels,
        list_values,
        filter_values,
      },
    } = item;

    if (!filter_values.includes(filter)) {
      return list;
    }
    
    // The new list can be assembled from the list_values and list_labels array
    // based on the indexes of the filter_values. The position(s) of the filter 
    // in the filter_values identify the position of items in the list_labels and
    // list_values. List_values serve as key while list_labels act as value in the
    // returned object.
    const filterIndexes = filter_values.reduce((acc, curr, index) => {
      if (curr === filter) {
        acc.push(index);
      }
      return acc;
    }, []);

    const filteredList = { '': null };
    filterIndexes.forEach((index) => {
      filteredList[list_values[index]] = list_labels[index];
    });

    const filteredArray = [];
    Object.entries(filteredList).forEach(([key, value]) => {
      filteredArray.push({ [key]: value });
    });
    return filteredArray;
  };

  const data = form.map((item) => {
    if (
      (item.type === 'list' || item.type === 'autocomplete') 
      && item.filtering_list
      && item.filtering_list.filter_selector
    ) {
      // filterColumn is the name of the column whose value can be used
      // to filter the current list.
      const { filtering_list: { filter_selector } } = item;
      const filterColumn = Array.isArray(filter_selector) ? filter_selector[0] : filter_selector;
      const filter = values[filterColumn];
      const filteredList = getFilteredList(item, filter);
      return {
        ...item,
        list: filteredList ? Object.values(filteredList) : [],
        genlist: filteredList ? Object.values(filteredList) : [],
        value: values[item.column],
        error: errors[item.column],
        isSticky: stickinesses[item.column],
        isSavedSticky: savedStickinesses[item.column],
        lastPickedData: lastPickedValues,
      };
    }

    return {
      ...item,
      value: values[item.column],
      error: errors[item.column],
      isSticky: stickinesses[item.column],
      isSavedSticky: savedStickinesses[item.column],
      lastPickedData: lastPickedValues,
    };
  });
  
  return (
    <FlatList
      ref={(ref) => getFormRef && getFormRef(ref)}
      keyboardShouldPersistTaps="handled"
      contentContainerStyle={styles.container}
      data={data}
      keyExtractor={(item) => item.column}
      removeClippedSubviews={false}
      renderItem={({ item }) => (
        <ListItem
          item={item}
          onChange={(value) => onValueChange(item.column, value, item)}
          onPinPress={() => onPinPress(item.column)}
          server={server}
          database={database}
          observation={observation}
          showObservationListCommonFields={showObservationListCommonFields}
          isEditing={isEditing}
        />
      )} 
    />
  );
};

Form.propTypes = {
  form: PropTypes.array.isRequired,
  values: PropTypes.shape({}).isRequired,
  onValueChange: PropTypes.func.isRequired,
  errors: PropTypes.oneOfType([
    PropTypes.shape({}),
    PropTypes.string, 
  ]).isRequired,
  stickinesses: PropTypes.shape({}).isRequired,
  savedStickinesses: PropTypes.shape({}).isRequired,
  onPinPress: PropTypes.func.isRequired,
  getFormRef: PropTypes.func,
  server: PropTypes.object.isRequired,
  database: PropTypes.object.isRequired,
  observation: PropTypes.object.isRequired,
  showObservationListCommonFields: PropTypes.bool,
  isEditing: PropTypes.bool,
  lastPickedValues: PropTypes.object,
};

Form.defaultProps = {
  getFormRef: null,
  showObservationListCommonFields: false,
  isEditing: false,
  lastPickedValues: {},
};

export default Form;
