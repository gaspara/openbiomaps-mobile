import React, { Component } from 'react';
import { Text, View, CheckBox } from 'react-native';
import PropTypes from 'prop-types';
import { CRings as styles } from './styles';

export default class CRings extends Component {
  handleValueChange = (key, value) => {
    let values = [...this.props.values];
    if (value) {
      values.push(key);
    } else {
      values = values.filter(value => value !== key);
    }
    this.props.onValueChange(values);
  }

  render() {
    return (
      <View style={styles.container}>
        {this.props.data.map(item => (
          <View key={item} style={styles.row}>
            <Text style={styles.text}>{item}</Text>
            <CheckBox
              value={this.props.values.includes(item)}
              style={styles.checkBox}
              onValueChange={value => this.handleValueChange(item, value)}
            />
          </View>
        ))}
      </View>
    );
  }
}

CRings.propTypes = {
  data: PropTypes.array.isRequired,
  values: PropTypes.array,
  onValueChange: PropTypes.func,
};

CRings.defaultProps = {
  values: [],
  onValueChange: () => {},
};
