import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  topContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  textVContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
  },
  buttonContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  icon: {
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 3,
    paddingHorizontal: 15,
    paddingVertical: 2,
    marginStart: 5,
    marginTop: 5,
  },
  title: {
    fontSize: 18,
    color: 'black',
    fontWeight: 'bold',
    marginBottom: -10,
  },
  details: {
    fontSize: 15,
    color: 'black',
  },
  imageContainer: {
    height: 200,
    width: '100%',
    marginVertical: 5,
  },
  image: {
    borderRadius: 10,
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
  },
});
