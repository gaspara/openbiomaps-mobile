import React, { Fragment } from 'react';
import { FlatList, View, Text } from 'react-native';
import PropTypes from 'prop-types';
import ListItem from './ImageListItem';
import I18n from '../../i18n';
import { Colors } from '../../colors';

const ImageList = ({ name, data }) => (
  <Fragment>
    <View style={{ flexDirection: 'row' }}>
      <Text>{name}: </Text>
      {data === I18n.t('no_field_data') && <Text style={{ color: Colors.red }}>{data}</Text>}
    </View>
    {data !== I18n.t('no_field_data') && (
      <FlatList
        listKey={String(name)}
        data={data}
        keyExtractor={item => String(item.id)}
        renderItem={({ item }) => <ListItem uri={item.uri} />}
      />
    )}
  </Fragment>
);

ImageList.propTypes = {
  name: PropTypes.string.isRequired,
  data: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number.isRequired,
      uri: PropTypes.string.isRequired,
    })),
  ]).isRequired,
};

export default ImageList;
