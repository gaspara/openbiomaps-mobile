import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';
import I18n from '../../i18n';
import { Colors } from '../../colors';

const ListItem = ({ value }) => (
  <Text style={value === I18n.t('no_field_data') && { color: Colors.red }}>
    {value === I18n.t('no_field_data') ? '*' : value}
  </Text>
);

ListItem.propTypes = {
  value: PropTypes.string.isRequired,
};

export default ListItem;
