import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  Modal,
  ScrollView,
  FlatList,
} from 'react-native';
import PropTypes from 'prop-types';
import { styles } from './styles';
import { UnfoldingList as unfoldingListStyle } from '../MeasurementForm/styles';
import { TouchableIcon } from '../TouchableIcon';
import { Colors } from '../../colors';
import I18n from '../../i18n';
import { showAlertDialog } from '../AlertDialog';

class Picker extends Component {
  constructor(props) {
    super(props);

    this.state = { showModal: false };
  }

  addItemToList = (value, defaultList) => {
    const itemKey = value.key.toString();

    const list = [...defaultList];

    const selectedElement = list.find(
      (findValue) => findValue.item === itemKey,
    );
    const selecetedElementIndex = list.findIndex(
      (findValue) => findValue.item === itemKey,
    );

    if (selectedElement) {
      const selectedElementCount = selectedElement.count;
      list[selecetedElementIndex] = {
        item: itemKey,
        count: selectedElementCount + 1,
      };
    } else {
      list.push({ item: itemKey, count: 1 });
    }

    return list;
  };

  onItemSelect = (item) => () => {
    const { unfoldingList } = this.props;
    if (unfoldingList) {
      const savedSelectedList = this.addItemToList(
        item,
        this.props.value === null ? [] : this.props.value,
      );
      this.props.onItemSelect(savedSelectedList);
    } else {
      this.props.onItemSelect(item.key.toString());
    }
    this.setState({ showModal: false });
  };

  renderItem = (item, index, arr) => {
    const { customPickerStyle } = this.props;
    const itemContainerStyle = [
      styles.itemContainer,
      customPickerStyle.itemContainer,
      index === arr.length - 1 && { marginBottom: 10 },
      index === 0 && { marginTop: 10 },
    ];

    return (
      <TouchableOpacity
        style={itemContainerStyle}
        onPress={this.onItemSelect(item)}
        key={item.key}
      >
        <Text style={styles.text}>{item.value || '-'}</Text>
      </TouchableOpacity>
    );
  };

  getValue() {
    const { value, unfoldingList } = this.props;
    if (unfoldingList) {
      return I18n.t('unfolding_list_please_choose');
    } else {
      return value || '-';
    }
  }

  handleAddCount(item) {
    const savedSelectedList = [...this.props.value];

    const selectedElement = savedSelectedList.find(
      (findValue) => findValue.item === item.item,
    );
    const selecetedElementIndex = savedSelectedList.findIndex(
      (findValue) => findValue.item === item.item,
    );

    if (selectedElement) {
      const selectedElementCount = selectedElement.count;
      savedSelectedList[selecetedElementIndex] = {
        item: item.item,
        count: selectedElementCount + 1,
      };
    } else {
      list.push({ item: item.item, count: 1 });
    }

    this.props.onItemSelect(savedSelectedList);
  }

  handleRemoveCount(item) {
    const savedSelectedList = [...this.props.value];

    const selectedElementSaved = savedSelectedList.find(
      (findValue) => findValue.item === item.item,
    );

    if (selectedElementSaved) {
      const selecetedElementIndexSaved = savedSelectedList.findIndex(
        (findValue) => findValue.item === item.item,
      );

      const selectedElementCountSaved = selectedElementSaved.count;

      if (selectedElementCountSaved == 1) {
        showAlertDialog({
          title: I18n.t('unfolding_list_delete_title', {
            itemName: this.findByKey(this.props.data, item).value,
          }),
          message: I18n.t('unfolding_list_delete_message'),
          showCancelButton: true,
          positiveLabel: I18n.t('bool_true'),
          negativeLabel: I18n.t('bool_false'),
          onPositiveBtnPress: () => {
            savedSelectedList.splice(selecetedElementIndexSaved, 1);
            this.props.onItemSelect(savedSelectedList);
          },
        });
      } else {
        savedSelectedList[selecetedElementIndexSaved] = {
          item: item.item,
          count: selectedElementCountSaved - 1,
        };
        this.props.onItemSelect(savedSelectedList);
      }
    }
  }

  findByKey = (arr, currValue) => {
    return arr.find((findValue) => findValue.key === currValue.item);
  };

  render() {
    const {
      title,
      data,
      customPickerStyle,
      touchableIconProps,
      pickerTouchDisable = false,
    } = this.props;
    const { showModal } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <TouchableOpacity
          disabled={pickerTouchDisable}
          style={[styles.baseContainer, customPickerStyle.baseContainer]}
          onPress={() => this.setState({ showModal: !showModal })}
        >
          <View
            style={[styles.labelContainer, customPickerStyle.labelContainer]}
          >
            <Text
              style={[styles.text, customPickerStyle.text]}
              {...customPickerStyle.textProps}
            >
              {this.getValue()}
            </Text>
          </View>
          <TouchableIcon
            name="arrow-drop-down"
            disabled
            {...touchableIconProps}
          />
        </TouchableOpacity>

        {this.props.unfoldingList && this.props.value && (
          <View style={unfoldingListStyle.container}>
            <FlatList
              contentInset={{ top: 0, bottom: 20, left: 0, right: 0 }}
              data={this.props.value}
              keyExtractor={(item) => item.item}
              removeClippedSubviews={false}
              renderItem={({ item }) => (
                <View style={unfoldingListStyle.containerWithButton}>
                  <TouchableIcon
                    style={unfoldingListStyle.rowButton}
                    onPress={() => this.handleRemoveCount(item)}
                    name="remove"
                    color={Colors.darkGray}
                    size={30}
                  />
                  <View style={unfoldingListStyle.valueContainer}>
                    <Text>
                      {this.findByKey(data, item).value} ({item.count})
                    </Text>
                  </View>
                  <TouchableIcon
                    style={unfoldingListStyle.rowButton}
                    onPress={() => this.handleAddCount(item)}
                    name="add"
                    color={Colors.darkGray}
                    size={30}
                  />
                </View>
              )}
            />
          </View>
        )}

        <Modal
          transparent
          visible={showModal}
          onRequestClose={() => this.setState({ showModal: !showModal })}
        >
          <TouchableOpacity
            style={[
              styles.backdropContainer,
              customPickerStyle.backdropContainer,
            ]}
            onPress={() => this.setState({ showModal: !showModal })}
          >
            <View style={styles.list}>
              {title && (
                <Text
                  style={{
                    fontSize: 20,
                    color: 'black',
                    fontWeight: 'bold',
                    textAlign: 'center',
                    marginTop: 10,
                  }}
                >
                  {title}
                </Text>
              )}
              <ScrollView style={[, customPickerStyle.list]}>
                {data.map((item, index, arr) =>
                  this.renderItem(item, index, arr),
                )}
              </ScrollView>
            </View>
          </TouchableOpacity>
        </Modal>
      </View>
    );
  }
}

Picker.propTypes = {
  title: PropTypes.string,
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  onItemSelect: PropTypes.func.isRequired,
  pickerTouchDisable: PropTypes.bool,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.array,
  ]),
  customPickerStyle: PropTypes.object,
  touchableIconProps: PropTypes.object,
  unfoldingList: PropTypes.bool,
};

Picker.defaultProps = {
  value: [],
  customPickerStyle: {},
  touchableIconProps: {},
};

export { Picker };
