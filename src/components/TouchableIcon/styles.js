import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  baseContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  label: {
    fontSize: 14,
    color: 'black',
    marginHorizontal: 8,
  },
});
