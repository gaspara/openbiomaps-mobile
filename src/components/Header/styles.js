import { StyleSheet, Dimensions } from 'react-native';
import { Colors } from '../../colors';

export const headerStyle = {
  backgroundColor: Colors.mantisGreen,
};

export const titleStyle = {
  fontWeight: '400',
};

export const headerElementStyle = StyleSheet.create({
  title: {
    fontWeight: '400',
    fontSize: 22,
  },
  titlePadding: {
    paddingRight: 70
  },
  subTitle: {
    fontSize: 12,
    color:Colors.darkGray
  },
  leftButton: {
    marginLeft: 10,
    color: 'black'
  },
  rightButton: {
    marginRight:10,
    color: 'black'
  }
});

export const roundedButton = {
    borderWidth: 1.3,
    borderColor: 'black',
    borderRadius: 5,
    justifyContent: 'center',
    paddingHorizontal: 30,
    paddingVertical: 10,

}

export const headerTitleStyle = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    ...headerStyle,
  },
  titleContainerSync: {
    flex: 0,
    flexDirection: 'column',
    justifyContent: 'center',
    width: Dimensions.get('window').width * 0.4,
    ...headerStyle,
  },
  title: {
    ...titleStyle,
    fontSize: 22,
    // marginBottom: 10,
    // textAlign: 'center',
    color: 'rgba(0, 0, 0, 0.87)',
  },
  titleSync: {
    ...titleStyle,
    fontSize: 22,
    color: 'rgba(0, 0, 0, 0.87)',
    marginBottom: 0,
    paddingEnd: 5,
  },
  description: {
    fontSize: 12,
    color: 'rgba(0, 0, 0, 0.87)',
  },
  centerAligned: {
    alignItems: 'center',
  },
});

export const headerButtonStyle = StyleSheet.create({
  left: {
    marginLeft: 16,
  },
  right: {
    marginRight: 16,
  },
  roundedShape: {
    borderWidth: 1.3,
    borderColor: 'black',
    borderRadius: 5,
    justifyContent: 'center',
    paddingHorizontal: 30,
    paddingVertical: 10,
    // width: Dimensions.get('window').width * 0.4,
  },
});

export const navigatorHeight = {
  top: 'never', bottom: 'never',
};
