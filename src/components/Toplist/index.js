import React, { Component } from 'react';
import { FlatList, View, Text } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions';
import I18n from '../../i18n';
import styles from './styles';
import { getSelectedServer, getSelectedDatabase, getTopListState } from '../../utils';
import { Spinner } from '../Spinner';
import EmptyList from '../EmptyList';

class Toplist extends Component {
  static navigationOptions = {
    tabBarLabel: I18n.t('toplist'),
  };

  componentDidMount = () => {
    this.loadToplist();

    // Force update component when it is in focus to evoke render() for updateing changed props.
    // forceUpdate() calls render() by skipping shouldComponentUpdate()
    this.onNavigationFocus = this.props.navigation.addListener('willFocus', () => {
      this.forceUpdate();
    });
  };

  shouldComponentUpdate = (nextProps) => {
    const { fetching } = this.props;

    // Render component only when the feching state changes to prevent rerender every time
    // when a deeply nested property changes in the hierarchy tree.
    if (fetching !== nextProps.fetching) {
      return true;
    }

    return false;
  };

  componentWillUnmount() {
    this.onNavigationFocus.remove();
  }

  loadToplist = () => {
    const { loadToplist, selectedServer, selectedDatabase } = this.props;
    loadToplist(selectedServer, selectedDatabase);
  }

  render() {
    const { toplist, fetching } = this.props;

    if (!toplist.length && fetching) return <Spinner />;
    if (!toplist.length) return <EmptyList onRefreshPress={this.loadToplist} />;

    return (
      <FlatList
        contentContainerStyle={styles.container}
        ListHeaderComponent={
          <View style={styles.header}>
            <Text style={[styles.place, styles.headerText]} />
            <Text style={[styles.name, styles.headerText]}>{I18n.t('player')}</Text>
            <Text style={[styles.score, styles.headerText]}>{I18n.t('score')}</Text>
          </View>
        }
        keyExtractor={item => item.name}
        data={toplist}
        renderItem={({ item, index }) => (
          <View style={styles.row}>
            <Text style={styles.place}>{index + 1}</Text>
            <Text style={styles.name} ellipsizeMode="tail" numberOfLines={1}>
              {item.name}
            </Text>
            <Text style={styles.score}>{item.score}</Text>
          </View>
        )}
      />
    );
  }
}

Toplist.propTypes = {
  loadToplist: PropTypes.func.isRequired,
  selectedServer: PropTypes.object.isRequired,
  selectedDatabase: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired,
  toplist: PropTypes.array.isRequired,
  fetching: PropTypes.bool,
};

Toplist.defaultProps = {
  fetching: false,
};

const mapStateToProps = ({ servers: { data: serverList } }) => {
  const selectedServer = getSelectedServer(serverList);
  const selectedDatabase = getSelectedDatabase(selectedServer);
  const { toplist, fetching, error } = getTopListState(selectedDatabase);

  return {
    selectedServer,
    selectedDatabase,
    toplist,
    fetching,
    error,
  };
};

const mapDispatchToProps = (dispatch) => {
  const { loadToplist } = actions;

  return bindActionCreators(
    {
      loadToplist,
    },
    dispatch,
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Toplist);
