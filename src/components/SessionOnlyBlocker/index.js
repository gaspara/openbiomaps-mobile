import React, { Component } from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import { TouchableIcon } from '../TouchableIcon';
import { footerStyle } from '../Footer/styles';
import styles from './styles';
import I18n from '../../i18n';
import * as actions from '../../actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showAlertDialog } from '../AlertDialog';


class SessionOnlyBlocker extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { navigation } = this.props;
    navigation.setParams({
      toggleShortcut: this.toggleShortcut,
      deleteShortcut: this.deleteShortcut,
    });
  }

  toggleShortcut = () => {
    showAlertDialog({
      title: I18n.t('shortcut_to_home_screen_title'),
      message: I18n.t('shortcut_to_home_screen_message'),
      showCancelButton: true,
      positiveLabel: I18n.t('create_shortcut'),
      negativeLabel: I18n.t('cancel'),
      onPositiveBtnPress: () => { this.createShortcut(); },
    });
  }

  deleteShortcut = () => {
    showAlertDialog({
      title: I18n.t('delete_shortcut_title'),
      message: I18n.t('delete_shortcut_message'),
      showCancelButton: true,
      positiveLabel: I18n.t('ok'),
      negativeLabel: I18n.t('cancel'),
      onPositiveBtnPress: () => { this.removeShortcut(); },
    });
  }


  createShortcut = async () => {
    const {
      createShortcut,
      server: { id: serverId, name: serverName },
      database: { id: databaseId, name: databaseName, game: databaseGame},
      observation: { id, name, lastSelected },
      navigation
    } = this.props;
    createShortcut({ serverId, serverName }, { databaseId, databaseName, databaseGame }, { id, name, lastSelected });

    navigation.setParams(
      { pinned: true }
    )
  }

  removeShortcut = async () => {
    const {
      removeShortcut,
      server: { id: serverId },
      database: { id: databaseId },
      observation: { id },
      navigation
    } = this.props;
    removeShortcut(serverId, databaseId, id);

    navigation.setParams(
      { pinned: false }
    )
  }

  render() {
    const {
      onPress
    } = this.props;
    return (
      <View style={styles.blockerContainer}>
        <Text style={styles.blockerText}>{I18n.t('session_only_blocker_message')}</Text>
        <TouchableIcon 
          name="playlist-add"
          onPress={onPress}
          style={[styles.button, footerStyle.roundedShape]}
        />
      </View>
    );
  }


};

SessionOnlyBlocker.propTypes = {
  onPress: PropTypes.func.isRequired,
  server: PropTypes.object.isRequired,
  database: PropTypes.object.isRequired,
  observation: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired,
  createShortcut: PropTypes.func.isRequired,
  removeShortcut: PropTypes.func.isRequired
};

const mapDispatchToProps = (dispatch) => {
  const {
    createShortcut,
    removeShortcut
  } = actions;

  return bindActionCreators(
    {
      createShortcut,
      removeShortcut
    },
    dispatch,
  );
};

export default connect(
  null,
  mapDispatchToProps,
)(SessionOnlyBlocker);