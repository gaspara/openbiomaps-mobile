import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

import { useDispatch } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import * as Animatable from 'react-native-animatable';

import Row from '../Row';
import Center from '../Center';
import { showAlertDialog } from '../AlertDialog';
import { Colors } from '../../colors';
import { stopGpsRecording } from '../../actions/gps';
import { useShallowEqualSelector } from '../../utils/useShallowEqualSelector';
import I18n from '../../i18n';

import styles from './styles';

const GpsButton = () => {
  const dispatch = useDispatch();
  const isRecording = useShallowEqualSelector((state) => state.gps.isRecording);
  const observers = useShallowEqualSelector((state) => state.gps.observers);

  if (!isRecording) return null;

  const onPositiveButtonPress = () => {
    dispatch(stopGpsRecording());
  };

  const onPress = () => {
    showAlertDialog({
      title: I18n.t('turn_off_gps_alert_title'),
      message: `${I18n.t('turn_off_gps_alert_message')}: ${observers.concat()}`,
      showCancelButton: true,
      onPositiveBtnPress: onPositiveButtonPress,
    });
  };

  return (
    <TouchableOpacity onPress={onPress} style={styles.baseContainer}>
      <Row>
        <Center flexDirection="row">
          <Text style={styles.text}>Tracking location updates...</Text>
          <Animatable.View
            animation="fadeIn"
            iterationCount="infinite"
            direction="alternate"
            duration={500}
            useNativeDriver
          >
  
            <Icon
              name="gps-fixed"
              size={24}
              color={Colors.white}
              style={styles.icon}
            />
          </Animatable.View>
        </Center>
      </Row>
    </TouchableOpacity>
  );
};

export default GpsButton;
