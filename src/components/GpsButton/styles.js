import { StyleSheet } from 'react-native';
import { Colors } from '../../colors';

export default StyleSheet.create({
  baseContainer: {
    backgroundColor: Colors.blue,
    paddingVertical: 8,
  },
  text: {
    color: Colors.white,
  },
  icon: {
    marginLeft: 8,
  },
});
