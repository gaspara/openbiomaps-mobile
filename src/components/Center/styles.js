import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  baseContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
});
