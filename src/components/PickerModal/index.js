import React, { Component } from 'react';
import { Modal, ScrollView, TouchableOpacity, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import { styles } from './styles';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';

class PickerModal extends Component {
  onItemSelect = item => () => {
    const { onItemSelect, onRequestClose } = this.props;
    onItemSelect(item);
    onRequestClose();
  };

  renderItem = (item, index, arr) => {
    const { customPickerStyle, selectedValue } = this.props;
    const itemContainerStyle = [
      styles.itemContainer,
      customPickerStyle.itemContainer,
      index === arr.length - 1 && { marginBottom: 10 },
      index === 0 && { marginTop: 10 },
    ];

    return (
      <TouchableOpacity style={itemContainerStyle} onPress={this.onItemSelect(item)} key={item.key}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={styles.text}>{item.value}</Text>
          {selectedValue === item.value.toString() && <FontAwesome5Icon name='check' size={25} />}
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    const { modalTitle, showModal, data, onRequestClose, customPickerStyle } = this.props;

    return (
      <Modal transparent visible={showModal} onRequestClose={onRequestClose}>
        <TouchableOpacity
          style={[styles.backdropContainer, customPickerStyle.backdropContainer]}
          onPress={onRequestClose}
        >
          <View style={[styles.list, customPickerStyle.list]}>
            {modalTitle != null && <Text adjustsFontSizeToFit={true} style={{ padding: 10, fontSize: 20, fontWeight: 'bold', textAlign: 'center' }}>{modalTitle}</Text>}
            <ScrollView>
              {data.map((item, index, arr) => this.renderItem(item, index, arr))}
            </ScrollView>
          </View>
        </TouchableOpacity>
      </Modal>
    );
  }
}

PickerModal.propTypes = {
  modalTitle: PropTypes.string,
  selectedValue: PropTypes.string,
  showModal: PropTypes.bool.isRequired,
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  onRequestClose: PropTypes.func.isRequired,
  onItemSelect: PropTypes.func.isRequired,
  customPickerStyle: PropTypes.shape({
    backdropContainer: PropTypes.object,
    itemContainer: PropTypes.object,
    list: PropTypes.object,
  }),
};

PickerModal.defaultProps = {
  customPickerStyle: {},
};

export default PickerModal;
