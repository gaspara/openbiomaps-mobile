import { StyleSheet } from 'react-native';
import { Colors } from '../../colors';

export const footerStyle = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    backgroundColor: '#7EC667',
    alignItems: 'stretch',
    justifyContent: 'space-around',
    height: 70,
    padding: 12,
  },
  narrowContainer: {
    flexDirection: 'row',
    width: '100%',
    backgroundColor: '#7EC667',
    alignItems: 'stretch',
    justifyContent: 'space-around',
    height: 40,
    padding: 4,
  },
  wrapContentContainer: {
    flexDirection: 'row',
    width: '100%',
    backgroundColor: '#7EC667',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 12,
  },
  sessionButtonWrapper: { 
    flexDirection: 'row', 
    flex: 5,
  },
  rightButtonContainer: {
    marginLeft: 'auto',
    flexDirection: 'row',
    flex: 3,
  },
  roundedShape: {
    borderWidth: 1.3,
    borderColor: 'black',
    borderRadius: 5,
    justifyContent: 'center',
    marginHorizontal: 10,
  },
  leftButton: {
    flex: 1,
  },
  middleButton: {
    flex: 2,
  },
  rightButton: {
    flex: 3,
  },
  singleButton: {
    paddingHorizontal: 10,
  },
  narrowButton: {
    padding: 5,
  },
  hidden: {
    display: 'none',
  },
  narrowFooterText: {
    fontSize: 14,
    color: 'black',
  },
  footerText: {
    fontSize: 16,
    color: 'black',
  },
  deleteButtonLabel: {
    fontSize: 14,
    color: Colors.darkRed,
  },
  pickerContainer: {
    flex: 1,
    padding: 10,
  },
});
