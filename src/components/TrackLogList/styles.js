import { StyleSheet } from 'react-native';
import { Colors } from '../../colors';

export default StyleSheet.create({
  topContainer: {
    flex: 1,
    paddingHorizontal: 10,
    paddingVertical: 8,
  },
  sessionContainer: {
    flex: 1,
    paddingVertical: 8,
    borderEndWidth: 10,
    paddingStart: 10,
  },
  trackLogNameContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
  },
  buttonContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  icon: {
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 3,
    paddingHorizontal: 20,
    paddingVertical: 7,
    marginStart: 7,
    marginTop: 5,
  },
  trackLogNameText: {
    fontSize: 15,
    color: 'black',
    fontWeight: 'bold',
  },
  emptyListContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  warningText: {
    color: Colors.orange,
  },
  errorText: {
    color: Colors.brickRed,
  },
});
