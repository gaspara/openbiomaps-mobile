import React from 'react';
import { View, Text } from 'react-native';

import PropTypes from 'prop-types';

import { TouchableIcon } from '../TouchableIcon';
import { hashStringToHexColor } from '../../utils';
import I18n from '../../i18n';

import styles from './styles';

const TrackLogListItem = ({ 
  trackLogId, 
  trackLogName, 
  selectedDatabases,
  trackLogLength,
  sessionId, 
  onDeletePress, 
  onViewMapPress, 
  onDownloadPress, 
}) => {
  const title = trackLogName.split(/(?=_)/).map((text) => (<Text style={styles.trackLogNameText} key={text}>{text}</Text>));

  const renderDatabaseInfoText = selectedDatabases.reduce((acc, { serverName, databaseName, isSynced, error }) => {
    if (sessionId && !error) return acc;
    let syncedString = '';
    if (selectedDatabases.length > 1 && !sessionId) {
      syncedString = isSynced ? `(${I18n.t('synced')})` : `(${I18n.t('unsynced')})`;
    } 
    const infoString = `-${sessionId ? '' : `${serverName} (${databaseName})`}${syncedString}${(!sessionId && error) ? ':' : ''} ${error || ''}`;
    acc.push((
      <Text style={error && styles.errorText} key={databaseName}>{infoString}</Text>
    ));
    return acc;
  }, []);

  return (
    <View style={
        !sessionId 
          ? styles.topContainer
          : [styles.sessionContainer, { borderEndColor: hashStringToHexColor({ string: sessionId }) }]
}
    >
      <View style={styles.trackLogNameContainer}>{title}</View>
      { renderDatabaseInfoText.length !== 0 
        && <View>{renderDatabaseInfoText}</View>}
      { selectedDatabases.length === 0
        && <Text style={styles.warningText}>{I18n.t('no_server_assigned_to_tracklog')}</Text>}
      { trackLogLength === 0
        && <Text style={styles.warningText}>{I18n.t('tracklog_length', { length: trackLogLength })}</Text>}
      { trackLogLength > 0 
        && <Text>{I18n.t('tracklog_length', { length: trackLogLength })}</Text>}
      <View style={styles.buttonContainer}>
        <TouchableIcon style={styles.icon} name="map" size={22} width={30} onPress={() => onViewMapPress(trackLogId)} />
        <TouchableIcon style={styles.icon} name="download" iconType="MaterialCommunityIcon" size={22} onPress={() => onDownloadPress(trackLogId)} />
        <TouchableIcon style={styles.icon} name="delete" size={22} onPress={() => onDeletePress(trackLogId, { sessionId, selectedDatabases })} />
      </View>  
    </View>
  );
};

TrackLogListItem.propTypes = {
  trackLogId: PropTypes.string.isRequired,
  trackLogName: PropTypes.string.isRequired,
  selectedDatabases: PropTypes.array.isRequired,
  trackLogLength: PropTypes.number,
  sessionId: PropTypes.string,
  onDeletePress: PropTypes.func.isRequired,
  onViewMapPress: PropTypes.func.isRequired,
  onDownloadPress: PropTypes.func.isRequired,
};

TrackLogListItem.defaultProps = {
  sessionId: null,
  trackLogLength: 0,
};

export default TrackLogListItem;
