import React from 'react';
import {
  View,
  Text,
  TouchableOpacity, 
} from 'react-native';
import Modal from 'react-native-modal';
import { FlatList } from 'react-native-gesture-handler';
import PropTypes from 'prop-types';
import styles from './styles';
import I18n from '../../i18n';

const renderItem = item => (

  <View style={styles.itemContainer}>

    {item.description && (
      <Text style={styles.subText} ellipsizeMode="tail" numberOfLines={1}>
        {item.description}
      </Text>
        )}
    <Text style={styles.mainText}>{item.name}</Text>
    
  </View>
  
);

const ListModal = ({
  isOpen,
  listData,
  onPress,
}) => (
  <Modal
    animationIn="fadeIn"
    animationOut="fadeOut"
    isVisible={isOpen}
    onBackdropPress={onPress}
    onBackButtonPress={onPress}
    backdropOpacity={0.5}
    hideModalContentWhileAnimating
    propagateSwipe
  >

    <View style={styles.content}>

      <FlatList
        data={listData}
        renderItem={item => renderItem(item.item)}
        keyExtractor={(item, index) => item.toString() + index}
      />
      <TouchableOpacity onPress={onPress}>
        <View style={styles.closeButton}>
          <Text style={styles.mainText}>{I18n.t('close')}</Text>
        </View>
      </TouchableOpacity>
    </View>
  </Modal>
);

ListModal.propTypes = {
  isOpen: PropTypes.bool,
  listData: PropTypes.arrayOf(PropTypes.shape({})),
  onPress: PropTypes.func,
};

ListModal.defaultProps = {
  isOpen: false,
  listData: [],
  onPress: () => ({}),
};

export { ListModal };

