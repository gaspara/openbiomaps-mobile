import { StyleSheet } from 'react-native';
import { Colors } from '../../colors';

export default StyleSheet.create({
  modal: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'center',
  },
  content: {
    flex: 0,
    marginHorizontal: 30,
    marginVertical: 80,
    borderRadius: 5,
    padding: 10,
    backgroundColor: Colors.dirtyWhite,
    elevation: 1,
  },
  mainText: {
    fontSize: 18,
    color: '#212121',
  },
  subText: {
    fontSize: 16,
    color: '#888',
  },
  itemContainer: {
    padding: 6,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingStart: 10,
  },
  closeButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
