import React from 'react';
import { View, SectionList, Text } from 'react-native';
import PropTypes from 'prop-types';
import I18n from '../../i18n';
import ObservationOneLine from './ObservationOneLine';
import styles from './styles';

const keyExtractor = item => String(`${item.server.id}_${item.database.id}_${item.observation.id}_${item.measurement.id}`);

const filterAndSetDatabaseName = (allData, filterCondition) =>
  allData.reduce((acc, currItem, index, src) => {
    const { database: { name } } = currItem;

    if (filterCondition(currItem)) {
      if (index === 0 || (index !== 0 && src[index - 1].database?.name !== name)) {
        acc.push({ ...currItem, showDatabaseName: true });
      } else {
        acc.push(currItem);
      }
    }
    return acc;
  }, []);

const createSections = (measurementData, type) => {
  // Sort measurements - newest first
  measurementData.sort((a, b) => new Date(b.measurement.date) - new Date(a.measurement.date));

  // Define section data
  let uploadedMeasurements = [];
  let nonvalidMeasurements = [];
  let syncingMeasurements = [];
  let pendingMeasurements = [];

  switch (type) {
    case 'synced':
      uploadedMeasurements = filterAndSetDatabaseName(measurementData, ({ measurement }) =>
        measurement.isSynced && !measurement.errors);
      break;
    case 'unsynced':
      nonvalidMeasurements = filterAndSetDatabaseName(measurementData, ({ measurement }) =>
        measurement.errors && !measurement.syncing);

      syncingMeasurements = filterAndSetDatabaseName(measurementData, ({ measurement }) =>
        measurement.syncing);

      pendingMeasurements = filterAndSetDatabaseName(measurementData, ({ measurement }) =>
        !measurement.errors && !measurement.isSynced && !measurement.syncing);
        
      break;
    default:
      break;
  }


  // Populate sections
  const sections = [];

  if (uploadedMeasurements.length) sections.push({ title: I18n.t('measurement_uploaded'), data: uploadedMeasurements });
  if (nonvalidMeasurements.length) sections.push({ title: I18n.t('measurement_nonvalid'), data: nonvalidMeasurements, editable: true });
  if (syncingMeasurements.length) sections.push({ title: I18n.t('measurement_syncing'), data: syncingMeasurements });
  if (pendingMeasurements.length) sections.push({ title: I18n.t('measurement_pending'), data: pendingMeasurements, editable: true });

  return sections;
};

const showEmptyList = text => (
  <View style={styles.emptyListContainer}>
    <Text>{text}</Text>
  </View>
);

const ObservationOneLineList = ({ measurements, type, onEditPress, onDeletePress, onSchemaPress, onAddNumInd }) => {
  const sections = createSections(measurements, type);

  if (type === 'unsynced' && !sections.length) return showEmptyList(I18n.t('no_unsynced_data'));
  if (type === 'synced' && !sections.length) return showEmptyList(I18n.t('no_synced_data'));

  return (
    <SectionList
      keyExtractor={keyExtractor}
      sections={sections}
      initialNumToRender={20}
      renderSectionHeader={({ section: { title, data } }) => (
        data.length ? <Text style={styles.sectionHeader}>{title}</Text> : null
      )}
      windowSize={20}
      renderItem={({ item, section }) => (
        <ObservationOneLine
          values={{
            ...item.measurement.data,
            createdAt: item.measurement.date,
            // isNullRecord: item.measurement.meta?.observationListMetaRecord, 
          }}
          databaseName={item.showDatabaseName ? `${item.database.name}\n` : ''}
          observationName={item.observation.name}
          errors={item.measurement.errors}
          dataTypes={item.observation.form.dataTypes}
          boldYellow={item.observation.form.boldYellow}
          editable={section.editable}
          numInd={item.observation.form.numInd}
          sessionId={item.measurement?.meta?.sessionId}
          onEditPress={() => onEditPress(item)}
          onDeletePress={() => onDeletePress(item)}
          onSchemaPress={() => onSchemaPress(item)}
          onIncreaseNumInd={() => onAddNumInd(item, item.observation.form.numInd, 1)}
          onDecreaseNumInd={() => onAddNumInd(item, item.observation.form.numInd, -1)}
        />)}
    />
  );
};

ObservationOneLineList.propTypes = {
  measurements: PropTypes.arrayOf(PropTypes.object).isRequired,
  type: PropTypes.oneOf(['synced', 'unsynced']),
  onEditPress: PropTypes.func,
  onDeletePress: PropTypes.func.isRequired,
  onSchemaPress: PropTypes.func.isRequired,
  onAddNumInd: PropTypes.func,
};

ObservationOneLineList.defaultProps = {
  type: 'unsynced',
  onEditPress: () => { },
  onAddNumInd: () => { },
};

export default ObservationOneLineList;
