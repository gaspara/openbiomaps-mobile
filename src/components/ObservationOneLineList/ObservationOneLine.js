import React, { Component } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import MapComponent from '../Map';
import MeasurementInfo from '../MeasurementInfo';
import { hashStringToHexColor } from '../../utils';

class ObservationOneLine extends Component {
  shouldComponentUpdate = nextProps => (
    (nextProps.values[nextProps.numInd] !== this.props.values[this.props.numInd]) ||
    (nextProps.errors !== this.props.errors)
  );

  getRegion = (measurements) => {
    const positions = measurements.map(measurement => measurement.coordinate);
    const longitudes = positions.map(position => position.longitude);
    const latitudes = positions.map(position => position.latitude);
    const maxLatitude = Math.max(...latitudes);
    const minLatitude = Math.min(...latitudes);
    const maxLongitude = Math.max(...longitudes);
    const minLongitude = Math.min(...longitudes);
    switch (positions.length) {
      case 0:
        return null;
      case 1:
        return {
          ...positions[0],
          // distance of the edges of the map in degrees
          // too low values make the map pixelated
          latitudeDelta: 0.0005,
          longitudeDelta: 0.006,
        };
      default:
        return {
          latitude: (maxLatitude + minLatitude) / 2,
          longitude: (maxLongitude + minLongitude) / 2,
          latitudeDelta: Math.max(maxLatitude - minLatitude, 0.0005),
          longitudeDelta: Math.max(maxLongitude - minLongitude, 0.006),
        };
    }
  };

  getPositions = () => {
    const { dataTypes, values } = this.props;

    return dataTypes
      .filter(data => (data.type === 'point' || data.type === 'wkt') && values[data.column])
      .map(data => ({
        id: data.column,
        coordinate: values[data.column],
      }));
  };

  renderMapComponent = () => {
    const { boldYellow = false, dataTypes } = this.props;

    if (
      boldYellow.some(item => item === 'obm_geometry') ||
      (!boldYellow.length && dataTypes[0].column === 'obm_geometry')
    ) {
      const positions = this.getPositions();
      const region = this.getRegion(positions);

      if (region) {
        return (
          <View style={styles.mapContainer}>
            <MapComponent region={region} liteMode lastMeasurements={positions} />
          </View>
        );
      }
      return null;
    }
    return null;
  };

  render() {
    const {
      dataTypes,
      values,
      databaseName,
      observationName,
      boldYellow = [],
      editable,
      numInd,
      sessionId,
      onEditPress,
      onDeletePress,
      onSchemaPress,
      onIncreaseNumInd,
      onDecreaseNumInd,
    } = this.props;

    return (
      <View style={
        !sessionId ? 
        styles.container :
        [styles.sessionContainer, { borderEndColor: hashStringToHexColor({ string: sessionId }) }]}
      >
        {/* {this.renderMapComponent()} */}
        <View style={styles.description}>
          <MeasurementInfo
            dataTypes={dataTypes}
            observationName={observationName}
            values={values}
            databaseName={databaseName}
            boldYellow={boldYellow}
            editable={editable}
            numInd={numInd}
            onEditPress={onEditPress}
            onDeletePress={onDeletePress}
            onSchemaPress={onSchemaPress}
            onIncreaseNumInd={onIncreaseNumInd}
            onDecreaseNumInd={onDecreaseNumInd}
          />
        </View>
      </View>
    );
  }
}

ObservationOneLine.propTypes = {
  values: PropTypes.shape({}).isRequired,
  databaseName: PropTypes.string.isRequired,
  errors: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ]),
  observationName: PropTypes.string,
  dataTypes: PropTypes.array.isRequired,
  boldYellow: PropTypes.array.isRequired,
  editable: PropTypes.bool,
  numInd: PropTypes.string,
  sessionId: PropTypes.string,
  onEditPress: PropTypes.func,
  onDeletePress: PropTypes.func.isRequired,
  onSchemaPress: PropTypes.func.isRequired,
  onIncreaseNumInd: PropTypes.func,
  onDecreaseNumInd: PropTypes.func,
};

ObservationOneLine.defaultProps = {
  editable: false,
  numInd: null,
  sessionId: null,
  errors: null,
  onEditPress: () => {},
  onIncreaseNumInd: () => {},
  onDecreaseNumInd: () => {},
};

export default ObservationOneLine;
