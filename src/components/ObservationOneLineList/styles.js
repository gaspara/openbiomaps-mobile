import { StyleSheet } from 'react-native';
import { } from '../../colors';

export default StyleSheet.create({
  container: {
    width: '100%',
    alignSelf: 'center',
    overflow: 'hidden',
  },
  sessionContainer: {
    width: '100%',
    alignSelf: 'center',
    overflow: 'hidden',
    borderEndWidth: 25,
  },
  mapContainer: {
    height: 200,
  },
  description: {
    padding: 15,
  },
  sectionHeader: {
    fontWeight: 'bold',
    fontSize: 16,
    marginTop: 15,
    textAlign: 'center',
  },
  emptyListContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
