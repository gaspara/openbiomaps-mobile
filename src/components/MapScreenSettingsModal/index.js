import React from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import Modal from 'react-native-modal'; 
import PropTypes from 'prop-types';
import I18n from '../../i18n';
import { styles } from './styles';
import SettingsItem from '../SettingsItem';
import { DatePicker } from '../MeasurementForm/FormListItems';

const MapScreenSettingsModal = ({
  isModalVisible,
  showTrackLog,
  showMeasurements,
  filterByDate,
  showPernamentSamplePlots,
  startTime,
  endTime,
  changeFilter,
  changeTrackLogVisibility,
  changeMeasurementsVisibility,
  changePernamentSamplePlotsVisibility,
  hideModal,
  shiftIntervalToCurrentTime,
}) => (
  <Modal
    transparent
    visible={isModalVisible}
    onRequestClose={hideModal}
    onBackdropPress={hideModal}
  >

    <View style={styles.modalView}>
      <ScrollView>
        <Text style={styles.titleText}>{I18n.t('map_settings_modal_title')}</Text>
        <SettingsItem
          type="switch"
          title={I18n.t('measurements_visibility_setting_title')}
          value={showMeasurements}
          onValueChange={changeMeasurementsVisibility}
        />
        <SettingsItem
          type="switch"
          title={I18n.t('tracklog_visibility_setting_title')}
          value={showTrackLog}
          onValueChange={changeTrackLogVisibility}
        />
        <SettingsItem
          type="switch"
          title={I18n.t('pernament_sample_plots_visibility')}
          value={showPernamentSamplePlots}
          onValueChange={changePernamentSamplePlotsVisibility}
        />
        <SettingsItem
          type="switch"
          title={I18n.t('date_based_map_filter_title')}
          value={filterByDate}
          onValueChange={value => changeFilter(value, value ? startTime : null, value ? endTime : null)}
        />
        { filterByDate && startTime && endTime &&
        <View>
          <Text style={styles.minorText}>{I18n.t('date_based_map_filter_start')}</Text>
          <View style={styles.row}>
            <DatePicker
              value={startTime}
              onValueChange={value => changeFilter(filterByDate, value, endTime)}
            />
          </View>
          <Text style={styles.minorText}>{I18n.t('date_based_map_filter_end')}</Text>
          <View style={styles.row}>
            <DatePicker
              value={endTime}
              onValueChange={value => changeFilter(filterByDate, startTime, value)}
            />
          </View>
          
          <TouchableOpacity
            style={styles.shiftButton} 
            onPress={shiftIntervalToCurrentTime}
          >
            <Text style={styles.buttonLabel}>{I18n.t('date_based_map_filter_shift_button')}</Text>
          </TouchableOpacity>
        </View>
      }
      </ScrollView>
    </View>
  </Modal>
);

MapScreenSettingsModal.propTypes = {
  isModalVisible: PropTypes.bool.isRequired,
  showTrackLog: PropTypes.bool.isRequired,
  showMeasurements: PropTypes.bool.isRequired,
  showPernamentSamplePlots: PropTypes.bool.isRequired,
  filterByDate: PropTypes.bool.isRequired,
  startTime: PropTypes.oneOfType([
    PropTypes.instanceOf(Date),
    PropTypes.number,
    PropTypes.string,
  ]),
  endTime: PropTypes.oneOfType([
    PropTypes.instanceOf(Date),
    PropTypes.number,
    PropTypes.string,
  ]),
  hideModal: PropTypes.func.isRequired,
  changeTrackLogVisibility: PropTypes.func.isRequired,
  changeMeasurementsVisibility: PropTypes.func.isRequired,
  changePernamentSamplePlotsVisibility: PropTypes.func.isRequired,
  changeFilter: PropTypes.func.isRequired,
  shiftIntervalToCurrentTime: PropTypes.func.isRequired,
};

MapScreenSettingsModal.defaultProps = {
  startTime: null,
  endTime: null,
};

export { MapScreenSettingsModal };
