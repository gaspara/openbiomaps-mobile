import { StyleSheet } from 'react-native';
import { Colors } from '../../colors';


export const styles = StyleSheet.create({

  modalView: {
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: 'white',
    borderColor: Colors.mantisGreen,
    borderRadius: 20,
    borderWidth: 5,
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  titleText: {
    color: 'black',
    fontSize: 15,
    textAlign: 'center',
  },
  minorText: {
    paddingVertical: 5,
    paddingHorizontal: 15,
    fontSize: 15,
    color: Colors.mantisGreen,
    textAlign: 'justify',
  },
  buttonLabel: {
    color: 'white',
    fontSize: 17,
    textAlign: 'center',
    textTransform: 'uppercase',
  },
  row: { 
    flexDirection: 'row',
  },
  shiftButton: {
    margin: 15,
    backgroundColor: Colors.mantisGreen,
    borderColor: Colors.asparagusGreen,
    borderWidth: 3,
    borderRadius: 5,
    padding: 10,
  },
});
  
