import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import { styles } from './styles';
import I18n from '../../i18n';

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
      errorMessage: null,
    };
  }
  
  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return {
      hasError: true,
      errorMessage: error.message,
    };
  }
  
  render() {
    const { hasError, errorMessage } = this.state;
    if (hasError) {
      // You can render any custom fallback UI
      return (
        <View style={styles.errorContainer}>
          <Text style={styles.errorText}>{I18n.t('unexpected_component_error')}</Text>
          {errorMessage !== null && <Text style={styles.errorText}>{errorMessage}</Text>}
        </View>);
    }
  
    return this.props.children; 
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.object.isRequired,
};

export default ErrorBoundary;
