import React, { Component } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import Notification from '../Notification';
import { NotificationModel } from '../../models/NotificationModel';
import styles from './styles';
import SoundPlayer from 'react-native-sound-player'

/**
 * @method addNotification          - add new notificaiton to the local notifications array
 * @argument {object} notificaiton  - {{ message, color }}
 * 
 * @method removeNotification       - remove notificaiton from the local notifications array
 * @argument {object} notificaiton  - {{ message }}
 * 
 * @returns {<Notification />} 
 */
class NotificationHandler extends Component {
  constructor(props) {
    super(props);

    this.state = {
      notifications: [],
      timeoutCallIds: {}
    };

    this.fixableNotifications = [0, 4, 5, 7, 17];
  }

  shouldComponentUpdate = (_, nextState) => {
    const { notifications: nextNotifications } = nextState;
    const { notifications: currNotifications } = this.state;

    if (JSON.stringify(currNotifications) !== JSON.stringify(nextNotifications)) {
      return true;
    }

    return false;
  }

  componentWillUnmount = () => {
    const { timeoutCallIds } = this.state;
    Object.entries(timeoutCallIds).forEach(([, value]) => {
      if (value) clearTimeout(value);
    });
  }

  _validateNotificationObject = (notification) => {
    if (!(notification instanceof NotificationModel)) {
      throw new Error('Property is not an instance of NotificationModel');
    }
  };

  _hasSameNotification = notificaiton => this.state.notifications.some(item =>
    item.id === notificaiton.id);

  _addNotification = (notification) => {
    if (notification.enableSound && notification.sound) {
      try {
        SoundPlayer.addEventListener('FinishedLoading')

        SoundPlayer.addEventListener('FinishedLoadingFile')

        SoundPlayer.loadSoundFile('succesfully_recorded', 'wav');

        SoundPlayer.play();

      } catch (e) {
        Sentry.captureMessage(`Success Notification Sound Error: ${e}`);
      }
    }

    this.setState(prevState => ({
      notifications: prevState.notifications.concat(notification),
    }));

  };

  /**
   * Update is neceassary to rerender the <Notification /> component when the
   * language is changed. 
   */
  _updateNotification = (notification) => {
    const updateIndex = this.state.notifications.findIndex(item => item.id === notification.id);
    if (updateIndex > -1) {
      this.setState((prevState) => {
        const updatedNotifications = prevState.notifications.map(oldNotification =>
          (oldNotification.id === notification.id ? notification : oldNotification));
        return ({
          notifications: updatedNotifications,
        });
      });
    }
  };

  _deleteNotification = (notification) => {
    const newTimeoutCallIds = { ...this.state.timeoutCallIds };
    if (notification.dismissTime) {
      if (newTimeoutCallIds[notification.id]) clearTimeout(newTimeoutCallIds[notification.id]);
      newTimeoutCallIds[notification.id] = undefined;
    }
    this.setState(prevState => ({
      notifications: prevState.notifications.filter(item => item.id !== notification.id),
      timeoutCallIds: newTimeoutCallIds,
    }));
  }

  _animateOutNotification = (notification) => {
    this._updateNotification({ ...notification, animation: 'bounceOut' });
  }

  _setDismissTimeOut = (notification) => {
    if (notification.dismissTime) {
      const newTimeoutCallIds = { ...this.state.timeoutCallIds };
      if (newTimeoutCallIds[notification.id]) {
        clearTimeout(newTimeoutCallIds[notification.id]);
      }
      newTimeoutCallIds[notification.id] = setTimeout(() => { this._deleteNotification(notification); }, notification.dismissTime);
      this.setState({
        timeoutCallIds: newTimeoutCallIds,
      });
    }
  }

  showNotification = (notification) => {
    this._validateNotificationObject(notification);
    if (this._hasSameNotification(notification)) {
      this._updateNotification(notification);
    } else {
      this._addNotification(notification);
    }

    this._setDismissTimeOut(notification);
  };

  hideNotification = (notification) => {
    this._validateNotificationObject(notification);
    if (this._hasSameNotification(notification)) {
      this._deleteNotification(notification);
    }
  };

  render() {
    const { fixed } = this.props;
    const { notifications } = this.state;
    return (
      <View>
        {fixed === 0 && notifications.map(({ message, color, component = null, animation = null, id }) => (
          <Notification key={id} notification={message} backgroundColor={color} component={component} animation={animation} />
        ))}
        {fixed !== 0 &&
          <View>
            <View
              backgroundColor={fixed !== 0 && styles.baseContainerStyle.backgroundColor}
              minHeight={styles.baseContainerStyle.minHeight * fixed}
            >
              {notifications.filter(({ id }) => this.fixableNotifications.includes(id))
                .map(({ message, color, component = null, animation = null, id }) => (
                  <Notification key={id} notification={message} backgroundColor={color} component={component} animation={animation} />
                ))}
            </View>
            {notifications.filter(({ id }) => !this.fixableNotifications.includes(id))
              .map(({ message, color, component = null, animation = null, id }) => (
                <Notification key={id} notification={message} backgroundColor={color} component={component} animation={animation} />
              ))}
          </View>

        }
      </View>
    );
  }
}

NotificationHandler.propTypes = {
  fixed: PropTypes.number,
};

NotificationHandler.defaultProps = {
  fixed: 0,
};

export default NotificationHandler;
