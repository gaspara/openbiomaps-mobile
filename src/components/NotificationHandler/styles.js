import { StyleSheet } from 'react-native';
import { Colors } from '../../colors';

export default StyleSheet.create({
  baseContainerStyle: {
    minHeight: 31,
    backgroundColor: Colors.mantisGreen,
  },
});
