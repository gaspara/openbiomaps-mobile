import * as Sentry from '@sentry/react-native';
import React, { Component } from 'react';
import { View, BackHandler } from 'react-native';
import PropTypes from 'prop-types';
import { SelectorList } from '../SelectorList';
import NavigationHandler from '../../NavigationHandler';
import { Spinner } from '..';
import EmptyList from '../EmptyList';
import client from '../../config/apiClient';
import I18n from '../../i18n';
import { ERROR } from '../../Constants';

class ServerList extends Component {
  // eslint-disable-next-line
  _willFocusSubscription;

  _willBlurSubscription;

  _didParentFocusSubscription;

  _willParentBlurSubscription;

  constructor(props) {
    super(props);

    this.state = {
      initLoading: true,
    };
    this.backPressListener = null;
  }

  componentDidMount = () => {
    const { navigation } = this.props;
    const { state: { routeName } } = navigation;
    const parentNavigator = navigation.dangerouslyGetParent();

    if (parentNavigator) {
      // Fetch data only when parent navigator gets in focus.
      this._didParentFocusSubscription = parentNavigator.addListener('didFocus', () => {
        if (routeName === 'ServersScreen') this.loadServers();
        this.setState({ initLoading: false });
      });
  
      this._willParentBlurSubscription = parentNavigator.addListener('willBlur', () => {
        this.setState({ initLoading: true });
      });
    } else {
      Sentry.captureMessage('ParentNavigator is null in ServerList. Should not be null');
    }

    // Force update component when it is in focus to evoke render() for updateing changed props.
    // forceUpdate() calls render() by skipping shouldComponentUpdate()
    this._willFocusSubscription = navigation.addListener('willFocus', () => {
      this.forceUpdate();
      this.backPressListener = BackHandler.addEventListener('hardwareBackPress', this.onAndroidBackPress);
    });

    this._willBlurSubscription = navigation.addListener('willBlur', () => this.backPressListener?.remove());
  };

  shouldComponentUpdate = (nextProps, nextState) => {
    const { fetching } = this.props;
    const { initLoading } = this.state;

    // Render component only when the feching state changes to prevent rerender every time
    // when a deeply nested property changes in the hierarchy tree.
    if (fetching !== nextProps.fetching) {
      return true;
    }

    // Rerender component when initLoading state changes
    if (initLoading !== nextState.initLoading) {
      return true;
    }

    return false;
  };

  componentWillUnmount() {
    if (this._willFocusSubscription) this._willFocusSubscription.remove();
    if (this._willBlurSubscription) this._willBlurSubscription.remove();
    if (this._didParentFocusSubscription) this._didParentFocusSubscription.remove();
    if (this._willParentBlurSubscription) this._willParentBlurSubscription.remove();
  }

  onAndroidBackPress = () => {
    NavigationHandler.navigate('Home');
    return true;
  };

  getData = () => (
    this.props.serverList.map((server) => ({
      ...server,
      error: (server.user && server.user.error === ERROR.INVALID_REFRESH_TOKEN) ? I18n.t('invalid_refresh_token') : null,
    }))
  );

  loadServers = () => {
    this.props.loadServers();
  };

  selectServer = (server) => {
    const { databases = {} } = server;
    const { data: databaseList = [] } = databases;

    // Set selected server url
    client.prefix = server.url;

    this.props.selectServer(server.id);

    if (!server.user || !server.user.accessToken) {
      NavigationHandler.navigate('Login', { title: server.name });
    } else if (databaseList.some((database) => database.lastSelected && database.observations.data.length)) {
      NavigationHandler.navigate(
        'DatabaseList',
        { title: server.name },
        null,
        NavigationHandler.navigate('LastDatabasesScreen'),
      );
    } else {
      NavigationHandler.navigate(
        'DatabaseList',
        { title: server.name },
        null,
        NavigationHandler.navigate('DatabaseListScreen'),
      );
    }
  };

  render() {
    const { serverList, fetching, navigation } = this.props;
    const { initLoading } = this.state;
    const { state: { routeName } } = navigation;

    if (routeName === 'ServersScreen' && (fetching || initLoading)) return <Spinner />;
    if (!serverList.length) return <EmptyList onRefreshPress={this.loadServers} />;

    return (
      <View style={{ flex: 1 }}>
        <SelectorList data={this.getData()} onPress={this.selectServer} />
      </View>
    );
  }
}

ServerList.propTypes = {
  loadServers: PropTypes.func.isRequired,
  selectServer: PropTypes.func.isRequired,
  serverList: PropTypes.array.isRequired,
  navigation: PropTypes.object.isRequired,
  fetching: PropTypes.bool,
};

ServerList.defaultProps = {
  fetching: false,
};

export default ServerList;
