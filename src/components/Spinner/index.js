import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import styles from './styles';
import { Colors } from '../../colors';

export const Spinner = () => (
  <View style={styles.componentContainerStyle}>
    <ActivityIndicator size="large" color={Colors.mantisGreen} />
  </View>
);
