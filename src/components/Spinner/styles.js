import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  componentContainerStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
