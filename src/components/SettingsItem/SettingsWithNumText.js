import React, { Component } from 'react';
import { TouchableOpacity, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import { SettingsWithNumTextStyle as styles } from './styles';
import { TextInput } from '../TextInput';
import { showAlertDialog } from '../../components/AlertDialog';
import I18n from '../../i18n';

class SettingsWithNumText extends Component {
  constructor(props) {
    super(props);

    const { value, multiplierForStoring } = this.props; 

    this.state = {
      newValue: (value / multiplierForStoring).toString(),
      lastValue: (value / multiplierForStoring).toString(),
    };
  }

  handleDonePress = () => {
    const { onValueChange, minValue, maxValue, alertBelow, alertAbove, alertTitle, alertMessage, multiplierForStoring } = this.props;

    if (isNaN(this.state.newValue)) {
      showAlertDialog({
        title: I18n.t('error'),
        message: I18n.t('error_nan'),
        positiveLabel: I18n.t('ok'),
      });
      this.setState({ newValue: this.state.lastValue });
    } else {
      const newNumber = Number(this.state.newValue);
      if (newNumber >= minValue && newNumber <= maxValue) {
        if (newNumber % 1 === 0) {
          this.setState({ lastValue: this.state.newValue });
          onValueChange(newNumber * multiplierForStoring);
          if ((alertBelow && alertAbove === null && newNumber < alertBelow) || 
              (!alertBelow && alertAbove !== null && newNumber > alertAbove) ||
              (alertBelow && alertAbove !== null && newNumber > alertAbove && newNumber < alertBelow)) {
            showAlertDialog({
              title: alertTitle || I18n.t('default_alert_title'),
              message: alertMessage,
              positiveLabel: I18n.t('ok'),
            });
          }
        } else {
          showAlertDialog({
            title: I18n.t('error'),
            message: I18n.t('not_integer'),
            positiveLabel: I18n.t('ok'),
          });
          this.setState({ newValue: this.state.lastValue });
        }
      } else {
        showAlertDialog({
          title: I18n.t('error'),
          message: I18n.t('error_number_out_of_range', { min: minValue, max: maxValue }),
          positiveLabel: I18n.t('ok'),
        });
        this.setState({ newValue: this.state.lastValue });
      }
    }
  };

  setBackToOriginal = () => {
    const { value, multiplierForStoring } = this.props; 
    
    this.setState({
      newValue: (value / multiplierForStoring).toString(),
      lastValue: (value / multiplierForStoring).toString(),
    });
  }

  render() {
    const { title, description } = this.props;

    return (
      <TouchableOpacity
        style={styles.baseContainer}
        activeOpacity={1}
      >
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{title}</Text>
          {description && <Text style={styles.description}>{description}</Text>}
        </View>
        <TextInput
          value={this.state.newValue}
          onChangeText={value => this.setState({ newValue: value.replace(/[A-Za-z.,]/g, '') })}
          onEndEditing={this.setBackToOriginal}
          onSubmitEditing={this.handleDonePress}
          numeric
          keyboardType="numeric" 
          returnKeyType="done"
        />
      </TouchableOpacity>
    );
  }
}

SettingsWithNumText.propTypes = {
  title: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
  onValueChange: PropTypes.func.isRequired,
  description: PropTypes.string,
  minValue: PropTypes.number,
  maxValue: PropTypes.number,
  alertBelow: PropTypes.number,
  alertAbove: PropTypes.number,
  alertTitle: PropTypes.string,
  alertMessage: PropTypes.string,
  multiplierForStoring: PropTypes.number,
};

SettingsWithNumText.defaultProps = {
  description: null,
  minValue: 1,
  maxValue: 1000,
  alertBelow: null,
  alertAbove: null,
  alertTitle: null,
  alertMessage: null,
  multiplierForStoring: 1,
};

export default SettingsWithNumText;
