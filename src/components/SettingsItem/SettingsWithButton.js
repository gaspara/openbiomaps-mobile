import React from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';
import { SettingsWithButtonStyle as styles } from './styles';
import { TouchableIcon } from '../TouchableIcon';

const SettingsWithButton = ({ title, description, onPress, iconName }) => (
  <View 
    style={styles.baseContainer}
  >
    <View style={styles.titleContainer}>
      <Text style={styles.title}>{title}</Text>
      {description && <Text style={styles.description}>{description}</Text>}
    </View>
    <TouchableIcon
      onPress={onPress}
      name={iconName}
      style={styles.buttonStyle}
    />
  </View>
);

SettingsWithButton.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  onPress: PropTypes.func.isRequired,
  iconName: PropTypes.string.isRequired,
};

SettingsWithButton.defaultProps = {
  description: null,
};

export default SettingsWithButton;
