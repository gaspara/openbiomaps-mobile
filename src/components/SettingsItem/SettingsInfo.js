import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import { SettingsInfoStyle as styles } from './styles';

const SettingsInfo = ({ title, description }) => (
  <View style={styles.baseContainer}>
    <Text style={styles.title}>{title}</Text>
    <Text style={styles.description}>{description}</Text>
  </View>
);

SettingsInfo.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

export default SettingsInfo;
