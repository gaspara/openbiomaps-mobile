import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import { SettingsTitleStyle as styles } from './styles';

const SettingsTitle = ({ title, customTextStyle }) => (
  <View style={styles.baseContainer}>
    <Text style={[styles.title, customTextStyle]}>{title}</Text>
  </View>
);

SettingsTitle.propTypes = {
  title: PropTypes.string.isRequired,
  customTextStyle: PropTypes.object,
};

SettingsTitle.defaultProps = {
  customTextStyle: {},
};

export default SettingsTitle;
