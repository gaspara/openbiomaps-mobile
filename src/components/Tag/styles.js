import { StyleSheet } from 'react-native';
import { Colors } from '../../colors';

export default StyleSheet.create({
  baseContainer: {
    flexDirection: 'row',
    backgroundColor: Colors.mantisGreen,
    alignItems: 'center',
    paddingVertical: 4,
    paddingHorizontal: 6,
    alignSelf: 'baseline', // To wrap the child components
    borderRadius: 4,
    marginLeft: 4,
    marginTop: 4,
  },
  text: {
    fontSize: 13,
    color: 'white',
  },
  icon: {
    marginLeft: 4,
  },
});
