import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import PropTypes from 'prop-types';
import { TouchableIcon } from '../TouchableIcon';
import styles from './styles';

const Tag = ({ label, onPress }) => (
  <TouchableOpacity style={styles.baseContainer} onPress={() => onPress(label)} >
    <Text style={styles.text}>{typeof label === 'object' ? label.value : label}</Text>
    <TouchableIcon name="clear" size={16} color="white" style={styles.icon} disabled />
  </TouchableOpacity>
);

Tag.propTypes = {
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
  onPress: PropTypes.func,
};

Tag.defaultProps = {
  onPress: () => {},
};

export default Tag;
