import { StyleSheet } from 'react-native';
import { Colors } from '../../colors';

export default StyleSheet.create({
  baseContainerStyle: {
    padding: 6,
    paddingHorizontal: 20,
    alignItems: 'center',
    backgroundColor: Colors.blue,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  textStyle: {
    fontSize: 14,
    color: 'white',
    textAlign: 'center',
  },
});
