import { MEMORY_USAGE_NORMALIZED, TOO_MUCH_MEMORY_USED, SHOULD_SEND_MEMORY_ALERT } from './types';
import { MEMORY_ALERT_PERCENTAGE, MEMORY_LOW_LIMIT } from '../Constants';

export const setMemoryUsage = (ratio, remainingMem) => (dispatch, getState) => {
  const { hasEnoughMemory, memoryUsageCritical } = getState().memory;
  if ((ratio < MEMORY_ALERT_PERCENTAGE && !hasEnoughMemory)
        || (remainingMem > MEMORY_LOW_LIMIT && memoryUsageCritical)) {
    return dispatch({ type: MEMORY_USAGE_NORMALIZED });
  }
  if (ratio > MEMORY_ALERT_PERCENTAGE) return dispatch({ type: SHOULD_SEND_MEMORY_ALERT });
  if (remainingMem < MEMORY_LOW_LIMIT) return dispatch({ type: TOO_MUCH_MEMORY_USED });
  return null;
};

