import { NavigationActions, StackActions } from 'react-navigation';

export const navigateTo = (routeName, params = {}, action = null) =>
  NavigationActions.navigate({ routeName, params, action });

export const navigateBack = (routeName = null) =>
  NavigationActions.back({ key: routeName });

export const resetStack = (routeName, params) =>
  StackActions.reset({
    key: null,
    index: 0,
    actions: [NavigationActions.navigate({ routeName, params })],
  });

export const setParams = (params, key) =>
  NavigationActions.setParams({ params, key });

export default {};
