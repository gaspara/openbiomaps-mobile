import {
  ADD_TO_OBSERVATION_LIST,
  REMOVE_FROM_OBSERVATION_LIST,
  PERFORM_IN_OBSERVATION_LIST,
  OBSERVATION_FETCH_START,
  OBSERVATION_FETCH_ERROR,
  OBSERVATION_FETCH_SUCCESS,
  SELECT_OBSERVATION,
  UPDATE_OBSERVATION,
} from './types';
import { performInDatabaseList } from './database';
import { updateShortcutObservations } from './shortcutObservations';
import { loadGameContents, fetchStart as gameFetchStart, fetchError as gameFetchError } from './game';
import client from '../config/apiClient';
import { getUserState } from '../utils';

const fetchStart = () => ({
  type: OBSERVATION_FETCH_START,
});

const fetchError = error => ({
  type: OBSERVATION_FETCH_ERROR,
  error,
});

/**
 * Load observations of the selected database
 * @param {object} selectedServer - Target server object
 * @param {object} selectedDatabase - Target database object
 */
export const loadObservations = (selectedServer, selectedDatabase) => async (
  dispatch, 
  getState,
) => {
  const { network: { status } } = getState();
  const { accessToken, refreshToken, expiresIn, requestTime } = getUserState(selectedServer);
  const { id: serverId, url: serverUrl } = selectedServer;
  const { id: databaseId, projectUrl, game } = selectedDatabase;
 
  // Dispatch fetch start if network connection is available
  if (status !== 'none' && status !== 'unknown') {
    dispatch(performInDatabaseList(serverId, databaseId, fetchStart()));
    if (game === 'on') {
      dispatch(performInDatabaseList(serverId, databaseId, gameFetchStart()));
    }
  }

  const authObject = {
    serverId,
    serverUrl,
    accessToken,
    refreshToken,
    expiresIn,
    requestTime,
  };

  try {
    const result = await client.getFormList({ 
      projectUrl,
      authObject,
    });

    const observations = result.map(({ form_id, form_name, last_mod }) => ({
      id: form_id,
      name: form_name,
      formVersion: last_mod,
    }));

    dispatch(performInDatabaseList(serverId, databaseId, {
      type: OBSERVATION_FETCH_SUCCESS,
      payload: observations,
    }));

    dispatch(updateShortcutObservations(serverId, databaseId, observations));

    if (game === 'on') {
      dispatch(loadGameContents(selectedServer, selectedDatabase, authObject));
    }
  } catch (error) {
    dispatch(performInDatabaseList(serverId, databaseId, fetchError(error.message)));
    if (game === 'on') {
      dispatch(performInDatabaseList(serverId, databaseId, gameFetchError(error.message)));
    }
  }
};

/**
 * Add empty observation object to the list
 * @param {number} databaseListId - id of the database which will contains the observation
 * @param {object} payload - the properties of the new observation
 */
export const addToObservationList = (databaseListId, payload = {}) => dispatch =>
  dispatch(performInDatabaseList(databaseListId, {
    type: ADD_TO_OBSERVATION_LIST,
    payload,
  }));

/**
 * Remove the item from the list
 * @param {number} databaseListId - id of the database which contains the observation
 * @param {number} observationId - id of the deleted observation
 */
export const removeFromObservationList = (databaseListId, observationId) => dispatch =>
  dispatch(performInDatabaseList(databaseListId, {
    type: REMOVE_FROM_OBSERVATION_LIST,
    id: observationId,
  }));

/**
 * Perform an action in the observation list
 * @param {number} serverId - id of the target server
 * @param {number } databaseId - id of the target database
 * @param {number} observationId - id of the target observation
 * @param {object} action - the action what we want to perform in the observation list
 */
export const performInObservationList = (
  serverId, 
  databaseId, 
  observationId, 
  action,
) => (dispatch) => {
  dispatch(performInDatabaseList(serverId, databaseId, {
    type: PERFORM_IN_OBSERVATION_LIST,
    id: observationId,
    action,
  }));
};

export const selectObservation = (serverId, databaseId, observationId) => (dispatch) => {
  dispatch(performInDatabaseList(serverId, databaseId, {
    type: SELECT_OBSERVATION,
    id: observationId,
    date: new Date(),
  }));
};

export const sliceMeasurementList = (observation, maxMeasurementCount = 100) => {
  const { measurements, ...newObservation } = observation;
  if (measurements.length >= maxMeasurementCount) {
    const isDeletable = measurement => measurement.isSynced && !measurement.errors;
    const lengthDiff = measurements.length - maxMeasurementCount;
    const deletableMeasurements = measurements.filter(isDeletable);
    measurements.sort((a, b) => {
      if (isDeletable(a) && !isDeletable(b)) {
        return 1;
      }
      if (isDeletable(b) && !isDeletable(a)) {
        return -1;
      }
      return b.id - a.id;
    });
    const toBeDeletedCount = Math.max(Math.min(lengthDiff, deletableMeasurements.length), 0);
    measurements.splice(measurements.length - toBeDeletedCount);
  }
  newObservation.measurements = measurements;
  return {
    type: UPDATE_OBSERVATION,
    payload: newObservation,
  };
};
