import {
  SET_GPS_ERROR,
  SET_GPS_POSITION,
  START_GPS_RECORDING,
  STOP_GPS_RECORDING,
  SUBSCRIBE_TO_GPS,
  UNSUBSCRIBE_FROM_GPS,
} from './types';

export const startGpsRecording = () => ({
    type: START_GPS_RECORDING,
});

export const stopGpsRecording = () => ({
    type: STOP_GPS_RECORDING,
});

export const subscribeToGps = (observer) => ({
    type: SUBSCRIBE_TO_GPS,
    payload: {
        observer,
    },
});

export const unsubscribeFromGps = (observer) => ({
    type: UNSUBSCRIBE_FROM_GPS,
    payload: {
        observer,
    },
});

export const setGpsPosition = ({latitude, longitude, accuracy, timestamp}) => ({
    type: SET_GPS_POSITION,
    payload: {
        position: {
            latitude,
            longitude,
            accuracy,
            timestamp
        },
    },
});

export const setGpsError = ({code, message}) => ({
    type: SET_GPS_ERROR,
    payload: {
        error: {
            code,
            message,
        },
    },
});
