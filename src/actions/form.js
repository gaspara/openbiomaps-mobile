import {
  FORM_DATA_FETCH_SUCCESS,
  FORM_DATA_FETCH_START,
  FORM_DATA_FETCH_ERROR,
  END_SESSION,
  REMOVE_FROM_SHORTCUT_OBSERVATION_LIST,
  FORM_DATA_SAVE_STICKINESSES,
  FORM_DATA_REMOVE_STICKINESSES,
  FORM_DATA_SAVE_LAST_PICKED_COLUMN_VALUE,
  FORM_DATA_PURGE_LAST_PICKED_COLUMN_VALUE,
} from './types';
import client from '../config/apiClient';
import { getUserState } from '../utils';
import { performInObservationList } from './observation';
import { ERROR } from '../Constants';

const fetchStart = () => ({
  type: FORM_DATA_FETCH_START,
});

const fetchError = (error) => ({
  type: FORM_DATA_FETCH_ERROR,
  error,
});

export const saveStickinesses =
  (serverId, databaseId, observationId, stickinesses) => (dispatch) => {
    dispatch(
      performInObservationList(serverId, databaseId, observationId, {
        type: FORM_DATA_SAVE_STICKINESSES,
        payload: stickinesses,
      }),
    );
  };

export const removeStickinesses =
  (serverId, databaseId, observationId) => (dispatch) => {
    dispatch(
      performInObservationList(serverId, databaseId, observationId, {
        type: FORM_DATA_REMOVE_STICKINESSES,
      }),
    );
  };

export const saveLastPickedColumnValues =
  (serverId, databaseId, observationId, data) => (dispatch) => {
    dispatch(
      performInObservationList(serverId, databaseId, observationId, {
        type: FORM_DATA_SAVE_LAST_PICKED_COLUMN_VALUE,
        payload: data,
      }),
    );
  };

export const purgeLastPickedColumnValues =
  (serverId, databaseId, observationId) => (dispatch) => {
    dispatch(
      performInObservationList(serverId, databaseId, observationId, {
        type: FORM_DATA_PURGE_LAST_PICKED_COLUMN_VALUE,
      }),
    );
  };

export const loadFormData =
  (selectedServer, selectedDatabase, selectedObservation) =>
  async (dispatch, getState) => {
    const {
      network: { status },
    } = getState();
    const { accessToken, refreshToken, expiresIn, requestTime } =
      getUserState(selectedServer);
    const { id: serverId, url: serverUrl } = selectedServer;
    const { id: databaseId, name: projectName, projectUrl } = selectedDatabase;
    const { id: observationId } = selectedObservation;

    // Dispatch fetch start if network connection is available
    if (status !== 'none' && status !== 'unknown')
      dispatch(
        performInObservationList(
          serverId,
          databaseId,
          observationId,
          fetchStart(),
        ),
      );

    try {
      const result = await client.getFormData({
        projectName,
        projectUrl,
        id: observationId,
        authObject: {
          serverId,
          serverUrl,
          accessToken,
          refreshToken,
          expiresIn,
          requestTime,
        },
      });

      const { form_data, form_header } = result;
      const {
        login_email: loginEmail,
        login_name: loginName,
        permanent_sample_plots,
        boldyellow,
        num_ind: numInd = '',
        observationlist_mode: observationListMode = 'false',
        observationlist_time_length: listTime = null,
        tracklog_mode: trackLogMode = '',
        periodic_notification_time: periodicNotificationTime = null,
      } = form_header;

      const boldYellow = boldyellow && boldyellow.filter((item) => item);

      const formData = form_data.map(
        ({
          api_params,
          default_value,
          short_name,
          spatial_limit,
          ...restParams
        }) => ({
          apiParams: api_params,
          defaultValue: default_value,
          shortName: short_name,
          spatialLimit: spatial_limit,
          ...restParams,
        }),
      );

      const permanentSamplePlotInfos = permanent_sample_plots
        ? permanent_sample_plots.filter((p) => !!p.id && !!p.name)
        : [];

      dispatch(
        performInObservationList(serverId, databaseId, observationId, {
          type: FORM_DATA_FETCH_SUCCESS,
          payload: {
            dataTypes: formData,
            permanentSamplePlotInfos,
            boldYellow,
            loginEmail,
            loginName,
            numInd,
            observationListMode,
            listTime,
            trackLogMode,
            periodicNotificationTime,
          },
        }),
      );
    } catch (error) {
      let errorMessage = null;
      // If the response cannot transformed into JSON object,
      // it means that form is not sent by the api, aka. data
      // is not available. Form that is no longer available on the server
      // needs to be deleted from the shortcutObservation array.
      if (error.message === ERROR.FORM_ACCESS_DENIED) {
        errorMessage = ERROR.FORM_UNAVAILABLE;
        dispatch({
          type: REMOVE_FROM_SHORTCUT_OBSERVATION_LIST,
          serverId,
          databaseId,
          observationId,
        });

        // Init active session
        if (selectedObservation.session) {
          dispatch(
            performInObservationList(serverId, databaseId, observationId, {
              type: END_SESSION,
            }),
          );
        }
      } else {
        errorMessage = error.message;
      }

      dispatch(
        performInObservationList(
          serverId,
          databaseId,
          observationId,
          fetchError(errorMessage),
        ),
      );
    }
  };
