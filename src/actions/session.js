import 'react-native-get-random-values';
import { v4 as uuidv4 } from 'uuid';
import Toast from 'react-native-simple-toast';
import { START_SESSION, END_SESSION, ADD_TRACKLOG_TO_SESSION } from './types';
import { performInObservationList } from './observation';
import { createMeasurement, updateMeasurement } from './measurement';
import { saveSessionTrackLog } from './tracklog';
import { APP_VERSION, SESSION_TRACKLOG_META_INFO } from '../Constants';
import { getSessionByPath, getMeasurementsBySession, getAllMeasurementsBySession, getSessionMetaRecord } from '../utils';
import I18n from '../i18n';

/**
 * Start the session by adding a session object to the observation reducer
 * @param {string}  serverId                - REQ: Id of the current server
 * @param {string}  databaseId              - REQ: Id of the current database
 * @param {string}  observationId           - REQ: Id of the current observation
 * @param {number}  startedAt               - REQ: Timestamp of the start time
 * @param {number}  listTime                - REQ: How much time the user has to finish the list (seconds)
 */
export const startSession = (serverId, databaseId, observationId, startedAt, listTime, shouldRecordTrackLog = false) => (dispatch) => {
  dispatch(performInObservationList(serverId, databaseId, observationId, {
    type: START_SESSION,
    id: uuidv4(),
    startedAt,
    mustFinishAt: (listTime && listTime !== '0') ? startedAt + (listTime * 1000) : null,
    shouldRecordTrackLog,
  }));
};

/**
 * Update all measurements belonging to the same session with the common list fields.
 * @param {string}  serverId                - REQ: Id of the current server
 * @param {string}  databaseId              - REQ: Id of the current database
 * @param {string}  observationId           - REQ: Id of the current observation
 * @param {boolean} sessionId               - REQ: Id of the session (observation list)
 * @param {object}  commonFields            - REQ: Common observation list fields
 */
const updateMeasurementsWithCommonFields = (serverId, databaseId, observationId, sessionId, commonFields) => (dispatch, getState) => {
  const { servers: { data: serverList } } = getState();
  const measurements = getMeasurementsBySession(serverId, databaseId, observationId, sessionId, serverList);

  measurements.forEach((measurement) => {
    dispatch(updateMeasurement(serverId, databaseId, observationId, {
      ...measurement,
      data: { 
        ...commonFields,
        ...measurement.data,
      },
    }));
  });
};

/* Update MetaData => measurementsNum of a metaRecord after a session is finished and a null record was created
 *
 * */
export const updateMeasurementsMetaNum = (serverId, databaseId, observationId, sessionId) => (dispatch, getState) => {
  const { servers: { data: serverList } } = getState();
  const measurements = getAllMeasurementsBySession(serverId, databaseId, sessionId, serverList);
  const num = measurements.length;

  measurements.forEach((measurement) => {
    if (!measurement.data) {
      dispatch(updateMeasurement(serverId, databaseId, observationId, {
        ...measurement,
        meta: {
          ...measurement.meta,
          measurementsNum: num,
        },
      }));
    }
  });
};

/**
 * Finish an observation list recording session.
 * @param {string}  serverId                - REQ: Id of the current server
 * @param {string}  databaseId              - REQ: Id of the current database
 * @param {string}  observationId           - REQ: Id of the current observation
 * @param {boolean} shouldRecordMetaRecord  - OPT: Determine whether a session related meta record should be saved or not. (Meta record contains no data only metadata.)
 * @param {object}  commonFields            - OPT: Common observation list fields
 * @param {object}  nullRecordData          - OPT: Measurement object of manually created null record
 */
export const endSession = (serverId, databaseId, observationId, shouldRecordMetaRecord, commonFields, nullRecordData) => (dispatch, getState) => {
  const { servers: { data: serverList } } = getState();
  const {
    id: sessionId,
    startedAt,
    measurementsNum,
    sessionTrackLog,
    shouldRecordTrackLog,
  } = getSessionByPath(serverId, databaseId, observationId, serverList);
    // Assemble measurement object of null record, which indicates the start and
    // the end of a session. Null record should be handled as a real measurement,
    // however, it is hidden from the user and handled in the background.
    // Null record is important in case of those sessions druing which no data was recorded
    // (e.g. nothing was observerd during 10 minutes) and the user finish the session without
    // a real measurement.
  const measurement = {
    id: nullRecordData?.id || uuidv4(),
    data: nullRecordData?.data, // discard és commonfields-nél null, null rekord felvételnél a measurement, de az hiddenValue-s ad hozzá valamit????
    meta: {
      sessionId,
      startedAt,
      finishedAt: +new Date(),
      appVersion: APP_VERSION,
      // ObservationListMetaRecord is true if there aren't any recorded measurements in the list (aka. recorded empty list).
      // These kind of null records are handled differenty on the server than those ones that contains
      // a certain number of measurements, where ObservationListMetaRecord is false.
      observationListMetaRecord: true,
      measurementsNum,
      sessionTrackLog,
      recordedSessionTrackLog: shouldRecordTrackLog,
    },
  };

  if (shouldRecordMetaRecord) {
    // ha nem discardEmptyObservationList, akkor mindenképpen teljesül, a measurement.data üres, amikor Meta rekordot rögzít a listához
    dispatch(createMeasurement(serverId, databaseId, observationId, measurement));
  }

  if (commonFields) {
    dispatch(updateMeasurementsWithCommonFields(serverId, databaseId, observationId, sessionId, commonFields));
  }

  dispatch(performInObservationList(serverId, databaseId, observationId, {
    type: END_SESSION,
  }));
  return measurement.meta.sessionId;
};

/**
 * Add the collected trackLogs to the current session in the observation
 * @param {string}  serverId                - REQ: Id of the current server
 * @param {string}  databaseId              - REQ: Id of the current database
 * @param {string}  observationId           - REQ: Id of the current observation
 * @param {array}  sessionTrackLog          - REQ: an array of tracklogs: {latitude, longitude, timestamp}
 */
export const addTrackLogToOpenedSession = (serverId, databaseId, observationId, sessionTrackLog) => (dispatch) => {
  dispatch(performInObservationList(serverId, databaseId, observationId, {
    type: ADD_TRACKLOG_TO_SESSION,
    sessionTrackLog,
  }));
};

/**
 * Add the collected trackLogs to the closed sessions metarecord measurement in the measurementlist.
 * @param {Object}  session                - REQ: the session which should be edited
 * */
export const addTrackLogToClosedSession = ({
  serverId,
  serverName,
  databaseId,
  databaseName,
  projectUrl,
  observationId,
  observationName,
  sessionId,
  trackLogArray,
  startTime,
}) => (dispatch, getState) => {
  const { servers: { data: serverList } } = getState();
  const measurement = getSessionMetaRecord(serverId, databaseId, observationId, sessionId, serverList);

  if (measurement) {
    const newSessionTrackLog = measurement.meta.sessionTrackLog?.length
      ? [...measurement.meta.sessionTrackLog, ...trackLogArray]
      : trackLogArray;

    if (!newSessionTrackLog.length) Toast.show(I18n.t('session_tracklog_empty'), Toast.LONG);
    else Toast.show(I18n.t('session_tracklog_recorded', { length: newSessionTrackLog.length }), Toast.LONG);

    if (!newSessionTrackLog.length) Toast.show(I18n.t('session_tracklog_empty'), Toast.LONG);
    else Toast.show(I18n.t('session_tracklog_recorded', { length: newSessionTrackLog.length }), Toast.LONG);

    dispatch(updateMeasurement(serverId, databaseId, observationId, {
      ...measurement,
      meta: {
        ...measurement.meta,
        sessionTrackLog: [],
        trackLogInfo: SESSION_TRACKLOG_META_INFO.NOT_SET,
      },
    }));

    dispatch(saveSessionTrackLog({
      selectedDatabases: [{
        serverName,
        serverId,
        databaseName,
        databaseId,
        projectUrl,
        observationName,
        observationId,
      }],
      startTime,
      trackLogArray: newSessionTrackLog,
      sessionId,
    }));
  }
};

/**
 * Searches for the metarecord related to the tracklod via the sessionId, and sets its trackLogInfo field 
 * @param {string} trackLogInfo 
 * @param {string} sessionId 
 * @param {string} serverId 
 * @param {string} databaseId 
 * @param {string} observationId 
 * @returns 
 */
export const setSessionTrackLogInfoInMetaRecord = (
  trackLogInfo,
  sessionId,
  serverId,
  databaseId,
  observationId,
) => (dispatch, getState) => {
  if (!serverId || !databaseId || !observationId) return;

  const { servers: { data: serverList } } = getState();
  const measurement = getSessionMetaRecord(serverId, databaseId, observationId, sessionId, serverList);
  if (!measurement || measurement.isSynced) return;

  dispatch(updateMeasurement(serverId, databaseId, observationId, {
    ...measurement,
    meta: {
      ...measurement.meta,
      trackLogInfo,
    },
  }));
};
