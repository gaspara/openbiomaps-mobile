import { SELECT_DATABASE_FOR_MAP, REMOVE_SELECTED_DATABASE_FOR_MAP } from './types';

export const selectDatabaseForMap = payload => (dispatch) => {  
  dispatch({
    type: SELECT_DATABASE_FOR_MAP,
    payload,
  });
};

export const removeSelectedDatabaseForMap = payload => (dispatch) => {  
  dispatch({
    type: REMOVE_SELECTED_DATABASE_FOR_MAP,
    payload,
  });
};
