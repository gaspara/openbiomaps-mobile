import { GAME_FETCH_SUCCESS, QUIZ_PASSED, GAME_CONTENT_FETCH_START, GAME_CONTENT_FETCH_SUCCESS, GAME_CONTENT_FETCH_ERROR } from './types';
import { performInObservationList } from './observation';
import client from '../config/apiClient';
import { asyncForEach } from '../utils';
import { performInDatabaseList } from './database';

export const fetchStart = () => ({
  type: GAME_CONTENT_FETCH_START,
});

export const fetchSuccess = () => ({
  type: GAME_CONTENT_FETCH_SUCCESS,
});

export const fetchError = error => ({
  type: GAME_CONTENT_FETCH_ERROR,
  error,
});

const loadTrainingQuestions = async data =>
  (await client.getTrainingQuestions(data)).map(({
    training_id,
    answers,
    ...data
  }) => ({
    trainingId: training_id,
    answers: answers.map(({ Answer, isRight }) => ({ text: Answer, isRight: isRight === 'true' })),
    ...data,
  }));

export const loadGameContents = (server, database, authObject) => async (dispatch) => {
  try {
    const res = await client.getTrainings({
      projectName: database.name,
      projectUrl: database.projectUrl,
      authObject,
    });

    const trainings = res.map(({
      enabled,
      task_description,
      project_table,
      form_id,
      ...data
    }) => ({
      isEnabled: enabled === 't',
      taskDescription: task_description,
      projectTable: project_table,
      formId: form_id,
      ...data,
    }));

    const trainingResults = await client.trainingResults({
      projectName: database.name,
      projectUrl: database.projectUrl,
      authObject,
    });

    asyncForEach(trainings, async (training, index, array) => {
      try {
        const questions = await loadTrainingQuestions({
          projectName: database.name,
          trainingId: training.id,
          projectUrl: database.projectUrl,
          authObject,
        });

        dispatch(performInObservationList(server.id, database.id, training.formId, {
          type: GAME_FETCH_SUCCESS,
          payload: {
            ...training,
            questions,
            // possible values are:
            // 1 - validated by backend, 0 - waiting for validation, -1 not completed yet
            isValidatedByBackend: trainingResults[training.formId] === 1,
          },
        }));

        if (index === array.length - 1) {
          dispatch(performInDatabaseList(server.id, database.id, fetchSuccess()));
        }
      } catch (error) {
        dispatch(performInDatabaseList(server.id, database.id, fetchError(error.message)));
      }
    });
  } catch (error) {
    dispatch(performInDatabaseList(server.id, database.id, fetchError(error.message)));
  }
};

export const quizPassed = (serverId, databaseId, observationId) => (dispatch) => {
  dispatch(performInObservationList(serverId, databaseId, observationId, {
    type: QUIZ_PASSED,
  }));
};
