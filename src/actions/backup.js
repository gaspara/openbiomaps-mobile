import RNRestart from 'react-native-restart';
import moment from 'moment';
import DocumentPicker from 'react-native-document-picker';
import Toast from 'react-native-simple-toast';
import { FileSystem } from 'react-native-unimodules';
import {
  BACKUP_ERROR,
  ADD_BACKUP,
  REMOVE_BACKUPS,
  LOAD_BACKUP_LIST,
} from './types';
import { persistor } from '../config/setupStore';
import I18n from '../i18n';
import { ERROR } from '../Constants';
import { showAlertDialog } from '../components/AlertDialog';
import {
  hasExternalStoragePermission,
  requestExternalSAFDirPermission,
} from '../utils/permissonRequests';
import { changeSettingsData } from './settings';
import { Platform } from 'react-native';

const STORAGE_PATH = `${FileSystem.documentDirectory}persistStore/persist-root`;
const BACKUP_DIR_PATH = `${FileSystem.documentDirectory}persistStore/backup`;
const BACKUP_PATH = `${BACKUP_DIR_PATH}/persist-root`;
const BACKUP_NUM = 5; // TODO: would be nice if it should be changed by the user in the settings
const EXTERNAL_BACKUP_NUM = 10;

const { StorageAccessFramework } = FileSystem;

const loadBackupList = (backupIds) => ({
  type: LOAD_BACKUP_LIST,
  payload: backupIds,
});

const addBackup = (backupId) => ({
  type: ADD_BACKUP,
  payload: backupId,
});

const removeBackups = (backupIdArray) => ({
  type: REMOVE_BACKUPS,
  backupIdArray,
});

const backupError = (error) => ({
  type: BACKUP_ERROR,
  error,
});

export const restoreBackup = (backupId) => async (dispatch) => {
  try {
    await FileSystem.copyAsync({
      from: `${BACKUP_PATH}_${backupId}`,
      to: STORAGE_PATH,
    });
    RNRestart.Restart();
  } catch (error) {
    dispatch(backupError(error));
  }
};

const restoreExternalBackup = (filePath) => async (dispatch) => {
  try {
    const file = await FileSystem.readAsStringAsync(filePath);
    JSON.parse(file);
    await FileSystem.writeAsStringAsync(STORAGE_PATH, file);
    RNRestart.Restart();
  } catch (error) {
    console.warn('restore external backup error: ', { error });
    if (error.message?.includes(ERROR.WRONG_CONTENT_URI_FILE_PATH_ERROR)) {
      Toast.show(I18n.t('wrong_content_uri'), Toast.LONG);
    } else {
      Toast.show(I18n.t('invalid_backup_file'), Toast.LONG);
    }
    dispatch(backupError(error.message));
  }
};

export const pickAndRestoreExternalBackup = () => async (dispatch) => {
  try {
    const hasPermission = await hasExternalStoragePermission();
    if (!hasPermission) return;
    const documentPickerResult = await DocumentPicker.pickSingle({
      presentationStyle: 'formSheet',
      copyTo: 'cachesDirectory',
    });
    const filePath = documentPickerResult.fileCopyUri;
    dispatch(restoreExternalBackup(filePath));
  } catch (error) {
    if (!DocumentPicker.isCancel(error)) {
      dispatch(backupError(error.message));
    }
  }
};

const isValidExternalBackupFile = (file, localBackups = []) => {
  if (file.isDirectory) {
    return false;
  }
  const splittedByColon = file.fileName.split(':');
  const splittedFileName =
    splittedByColon[splittedByColon.length - 1].split('_');
  if (
    splittedFileName?.length === 2 &&
    splittedFileName[0] === 'obm' &&
    (localBackups?.includes(splittedFileName[1]) ||
      localBackups?.includes(parseInt(splittedFileName[1], 10)))
  ) {
    return true;
  }
  return false;
};

// The generated filename is: obm_123435 (plus .json at certain android version)
const generateValidBackupFileName = (id) => `obm_${id}`;

const checkExternalBackupsExists = async (externalBackupDirectory) => {
  if (!externalBackupDirectory) return null;
  try {
    const files = (
      await Promise.all(
        (
          await StorageAccessFramework.readDirectoryAsync(
            externalBackupDirectory,
          )
        ).map(async (item) => {
          const info = await FileSystem.getInfoAsync(item);
          return {
            ...info,
            fileName: decodeURIComponent(info.uri).slice(
              decodeURIComponent(info.uri).lastIndexOf('/') + 1,
            ),
          };
        }),
      )
    ).filter((item) => !item.isDirectory && item.fileName.match(/^obm_/));

    return !!files.length;
  } catch (error) {
    console.warn('error while checking external backup ', { error });
    return null;
  }
};

const checkStateAndOfferBackup =
  (localBackups) => async (dispatch, getState) => {
    const {
      servers: { data: serverList },
      shortcutObservations,
      settings: { externalBackupDirectory },
    } = getState();

    const extBackupDirUri = await requestExternalSAFDirPermission(
      externalBackupDirectory,
      'Backup',
    );
    if (extBackupDirUri && extBackupDirUri !== externalBackupDirectory)
      dispatch(changeSettingsData('externalBackupDirectory', extBackupDirUri));

    const localBackupExist = localBackups.length !== 0;
    const hasPermission = await hasExternalStoragePermission();

    if (
      !shortcutObservations?.length &&
      !serverList.some(({ databases }) =>
        databases?.data.some((db) => db.lastSelected),
      )
    ) {
      // no prevobs, no databases from each server
      const confirmDialog = (backup, restore) =>
        showAlertDialog({
          title: I18n.t('backup_confirm_title'),
          positiveLabel: I18n.t('restore'),
          onPositiveBtnPress: () => {
            dispatch(restore(backup));
          },
          showCancelButton: true,
        });

      if (localBackupExist) {
        showAlertDialog({
          title: I18n.t('default_alert_title'),
          message: I18n.t('possible_crash_backup_offer'),
          onPositiveBtnPress: () => {
            confirmDialog(localBackups[localBackups.length - 1], restoreBackup);
          },
          showCancelButton: true,
          positiveLabel: I18n.t('restore'),
        });
      } else if (hasPermission) {
        const externalBackup = await checkExternalBackupsExists(
          extBackupDirUri,
        );
        if (externalBackup) {
          showAlertDialog({
            title: I18n.t('default_alert_title'),
            message: I18n.t('possible_crash_external_backup_offer'),
            onPositiveBtnPress: () => {
              dispatch(pickAndRestoreExternalBackup());
            },
            showCancelButton: true,
            positiveLabel: I18n.t('select'),
          });
        }
      }
    }
  };

export const getBackupList = () => async (dispatch, getState) => {
  const { backups } = getState().backup;

  if (backups.length) {
    return;
  }

  try {
    const { exists } = await FileSystem.getInfoAsync(BACKUP_DIR_PATH);
    if (!exists) {
      dispatch(checkStateAndOfferBackup([]));
      return;
    }

    const result = await FileSystem.readDirectoryAsync(BACKUP_DIR_PATH);
    const backupIds = result
      .map((item) => parseInt(item.split('_')[1], 10))
      .sort((a, b) => a - b);
    dispatch(loadBackupList(backupIds));

    dispatch(checkStateAndOfferBackup(backupIds));
  } catch (error) {
    dispatch(backupError(error));
  }
};

export const createBackup = (successMessage) => async (dispatch, getState) => {
  const { backups } = getState().backup;
  const { isBackupEnabled } = getState().settings;
  const createdAt = moment().toDate();
  const backupId = moment(createdAt).unix(); // backupId === current timestamp
  if (!isBackupEnabled) {
    return;
  }

  try {
    // Write all states to disk immediatelly
    await persistor.flush();

    const { exists } = await FileSystem.getInfoAsync(BACKUP_DIR_PATH);
    // Create backup folder if doesn't exist yet.
    if (!exists) {
      await FileSystem.makeDirectoryAsync(BACKUP_DIR_PATH);
    }

    // Delete the oldest backup if the number of stored files reaches the specified number.
    if (backups.length >= BACKUP_NUM) {
      const backupsToDelete =
        BACKUP_NUM !== 1 ? backups.slice(0, -BACKUP_NUM + 1) : backups;
      const delpromises = backupsToDelete.map((id) =>
        FileSystem.deleteAsync(`${BACKUP_PATH}_${id}`),
      );
      await Promise.all(delpromises);
      dispatch(removeBackups(backupsToDelete));
    }

    // Add new backup file by copying the flushed storage into the backup dir
    await FileSystem.copyAsync({
      from: `${STORAGE_PATH}`,
      to: `${BACKUP_PATH}_${backupId}`,
    });
    dispatch(addBackup(backupId));

    if (Platform.OS == 'android') {
      const { externalBackupDirectory } = getState().settings;

      // External backups
      const hasPermission = await hasExternalStoragePermission(
        I18n.t('external_permission_denied_backup_creation'),
      );
      if (!hasPermission) {
        dispatch(backupError(ERROR.PERMISSION_DENIED));
        return;
      }
      const extBackupDirUri = await requestExternalSAFDirPermission(
        externalBackupDirectory,
        'Backup',
      );
      if (extBackupDirUri) {
        dispatch(
          changeSettingsData('externalBackupDirectory', extBackupDirUri),
        );
        // get the directory's files that are surely valid obm files sorted in alphabetical order
        const externalBackups = await Promise.all(
          (
            await StorageAccessFramework.readDirectoryAsync(
              externalBackupDirectory,
            )
          ).map(async (item) => {
            const info = await FileSystem.getInfoAsync(item);
            return {
              ...info,
              fileName: decodeURIComponent(info.uri).slice(
                decodeURIComponent(info.uri).lastIndexOf('/') + 1,
              ),
            };
          }),
        );
        const filteredExternalBacups = externalBackups
          .filter(
            (item) => !item.isDirectory && item.fileName.startsWith('obm_'),
          )
          .sort((a, b) => {
            const timeStampA = a.fileName.split('_')[1].split('.')[0];
            const timeStampB = b.fileName.split('_')[1].split('.')[0];
            return timeStampA - timeStampB;
          });

        // const filteredExternalBackupsReserve = filteredExternalBacups.reverse();
        if (filteredExternalBacups.length >= EXTERNAL_BACKUP_NUM) {
          const ebackupsToDelete =
            EXTERNAL_BACKUP_NUM !== 1
              ? filteredExternalBacups.slice(0, -EXTERNAL_BACKUP_NUM + 1)
              : filteredExternalBacups;

          const edelpromises = ebackupsToDelete.map((file) =>
            StorageAccessFramework.deleteAsync(file.uri),
          );
          await Promise.all(edelpromises);
          dispatch(removeBackups(ebackupsToDelete));
        }
        // Write external backup file like into persist backup
        const file = await FileSystem.readAsStringAsync(STORAGE_PATH);
        const newBackupUri = await StorageAccessFramework.createFileAsync(
          extBackupDirUri,
          generateValidBackupFileName(backupId),
          'application/json',
        );
        await FileSystem.writeAsStringAsync(newBackupUri, file);
      }
    }
    if (successMessage) Toast.show(successMessage, Toast.LONG);
  } catch (error) {
    if (error.message === ERROR.ROOT_DIR_SET) {
      dispatch(changeSettingsData('externalBackupDirectory', null));
    }
    dispatch(backupError(error));
  }
};
