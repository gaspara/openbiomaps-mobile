import { TOPLIST_FETCH_SUCCESS, TOPLIST_FETCH_START, TOPLIST_FETCH_ERROR } from './types';
import { performInDatabaseList } from './database';
import client from '../config/apiClient';
import { getUserState } from '../utils';

const fetchStart = () => ({
  type: TOPLIST_FETCH_START,
});

const fetchError = error => ({
  type: TOPLIST_FETCH_ERROR,
  error,
});

export const loadToplist = (selectedServer, selectedDatabase) => async (dispatch, getState) => {
  const { network: { status } } = getState();
  const { accessToken, refreshToken, expiresIn, requestTime } = getUserState(selectedServer);
  const { id: serverId, url: serverUrl } = selectedServer;
  const { id: databaseId, name: projectName, projectUrl } = selectedDatabase;

  // Dispatch fetch start if network connection is available
  if (status !== 'none' && status !== 'unknown') dispatch(performInDatabaseList(serverId, databaseId, fetchStart()));

  try {
    const res = await client.trainingToplist({
      projectName,
      projectUrl,
      authobject: {
        serverId,
        serverUrl,
        accessToken,
        refreshToken,
        expiresIn,
        requestTime,
      },
    });

    const toplist = Object.entries(res).map(([name, value]) => ({
      name,
      score: Number(value.max).toFixed(3),
    }));
    toplist.sort((a, b) => b.score - a.score);

    dispatch(performInDatabaseList(serverId, databaseId, {
      type: TOPLIST_FETCH_SUCCESS,
      payload: toplist,
    }));
  } catch (error) {
    dispatch(performInDatabaseList(serverId, databaseId, fetchError(error.message)));
  }
};
