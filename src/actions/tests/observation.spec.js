import configureStore from 'redux-mock-store';
import { UPDATE_OBSERVATION } from '../types';
import { mock, allDeletable, allDeletableExpected, deleteOnlyOne, deleteOnlyOneExpected } from './mockData';

import { sliceMeasurementList } from '../../actions';

const middlewares = [];
const mockStore = configureStore(middlewares);

describe('slice measurement list action', () => {
  it('should remove oldest synced measurements', () => {
    const initialState = mock(allDeletable);
    const store = mockStore(initialState);
    const { observations } = store.getState().databases[0];
    store.dispatch(sliceMeasurementList(observations[0], 5));

    const actions = store.getActions();
    const expected = {
      type: UPDATE_OBSERVATION,
      payload: {
        ...observations[0],
        measurements: allDeletableExpected,
      },
    };
    expect(actions).toEqual([expected]);
  });

  it('should keep not synced measurements', () => {
    const initialState = mock(deleteOnlyOne);
    const store = mockStore(initialState);
    const { observations } = store.getState().databases[0];
    store.dispatch(sliceMeasurementList(observations[0], 5));

    const actions = store.getActions();
    const expected = {
      type: UPDATE_OBSERVATION,
      payload: {
        ...observations[0],
        measurements: deleteOnlyOneExpected,
      },
    };
    expect(actions).toEqual([expected]);
  });
});
