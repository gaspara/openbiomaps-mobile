/* eslint-disable import/no-cycle */

import { FileSystem } from 'react-native-unimodules';
import Toast from 'react-native-simple-toast';

import { showAlertDialog } from '../components/AlertDialog';
import { ERROR, SESSION_TRACKLOG_META_INFO } from '../Constants';
import { createGpxFromTrackLog, formatToFilename } from '../utils';
import { hasExternalStoragePermission, requestExternalSAFDirPermission } from '../utils/permissonRequests';
import client from '../config/apiClient';
import I18n from '../i18n';

import { addTrackLogToClosedSession, addTrackLogToOpenedSession, setSessionTrackLogInfoInMetaRecord } from './session';
import { syncMeasurements } from './measurement';
import { changeSettingsData } from './settings';
import {
  START_TRACKLOG_RECORDING,
  STOP_TRACKLOG_RECORDING,
  SAVE_CURRENT_TRACKLOG,
  SAVE_SESSION_TRACKLOG,
  DELETE_TRACKLOG,
  DELETE_ALL_SYNCED_TRACKLOG,
  START_SYNC_FOR_TRACKLOG_LIST,
  STOP_SYNC_FOR_TRACKLOG_LIST,
  SYNC_SUCCESS_TRACKLOG_LIST,
  FETCH_SYNC_ERROR_TRACKLOG_LIST,
  INIT_CURRENT_TRACKLOG,
  DELETE_CURRENT_TRACKLOG,
  ADD_TO_CURRENT_TRACKLOG,
  EDIT_CURRENT_TRACKLOG,
  ADD_POINT_TO_GLOBAL_TRACKLOG,
  ADD_POINT_TO_SESSION_TRACKLOGS,
  ADD_NEW_SESSIONS_TO_TRACKLOGS,
  EDIT_SESSION_TRACKLOGS,
} from './types';
import { Platform } from 'react-native';

const { StorageAccessFramework } = FileSystem;

export const startTrackLogRecording = () => (dispatch) => {
  dispatch({ type: START_TRACKLOG_RECORDING });
};

export const stopTrackLogRecording = () => (dispatch) => {
  dispatch({ type: STOP_TRACKLOG_RECORDING });
};

export const initCurrentTrackLog = (trackLog) => (dispatch) => {
  dispatch({
    type: INIT_CURRENT_TRACKLOG,
    payload: {
      trackLog,
    },
  });
};

export const deleteCurrentTrackLog = () => (dispatch) => {
  dispatch({ type: DELETE_CURRENT_TRACKLOG });
};

export const addToCurrentTrackLog = (point) => (dispatch) => {
  dispatch({
    type: ADD_TO_CURRENT_TRACKLOG,
    payload: {
      point: {
        ...point,
        timestamp: Date.now(),
      },
    },
  });
};

export const addPointToGlobalTrackLog = (point) => ({
  type: ADD_POINT_TO_GLOBAL_TRACKLOG,
  payload: {
    point: {
      ...point,
      timestamp: Date.now(),
    },
  },
});

export const addPointToSessionTrackLogs = (point) => ({
  type: ADD_POINT_TO_SESSION_TRACKLOGS,
  payload: {
    point: {
      ...point,
      timestamp: Date.now(),
    },
  },
});

export const editCurrentTrackLog = (trackLog) => (dispatch) => {
  dispatch({
    type: EDIT_CURRENT_TRACKLOG,
    payload: {
      trackLog,
    },
  });
};

export const saveCurrentGlobalTrackLog = () => (dispatch) => {
  dispatch({
    type: SAVE_CURRENT_TRACKLOG,
  });
};

export const saveSessionTrackLog = (trackLog) => (dispatch) => {
  dispatch({
    type: SAVE_SESSION_TRACKLOG,
    payload: {
      trackLog,
    },
  });
};

/**
 * Before deleting the tracklog, checks whether it is a session tracklog or not, and if it is,
 * the session's metarecord's tracklogInfo field should be updated
 * @param {string} trackLogId
 * @param {string} sessionId 
 * @returns 
 */
export const deleteUnsyncedTrackLog = (trackLogId, { sessionId, selectedDatabases } = {}) => (dispatch) => {
  if (sessionId && selectedDatabases.length === 1) {
    const [{ serverId, databaseId, observationId }] = selectedDatabases;
    dispatch(setSessionTrackLogInfoInMetaRecord(SESSION_TRACKLOG_META_INFO.DELETED, sessionId, serverId, databaseId, observationId));
  }
  dispatch({
    type: DELETE_TRACKLOG,
    payload: {
      trackLogId,
    },
  });
};

/**
 * Deletes a synced tracklog without doing anything with the session's metarecord
 * @param {string} trackLogId 
 * @returns 
 */
export const deleteSyncedTrackLog = (trackLogId) => (dispatch) => {
  dispatch({
    type: DELETE_TRACKLOG,
    payload: {
      trackLogId,
    },
  });
};

export const addToSessionTrackLogs = (newSessions) => (dispatch) => {
  dispatch({
    type: ADD_NEW_SESSIONS_TO_TRACKLOGS,
    payload: {
      newSessions,
    },
  });
};

export const editSessionTrackLogs = (sessionTrackLogs) => (dispatch) => {
  dispatch({
    type: EDIT_SESSION_TRACKLOGS,
    payload: {
      sessionTrackLogs,
    },
  });
};

export const handleNewSessions = (newSessions) => (dispatch, getState) => {
  const { trackLog: { currentSessionTrackLogs } } = getState();
  const newSessionTrackLogs = [];
  newSessions.forEach((session) => {
    if (!currentSessionTrackLogs.some(({ sessionId }) => session.sessionId === sessionId)) {
      newSessionTrackLogs.push({
        ...session,
        trackLogArray: [],
        startTime: Date.now(),
      });
    }
  });
  if (newSessionTrackLogs.length) {
    dispatch(addToSessionTrackLogs(newSessionTrackLogs));
  }
};

export const handleEndedSessions = (endedSessions) => (dispatch, getState) => {
  const { trackLog: { currentSessionTrackLogs } } = getState();
  const newCurrentSessionTrackLogs = currentSessionTrackLogs.reduce((acc, session) => {
    if (endedSessions.some(({ sessionId: endedId }) => endedId === session.sessionId)) {
      dispatch(addTrackLogToClosedSession(session));
    } else {
      acc.push(session);
    }
    return acc;
  }, []);
  dispatch(editSessionTrackLogs(newCurrentSessionTrackLogs));
};

export const saveOpenSessionTrackLogs = () => (dispatch, getState) => {
  const { trackLog: { currentSessionTrackLogs } } = getState();
  currentSessionTrackLogs.forEach(({ serverId, databaseId, observationId, trackLogArray }) => {
    dispatch(addTrackLogToOpenedSession(serverId, databaseId, observationId, trackLogArray));
  });
  dispatch(editSessionTrackLogs([]));
};

export const deleteAllTrackLog = () => (dispatch) => {
  dispatch({ type: DELETE_ALL_SYNCED_TRACKLOG });
};

const startSyncForAll = (trackLogs) => (dispatch) => {
  dispatch({
    type: START_SYNC_FOR_TRACKLOG_LIST,
    payload: { trackLogs },
  });
};

const syncFailedButNoErrorForAll = (trackLogs) => (dispatch) => {
  dispatch({
    type: STOP_SYNC_FOR_TRACKLOG_LIST,
    payload: { trackLogs },
  });
};

const syncSuccessForAll = (syncResults) => (dispatch) => {
  dispatch({
    type: SYNC_SUCCESS_TRACKLOG_LIST,
    payload: { syncResults },
  });
};

const fetchSyncError = (syncResults) => (dispatch) => {
  dispatch({
    type: FETCH_SYNC_ERROR_TRACKLOG_LIST,
    payload: { syncResults },
  });
};

const syncTrackLogPromise = async (trackLog, server) => {
  const { url: serverUrl, user: { accessToken, refreshToken, requestTime, expiresIn } } = server;
  const {
    selectedDatabase: { serverId, projectUrl, databaseId, observationId },
    trackLogId, trackLogArray, trackLogName, startTime, endTime, sessionId } = trackLog;

  let errors = null;
  let result = null;
  try {
    result = await client.uploadTrackLog({
      projectUrl,
      tracklog: {
        trackLogId,
        trackLogName,
        trackLogArray,
        startTime,
        endTime,
        observation_list_id: sessionId,
      },
      authObject: {
        serverId,
        serverUrl,
        accessToken,
        refreshToken,
        expiresIn,
        requestTime,
      },
    });
  } catch (error) {
    try {
      Object.entries(error).forEach(([key, value]) => {
        error[key] = value['1'];
      });
      errors = error;
    } catch (err) {
      // console.log('hiba a object entriesben');
      errors = error;
    }
  }
  // There is no error, but it is not confirmed that the data is uploaded
  if (!errors && (result === 'undefined' || !result || !result.status || result.status !== 'success')) {
    errors = {};
    if (!result?.error) {
      errors.message = result.error;
    } else if (!result?.message) {
      errors.message = result.message;
    } else {
      let j = ERROR.UNKNOWN_ERROR;
      try {
        j = JSON.parse(result);
      } catch (e) {
        //    something else...
      }
      errors.message = j;
    }
  }
  /*
    This is how a result object will look like
    resultObject = {
    syncSucceeded: bool,
    trackLogId,
    serverId: null,
    databaseId: null,
    observationId: null,
    sessionId: null,
    payload: null,
  };
  */

  if (errors) {
    if (errors.message === ERROR.REQUEST_TIMEOUT || errors.message === ERROR.NETWORK_ERROR) {
      // If the data couldn't be uploaded but still correct
      return { syncSucceeded: false, serverId, databaseId };
    }
    let resp = errors.message;
    try {
      resp = JSON.parse(errors.message);
    } catch (e) {
      //    plain text error message, e.g DATA_ALREADY_UPLOADED, File upload error
    }

    if (errors.message === ERROR.DATA_ALREADY_UPLOADED) {
      // move record to synced as successful data
      return { syncSucceeded: true, trackLogId, serverId, databaseId };
    }
    // some error happened, which indicates the form is incorrect or invalid user data etc.
    return { syncSucceeded: false, trackLogId, serverId, databaseId, payload: resp };
  }
  // everything is fine 
  return { syncSucceeded: true, trackLogId, serverId, databaseId, observationId, sessionId };
};

const syncTrackLogList = async (trackLogs, server) => {
  const promises = trackLogs.map(async (trackLog) => syncTrackLogPromise(trackLog, server));
  try {
    const results = await Promise.all(promises);
    return results;
  } catch (e) {
    // console.log('error A PROMISE ALLOS synclistben');
    // console.log(e);
    throw new Error(ERROR.UNKNOWN_ERROR);
  }
};

const setMetaRecordsOfUploadedTrackLogs = (uploadedTrackLogs) => (dispatch) => {
  uploadedTrackLogs.forEach(({ serverId, databaseId, observationId, sessionId }) => {
    if (sessionId && observationId) {
      dispatch(setSessionTrackLogInfoInMetaRecord(
        SESSION_TRACKLOG_META_INFO.UPLOADED,
        sessionId,
        serverId,
        databaseId,
        observationId,
      ));
    }
  });
};

export const syncTrackLogs = (serverId, syncManually = false, syncMeasurementsAfter = false) => async (dispatch, getState) => {
  const {
    network: { status },
    servers: { data: serverList },
    trackLog: { trackLogList },
    settings: { isAutoSyncEnabled },
  } = getState();

  const shouldSyncAutomatically = isAutoSyncEnabled && status === 'wifi';
  const shouldSyncManually = syncManually && !['none', 'unknown'].includes(status);

  if (shouldSyncAutomatically || shouldSyncManually) {
    const trackLogsToSync = trackLogList
      .filter(({ selectedDatabases }) => selectedDatabases.length && selectedDatabases
        .some(({ isSynced }) => !isSynced))
      .reduce((acc, curr) => {
        const { selectedDatabases } = curr;
        if (selectedDatabases.length > 1) {
          selectedDatabases.forEach((database) => {
            if (database.serverId === serverId && !database.isSynced) {
              acc.push({
                ...curr,
                selectedDatabases: null,
                selectedDatabase: database,
              });
            }
          });
        } else if (selectedDatabases[0]?.serverId === serverId) {
          acc.push({
            ...curr,
            selectedDatabases: null,
            selectedDatabase: selectedDatabases[0],
          });
        }
        return acc;
      }, []);

    const server = { ...serverList.find(({ id }) => id === serverId), databases: null };
    if (trackLogsToSync.length) dispatch(startSyncForAll(trackLogsToSync));

    let i;
    for (i = 0; i < trackLogsToSync.length; i += 100) {
      const batchOfTrackLogs = trackLogsToSync.slice(i, Math.min(i + 100, trackLogsToSync.length));
      try {
        // eslint-disable-next-line no-await-in-loop
        const results = await syncTrackLogList(batchOfTrackLogs, server);

        const succeededTrackLogs = results.filter((res) => res.syncSucceeded);
        if (succeededTrackLogs.length) {
          dispatch(syncSuccessForAll(succeededTrackLogs));
          dispatch(setMetaRecordsOfUploadedTrackLogs(succeededTrackLogs));
        }

        const failedWithoutErrorTrackLogs = results.filter((res) => !res.syncSucceeded && !res.payload);
        if (failedWithoutErrorTrackLogs.length) dispatch(syncFailedButNoErrorForAll(failedWithoutErrorTrackLogs));

        const failedWithErrorTrackLogs = results.filter((res) => res.payload);
        if (failedWithErrorTrackLogs.length) dispatch(fetchSyncError(failedWithErrorTrackLogs));
      } catch (e) {
        dispatch(syncFailedButNoErrorForAll(batchOfTrackLogs));
      }
    }
    if (syncMeasurementsAfter) {
      dispatch(syncMeasurements(serverId, syncMeasurementsAfter));
    }
  }
};

export const exportTrackLog = (trackLogId) => async (dispatch, getState) => {
  const { trackLog: { trackLogList }, settings: { externalTracklogDirectory } } = getState();
  const trackLogToExport = trackLogList.find(({ trackLogId: id }) => id === trackLogId);

  if (trackLogToExport) {
    const { trackLogName } = trackLogToExport;
    try {
      const hasPermission = await hasExternalStoragePermission();
      if (!hasPermission) return;

      if(Platform.OS == 'android') {
        const extTracklogDirUri = await requestExternalSAFDirPermission(externalTracklogDirectory, 'Tracklog');
        if (extTracklogDirUri && extTracklogDirUri !== externalTracklogDirectory) dispatch(changeSettingsData('externalTracklogDirectory', extTracklogDirUri));

      showAlertDialog({
        title: I18n.t('export_tracklog_alert_title'),
        message: I18n.t('export_tracklog_alert_message', { trackLogName, fileUri: decodeURIComponent(extTracklogDirUri) }),
        onPositiveBtnPress: async () => {
          try {
            const gpxTrackLog = createGpxFromTrackLog(trackLogToExport);
            const newTracklogUri = await StorageAccessFramework.createFileAsync(extTracklogDirUri, `${formatToFilename(trackLogToExport.trackLogName)}.gpx`, 'application/gpx+xml');
            await StorageAccessFramework.writeAsStringAsync(newTracklogUri, gpxTrackLog, { encoding: 'utf8' });
            Toast.show(I18n.t('successful_tracklog_creation'), Toast.LONG);
          } catch (error) {
            if (error.message === ERROR.EMPTY_TRACKLOG) {
              Toast.show(I18n.t('tracklog_length'), Toast.LONG);
            } else {
              Toast.show(I18n.t('error'), Toast.LONG);
            }
          }
        },
        showCancelButton: true,
      });
      }else if (Platform.OS == 'ios') {
  
        const tracklogDirUri = `${FileSystem.documentDirectory}/tracklog`;
        const tracklogPath = `${tracklogDirUri}/${formatToFilename(trackLogToExport.trackLogName)}.gpx`;

        const { exists } = await FileSystem.getInfoAsync(tracklogDirUri);

        if (!exists) {
          await FileSystem.makeDirectoryAsync(tracklogDirUri);
        }

        showAlertDialog({
          title: I18n.t('export_tracklog_alert_title'),
          message: I18n.t('export_tracklog_alert_message', { trackLogName, fileUri: decodeURIComponent(tracklogDirUri) }),
          onPositiveBtnPress: async () => {
            try {

              const gpxTrackLog = createGpxFromTrackLog(trackLogToExport);
              await FileSystem.writeAsStringAsync(tracklogPath, gpxTrackLog, { encoding: 'utf8' });

              Toast.show(I18n.t('successful_tracklog_creation'), Toast.LONG);
            } catch (error) {  
              if (error.message === ERROR.EMPTY_TRACKLOG) {
                Toast.show(I18n.t('tracklog_length'), Toast.LONG);
              } else {
                Toast.show(I18n.t('error'), Toast.LONG);
              }
            }
          },
          showCancelButton: true,
        });

      }

      if (!trackLogToExport.trackLogArray?.length) throw new Error(ERROR.EMPTY_TRACKLOG);

    } catch (error) {
      Toast.show(I18n.t('error'), Toast.LONG);
    }
  }
};
