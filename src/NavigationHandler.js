import { NavigationActions, StackActions, DrawerActions } from 'react-navigation';
import { store } from './config/setupStore';

let navigator;

const setTopLevelNavigator = (navigatorRef) => {
  navigator = navigatorRef;
};

// NavigationActions methods

const navigate = (routeName, params, key, action) => {
  navigator.dispatch(NavigationActions.navigate({
    routeName,
    params,
    key,
    action,
  }));

  // Dispatch redux action
  store.dispatch({
    type: 'Navigation/NavigateTo',
    routeName,
    params,
    key,
  });
};

const goBack = (key) => {
  navigator.dispatch(NavigationActions.back({
    key,
  }));
};

const setParams = (params, key) => {
  navigator.dispatch(NavigationActions.setParams({
    params,
    key,
  }));
};

// StackActions methods

const pop = (n) => {
  navigator.dispatch(StackActions.pop({
    n,
  }));
};

const reset = (routeName, params) => {
  navigator.dispatch(StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName, params })],
  }));
};

const replace = (routeName, params) => {
  navigator.dispatch(StackActions.replace({
    routeName,
    params,
  }));
};

// DrawerActions methods

const openDrawer = () => {
  navigator.dispatch(DrawerActions.openDrawer());
};

const closeDrawer = () => {
  navigator.dispatch(DrawerActions.closeDrawer());
};

const toggleDrawer = () => {
  navigator.dispatch(DrawerActions.toggleDrawer());
};

export default {
  setTopLevelNavigator,
  navigate,
  goBack,
  setParams,
  pop,
  reset,
  replace,
  openDrawer,
  closeDrawer,
  toggleDrawer,
};
