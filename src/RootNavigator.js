import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import MainNavigator from './navigators/MainNavigator';
import GameNavigator from './navigators/GameNavigator';

const RootNavigator = createStackNavigator(
  {
    Main: {
      screen: MainNavigator,
    },
    Game: {
      screen: GameNavigator,
    },
  },
  {
    initialRouteName: 'Main',
    headerMode: 'none',
  },
);

export default createAppContainer(RootNavigator);
